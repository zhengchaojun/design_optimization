package qlsl.androiddesign.api.http.service.commonservice;

import com.alibaba.fastjson.JSONObject;
import com.cx.annotation.aspect.CheckNetWork;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import qlsl.androiddesign.api.activity.baseactivity.CommonActivityBase;
import qlsl.androiddesign.api.http.service.baseservice.ServiceBase;
import qlsl.androiddesign.api.http.xutils.HttpProtocol;

/**
 * 系统模块：地图相关
 * 作者：郑朝军 on 2017/3/27 22:54
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */

public class ZMapMainService extends ServiceBase<CommonActivityBase>
{
    private String className = getClassName(ZMapMainService.class);

    /**
     * 根据当前的屏幕范围查找当前范围的线
     *
     * @param range
     */
    @CheckNetWork
    public void queryAttGraByWin(final String range)
    {
        final String method = "queryAttGraByWin";
        getV().showProgressBar();
        Flowable.create(new FlowableOnSubscribe<Object>()
        {
            @Override
            public void subscribe(FlowableEmitter<Object> e) throws Exception
            {
                HttpProtocol protocol = new HttpProtocol();
                try
                {
                    JSONObject jo = null;

                    if (isDataInvalid(jo, e))
                    {
                        return;
                    }

//                    List<Syscfg> list = JSONArray.parseArray(
//                            jo.getString("row"), Syscfg.class);


//                    SysCfg sysCfg = new SysCfg();
//                    sysCfg.setListSyscfg(list);
//                    application.setSysCfg(sysCfg);

//                    e.onNext(list);
                    e.onComplete();
                    outputMessageInfo(protocol, jo, className, method);
                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                    outputException(e, exception, className);
                }
            }
        }, BackpressureStrategy.BUFFER)
                .compose(getScheduler())
                .compose(getV().bindToLifecycle())
                .subscribe(createSubscriber(method));
    }

}
