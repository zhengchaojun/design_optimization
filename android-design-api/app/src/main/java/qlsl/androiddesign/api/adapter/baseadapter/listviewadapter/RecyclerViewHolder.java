package qlsl.androiddesign.api.adapter.baseadapter.listviewadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * 类描述：RecyclerView的item的ViewHolder
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传：
 * 作者：郑朝军 on 2017/6/20 10:27
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/20 10:27
 * 修改备注：
 */
public class RecyclerViewHolder extends RecyclerView.ViewHolder
{
    protected Context mContext;

    protected ViewHolderHelper mViewHolderHelper;

    protected RecyclerView mRecyclerView;

    protected RecyclerViewBaseAdapter mRecyclerViewAdapter;

    public RecyclerViewHolder(RecyclerViewBaseAdapter recyclerViewAdapter, RecyclerView
            recyclerView, View itemView)
    {
        super(itemView);
        mRecyclerViewAdapter = recyclerViewAdapter;
        mRecyclerView = recyclerView;
        mContext = mRecyclerView.getContext();
        mViewHolderHelper = new ViewHolderHelper(mRecyclerView, this);
    }

    public int getAdapterPositionWrapper()
    {
        if (mRecyclerViewAdapter.getHeadersCount() > 0)
        {
            return getAdapterPosition() - mRecyclerViewAdapter.getHeadersCount();
        }
        else
        {
            return getAdapterPosition();
        }
    }

    public ViewHolderHelper getmViewHolderHelper()
    {
        return mViewHolderHelper;
    }
}
