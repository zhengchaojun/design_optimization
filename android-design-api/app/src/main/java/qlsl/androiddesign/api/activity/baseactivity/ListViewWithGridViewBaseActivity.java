package qlsl.androiddesign.api.activity.baseactivity;

import android.view.View;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.adapter.commonadapter.ListEditAdapter;
import qlsl.androiddesign.api.adapter.commonadapter.PictureUpdateAdapter;
import qlsl.androiddesign.api.http.service.commonservice.AListViewWithGridViewService;
import qlsl.androiddesign.api.util.commonutil.AdapterViewUtils;
import qlsl.androiddesign.api.util.commonutil.ReflectUtil;

/**
 * 类描述：ListViewWithGridView  和内容描述
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传：
 * 作者：郑朝军 on 2017/6/28 17:32
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/28 17:32
 * 修改备注：
 */
public class ListViewWithGridViewBaseActivity<P extends AListViewWithGridViewService> extends
        CommonActivityBase<P>
{
    protected ListView listView;

    protected GridView gridView;

    protected List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();

    protected List<String> gridViewlist = new ArrayList<String>();

    @Override
    public int getLayoutId()
    {
        return R.layout.activity_list_grid;
    }

    @Override
    public boolean useEventBus()
    {
        return false;
    }

    @Override
    public P newP()
    {
        return ReflectUtil.getT(this, 0);
    }


    @Override
    protected void initView()
    {
        listView = findView(R.id.listView);
        gridView = findView(R.id.gridView);
    }

    @Override
    protected void initData()
    {
        notifyDataSetChanged();
        AdapterViewUtils.setListViewHeight(listView);

        notifyDataSetChangedGridView();
        AdapterViewUtils.setGridViewHeight(gridView);
    }

    @Override
    protected void initListener()
    {

    }

    public <T> void showData(T... t)
    {

    }

    public <T> void showNoData(T... t)
    {

    }

    public void onClick(View view)
    {

    }

    protected void notifyDataSetChanged()
    {
        BaseAdapter adapter = (BaseAdapter) listView.getAdapter();
        if (adapter == null)
        {
            adapter = getListAdapter();
            listView.setAdapter(adapter);
        }
        else
        {
            adapter.notifyDataSetChanged();
        }
    }

    protected BaseAdapter getListAdapter()
    {
        return new ListEditAdapter(activity, list);
    }

    protected void notifyDataSetChangedGridView()
    {
        BaseAdapter adapter = (BaseAdapter) gridView.getAdapter();
        if (adapter == null)
        {
            gridViewlist.add(null);
            adapter = getGridAdapter();
            gridView.setAdapter(adapter);
        }
        else
        {
            adapter.notifyDataSetChanged();
        }
    }

    protected BaseAdapter getGridAdapter()
    {
        return new PictureUpdateAdapter(activity, gridViewlist);
    }


}
