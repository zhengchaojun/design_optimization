package qlsl.androiddesign.api.entity.commonentity;

import android.os.Parcel;
import android.os.Parcelable;

import qlsl.androiddesign.api.entity.baseentity.BaseModel;

/**
 * 类描述：联系人列表
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传：
 * 作者：郑朝军 on 2017/7/6 15:52
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/7/6 15:52
 * 修改备注：
 */
public class Contacts extends BaseModel implements Parcelable,Comparable<Contacts>
{
    private String contact;//联系人名字

    private String id;

    private String initials;

    public String getContact()
    {
        return contact;
    }

    public void setContact(String contact)
    {
        this.contact = contact;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getInitials()
    {
        return initials;
    }

    public void setInitials(String initials)
    {
        this.initials = initials;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(this.contact);
        dest.writeString(this.id);
    }

    public Contacts()
    {
    }

    protected Contacts(Parcel in)
    {
        this.contact = in.readString();
        this.id = in.readString();
    }

    public static final Parcelable.Creator<Contacts> CREATOR = new Parcelable.Creator<Contacts>()
    {
        @Override
        public Contacts createFromParcel(Parcel source)
        {
            return new Contacts(source);
        }

        @Override
        public Contacts[] newArray(int size)
        {
            return new Contacts[size];
        }
    };

    @Override
    public int compareTo(Contacts o)
    {
        return initials.compareTo(o.getInitials());
    }
}
