package qlsl.androiddesign.api.transformation;

import android.graphics.Bitmap;

import com.squareup.picasso.Transformation;

import qlsl.androiddesign.api.util.commonutil.BitmapUtils;

public class CircleTransformation implements Transformation
{

    public Bitmap transform(Bitmap source)
    {
        Bitmap result = BitmapUtils.getRoundedCornerBitmap(source,
                source.getWidth());
        if (result != source)
        {
            source.recycle();
        }
        return result;
    }

    public String key()
    {
        return "circle";
    }
}
