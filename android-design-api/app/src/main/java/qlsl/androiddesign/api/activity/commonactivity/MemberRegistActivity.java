package qlsl.androiddesign.api.activity.commonactivity;

import android.os.CountDownTimer;
import android.text.Editable;
import android.text.InputType;
import android.text.Selection;
import android.text.TextUtils;
import android.text.method.DigitsKeyListener;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import com.cx.annotation.aspect.SingleClick;

import java.lang.ref.WeakReference;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.activity.baseactivity.CommonActivityBase;
import qlsl.androiddesign.api.entity.baseentity.BaseModel;
import qlsl.androiddesign.api.entity.commonentity.FragmentArgs;
import qlsl.androiddesign.api.http.service.commonservice.MemberService;
import qlsl.androiddesign.api.util.commonutil.InputMatch;
import qlsl.androiddesign.api.view.commonview.ClearEditText;

/**
 * 作者：郑朝军 on 2017/4/14 19:16
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */

public class MemberRegistActivity extends CommonActivityBase<MemberService>
{
    private ClearEditText et_phone;

    private ClearEditText et_code;

    private Button btn_send;

    private ClearEditText et_password;

    private boolean password = false;

    private String code;

    private SendCodeTimer sendCodeTimer;

    @Override
    public int getLayoutId()
    {
        return R.layout.activity_member_regist_real_new;
    }

    @Override
    public boolean useEventBus()
    {
        return false;
    }

    @Override
    public MemberService newP()
    {
        return new MemberService();
    }

    @Override
    protected void initView()
    {
        setTitle("注册");

        et_phone = findView(R.id.et_phone);
        et_code = findView(R.id.et_code);
        btn_send = findView(R.id.btn_send);
        et_password = findView(R.id.et_password);
    }

    @Override
    protected void initData()
    {

    }

    @Override
    protected void initListener()
    {

    }

    @Override
    public void showNetWorkSucceedData(String method, Object values)
    {
        if (values instanceof BaseModel)
        {
            BaseModel baseModel = (BaseModel) values;
            code = baseModel.getCode();
        }
    }

    @SingleClick
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.btn_send:
                doClickSendButton();
                break;
            case R.id.btn_regist:
                doClickRegistButton();
                break;
            case R.id.iv_open:
                doClickOpenView();
                break;
            case R.id.tv_article:
                startActivity(new FragmentArgs().add("title", "使用条款").add("url",
                        "file:///android_asset/other/agreement.html"), WebViewActivity.class);
                break;
        }
    }

    private void doClickSendButton()
    {
        if (!btn_send.getText().toString().equals("发送验证码"))
        {
            return;
        }
        String phone = et_phone.getText().toString();
        if (TextUtils.isEmpty(phone))
        {
            showToast("请输入手机号码");
            return;
        }
        if (!InputMatch.isMobileNO(phone))
        {
            showToast("请输入正确格式的手机号码");
            return;
        }
        sendCodeTimer = new SendCodeTimer(this, 60000, 1000);
        sendCodeTimer.start();

//        getP().requestSMSCode("1", phone, sendCodeTimer, btn_send);
    }

    private void doClickRegistButton()
    {
        String phone = et_phone.getText().toString();
        String code = et_code.getText().toString();
        String password = et_password.getText().toString();
        if (TextUtils.isEmpty(phone))
        {
            showToast("请输入手机号码");
            return;
        }
        if (!InputMatch.isMobileNO(phone))
        {
            showToast("请输入正确格式的手机号码");
            return;
        }
        if (TextUtils.isEmpty(code))
        {
            showToast("请输入验证码");
            return;
        }
        if (!InputMatch.isNumeric(code))
        {
            showToast("请输入数字验证码");
            return;
        }
        if (!code.equals(this.code))
        {
            showToast("验证码错误");
            return;
        }
        if (TextUtils.isEmpty(password))
        {
            showToast("请输入密码");
            return;
        }
        if (password.length() < 6)
        {
            showToast("密码不得小于6位");
            return;
        }
        if (!((CheckBox) findViewById(R.id.cb_agree)).isChecked())
        {
            showToast("尚未同意使用条款");
            return;
        }

//        getP().registByMobile(phone, password, code);
    }

    private void doClickOpenView()
    {
        String digits = activity.getString(R.string.password_digits);
        DigitsKeyListener keyListener = DigitsKeyListener.getInstance(digits);
        if (!password)
        {
            et_password.setInputType(InputType.TYPE_CLASS_TEXT
                    | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
        else
        {
            et_password.setInputType(InputType.TYPE_CLASS_TEXT
                    | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        }
        et_password.setKeyListener(keyListener);
        Editable etable = et_password.getText();
        Selection.setSelection(etable, etable.length());
        password = !password;
    }

    private static class SendCodeTimer extends CountDownTimer
    {

        private MemberRegistActivity autoPlayManager;

        public SendCodeTimer(MemberRegistActivity autoPlayManager,
                             long millisInFuture, long countDownInterval)
        {
            super(millisInFuture, countDownInterval);
            this.autoPlayManager = new WeakReference<MemberRegistActivity>(
                    autoPlayManager).get();
        }

        public void onTick(long millisUntilFinished)
        {
            this.autoPlayManager.btn_send.setText((millisUntilFinished / 1000)
                    + "秒后重发");
        }

        public void onFinish()
        {
            this.autoPlayManager.btn_send.setText("发送验证码");
        }

    }


    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if (sendCodeTimer != null)
        {
            sendCodeTimer.cancel();
            sendCodeTimer = null;
        }
    }
}
