package qlsl.androiddesign.api.adapter.baseadapter.listviewadapter;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;

import java.util.List;

/**
 * 类描述：BaseAdapter适配器 ViewHolder缓存功能
 * 需要传入的键：Activity list
 * 传入的值类型：Activity List<T>
 * 传入的值含义：Activity item对象
 * 是否必传：是 是
 * 作者：郑朝军 on 2017/6/20 10:45
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/20 10:45
 * 修改备注：
 */
public abstract class ListViewBaseAdapter<T> extends android.widget.BaseAdapter
        implements Filterable
{
    protected Activity activity;

    protected List<T> list;

    public ListViewBaseAdapter(Activity activity)
    {
        this.activity = activity;
    }

    public ListViewBaseAdapter(Activity activity, List<T> list)
    {
        this.activity = activity;
        this.list = list;
    }

    /**
     * 外部类可通过此函数传递对象进来<br/>
     * 以应对在构造adapter后，达到某种条件时需要传值进来的情况<br/>
     */
    @SuppressWarnings("hiding")
    public <T> void transport(T... t)
    {

    }

    public synchronized void notifyDataSetChanged()
    {
        super.notifyDataSetChanged();
    }

    /**
     * 局部更新数据，调用一次getView()方法；Google推荐的做法
     *
     * @param listView 要更新的listview
     * @param position 要更新的位置
     */
    public void notifyDataSetChanged(ListView listView, int position)
    {
        int firstVisiblePosition = listView.getFirstVisiblePosition();
        int lastVisiblePosition = listView.getLastVisiblePosition();
        if (position >= firstVisiblePosition && position <= lastVisiblePosition)
        {  // 获取指定位置view对象
            View view = listView.getChildAt(position - firstVisiblePosition);
            getView(position, view, listView);
        }
    }

    public int getCount()
    {
        return list.size();
    }

    public T getItem(int position)
    {
        return list.get(position);
    }

    public long getItemId(int position)
    {
        return position;
    }

    /**
     * 获取MVC模式控件的子布局
     */
    protected View getItemView(View convertView, int item_resource, ViewGroup parent)
    {
        if (convertView == null)
        {
            convertView = activity.getLayoutInflater().inflate(item_resource,
                    null);
        }
        return convertView;
    }

    /**
     * 获取MVC模式控件子布局的控件，自动处理ViewHolder的缓存
     */
    @SuppressWarnings("unchecked")
    protected <V extends View> V getView(View convertView, int id)
    {
        SparseArray<View> viewHolder = (SparseArray<View>) convertView.getTag();
        if (viewHolder == null)
        {
            viewHolder = new SparseArray<View>();
            convertView.setTag(viewHolder);
        }
        View childView = viewHolder.get(id);
        if (childView == null)
        {
            childView = convertView.findViewById(id);
            viewHolder.put(id, childView);
        }
        return (V) childView;
    }

    public Filter getFilter()
    {
        return null;
    }


    //////////////////////////////////////以下为提供的API
    // /////////////////////////////////////////////////////
    public List<T> getList()
    {
        return list;
    }
    /**
     * 在集合头部添加新的数据集合
     *
     * @param data
     */
    public void addNewData(List<T> data)
    {
        if (data != null)
        {
            list.addAll(0, data);
            notifyDataSetChanged();
        }
    }

    /**
     * 在集合尾部添加更多数据集合
     *
     * @param data
     */
    public void addMoreData(List<T> data)
    {
        if (data != null)
        {
            list.addAll(list.size(), data);
            notifyDataSetChanged();
        }
    }

    /**
     * 设置全新的数据集合，如果传入null，则清空数据列表（第一次从服务器加载数据，或者下拉刷新当前界面数据表）
     *
     * @param data
     */
    public void setData(List<T> data)
    {
        if (data != null)
        {
            list = data;
        }
        else
        {
            list.clear();
        }
        notifyDataSetChanged();
    }

    /**
     * 清空数据列表
     */
    public void clear()
    {
        list.clear();
        notifyDataSetChanged();
    }

    /**
     * 删除指定索引数据条目
     *
     * @param position
     */
    public void removeItem(int position)
    {
        list.remove(position);
        notifyDataSetChanged();
    }

    /**
     * 删除指定数据条目
     *
     * @param model
     */
    public void removeItem(T model)
    {
        list.remove(model);
        notifyDataSetChanged();
    }

    /**
     * 在指定位置添加数据条目
     *
     * @param position
     * @param model
     */
    public void addItem(int position, T model)
    {
        list.add(position, model);
        notifyDataSetChanged();
    }

    /**
     * 在集合头部添加数据条目
     *
     * @param model
     */
    public void addFirstItem(T model)
    {
        addItem(0, model);
    }

    /**
     * 在集合末尾添加数据条目
     *
     * @param model
     */
    public void addLastItem(T model)
    {
        addItem(list.size(), model);
    }

    /**
     * 替换指定索引的数据条目
     *
     * @param location
     * @param newModel
     */
    public void setItem(int location, T newModel)
    {
        list.set(location, newModel);
        notifyDataSetChanged();
    }

    /**
     * 替换指定数据条目
     *
     * @param oldModel
     * @param newModel
     */
    public void setItem(T oldModel, T newModel)
    {
        setItem(list.indexOf(oldModel), newModel);
    }

    /**
     * 移动数据条目的位置
     *
     * @param fromPosition
     * @param toPosition
     */
    public void moveItem(int fromPosition, int toPosition)
    {
        list.add(toPosition, list.remove(fromPosition));
        notifyDataSetChanged();
    }

    /**
     * @return 获取第一个数据模型
     */
    public
    @Nullable
    T getFirstItem()
    {
        return getCount() > 0 ? getItem(0) : null;
    }

    /**
     * @return 获取最后一个数据模型
     */
    public
    @Nullable
    T getLastItem()
    {
        return getCount() > 0 ? getItem(getCount() - 1) : null;
    }

}
