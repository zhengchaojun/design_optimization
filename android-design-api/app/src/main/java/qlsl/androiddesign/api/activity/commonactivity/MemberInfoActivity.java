package qlsl.androiddesign.api.activity.commonactivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.activity.baseactivity.CommonActivityBase;
import qlsl.androiddesign.api.adapter.commonadapter.MemberInfoAdapter;
import qlsl.androiddesign.api.constant.IntentCodeConstant;
import qlsl.androiddesign.api.http.service.commonservice.ACopyService;
import qlsl.androiddesign.api.http.xutils.HttpProtocol;
import qlsl.androiddesign.api.listener.OnUploadFileListener;
import qlsl.androiddesign.api.method.UserMethod;
import qlsl.androiddesign.api.util.commonutil.WindowsUtils;

/**
 * 类描述：个人信息页
 * 需要传入的键：****
 * 传入的值类型：****
 * 传入的值含义：****
 * 是否必传：****
 * 作者：郑朝军 on 2017/6/14 11:29
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/14 11:29
 * 修改备注：
 */
public class MemberInfoActivity extends CommonActivityBase<ACopyService>
{
    private ListView listView;

    @Override
    public int getLayoutId()
    {
        return R.layout.common_list_view_no_margin_divider;
    }

    @Override
    public boolean useEventBus()
    {
        return false;
    }

    @Override
    public ACopyService newP()
    {
        return new ACopyService();
    }

    @Override
    protected void initView()
    {
        setTitle("个人信息");
        listView = (ListView) findViewById(R.id.listView);
    }


    @Override
    protected void initData()
    {
    }

    @Override
    protected void initListener()
    {

    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.list_item:
                doClickListItem(view);
                break;
        }
    }

    public void showNetWorkSucceedData(String method, Object values)
    {

    }

    public <T> void showData(T... t)
    {

    }

    public <T> void showNoData(T... t)
    {

    }


    private void doClickListItem(View view)
    {
        int position = listView.getPositionForView(view);
        if (position == 0)
        {
            startActivityForResult(WindowsUtils.getImageFileIntent(), IntentCodeConstant.REQUEST_CODE_PHOTO_PICKED);
        }
        else if (position == 1)
        {
            TextView tv_value = (TextView) listView.getChildAt(1).findViewById(R.id.tv_value);
            Intent intent = new Intent(activity, EditActivity.class);
            intent.putExtra("title", "编辑用户名");
            intent.putExtra("content", tv_value.getText().toString());
            intent.putExtra("maxLength", 11);
            startActivity(intent);
        }
        else if (position == 2)
        {
            Intent intent = new Intent(activity, MemberForgetPasswordActivity.class);
            intent.putExtra("type", 1);
            startActivity(intent);
        }
        else if (position == 3)
        {
//            startActivity(MemberResetPhoneActivity.class);
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        setListViewData();
    }

    public void setListViewData()
    {
        String photo = UserMethod.getUser().getPhoto();
        String username = UserMethod.getUser().getName();
        String mobile = UserMethod.getUser().getCell();

        String[] names = new String[]{"头像", "用户昵称", "修改密码", "联系方式"};
        String[] values = new String[]{photo, username, null, mobile};

        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        for (int index = 0; index < names.length; index++)
        {
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("text", names[index]);
            map.put("value", values[index]);
            list.add(map);
        }
        MemberInfoAdapter adapter = new MemberInfoAdapter(activity, list);
        listView.setAdapter(adapter);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == IntentCodeConstant.REQUEST_CODE_PHOTO_PICKED && resultCode == Activity
                .RESULT_OK && data != null)
        {
            Uri uri = data.getData();
            if (uri == null)
            {
                showToast("无法加载图片");
                return;
            }
            cropPhoto(WindowsUtils.getImagePathForUri(activity, uri));
        }
        else if (requestCode == IntentCodeConstant.CROP_PHOTO)
        {
            if (data == null)
            {
                return;
            }
            Bitmap bitmap = data.getExtras().getParcelable("data");
            File file = saveBitmapToFile(bitmap);
            new HttpProtocol().setMethod("http://192.168.2.200:8080/gisxjwx/DocSvr?cmd=upload&major=7&minor=8&entityid=922&username=admin&fid=922&sys=21&glid=0&task=现场维修").uploadFile(file, null, listener);
        }
    }

    private OnUploadFileListener listener = new OnUploadFileListener()
    {
        public void onSuccess(JSONObject jo)
        {
            String photo = jo.getString("photo");
            UserMethod.getUser().setPhoto(photo);
            setListViewData();
        }
    };

}
