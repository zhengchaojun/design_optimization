package qlsl.androiddesign.api.view.slidinguppanel;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ListView;
import android.widget.ScrollView;

/**
 * 类描述：
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传：
 * 作者：郑朝军 on 2017/9/7 9:14
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/9/7 9:14
 * 修改备注：
 */
public class ScrollableViewHelper
{
    public int getScrollableViewScrollPosition(View scrollableView, boolean isSlidingUp) {
        if(scrollableView == null) {
            return 0;
        } else {
            View lm1;
            if(scrollableView instanceof ScrollView) {
                if(isSlidingUp) {
                    return scrollableView.getScrollY();
                } else {
                    ScrollView rv2 = (ScrollView)scrollableView;
                    lm1 = rv2.getChildAt(0);
                    return lm1.getBottom() - (rv2.getHeight() + rv2.getScrollY());
                }
            } else if(scrollableView instanceof ListView && ((ListView)scrollableView).getChildCount() > 0) {
                ListView rv1 = (ListView)scrollableView;
                if(rv1.getAdapter() == null) {
                    return 0;
                } else if(isSlidingUp) {
                    lm1 = rv1.getChildAt(0);
                    return rv1.getFirstVisiblePosition() * lm1.getHeight() - lm1.getTop();
                } else {
                    lm1 = rv1.getChildAt(rv1.getChildCount() - 1);
                    return (rv1.getAdapter().getCount() - rv1.getLastVisiblePosition() - 1) * lm1.getHeight() + lm1.getBottom() - rv1.getBottom();
                }
            } else if(scrollableView instanceof RecyclerView && ((RecyclerView)scrollableView).getChildCount() > 0) {
                RecyclerView rv = (RecyclerView)scrollableView;
                RecyclerView.LayoutManager lm = rv.getLayoutManager();
                if(rv.getAdapter() == null) {
                    return 0;
                } else {
                    View lastChild;
                    if(isSlidingUp) {
                        lastChild = rv.getChildAt(0);
                        return rv.getChildLayoutPosition(lastChild) * lm.getDecoratedMeasuredHeight(lastChild) - lm.getDecoratedTop(lastChild);
                    } else {
                        lastChild = rv.getChildAt(rv.getChildCount() - 1);
                        return (rv.getAdapter().getItemCount() - 1) * lm.getDecoratedMeasuredHeight(lastChild) + lm.getDecoratedBottom(lastChild) - rv.getBottom();
                    }
                }
            } else {
                return 0;
            }
        }
    }
}
