package qlsl.androiddesign.api.listener;

import android.view.View;

import com.itheima.pulltorefreshlib.PullToRefreshBase;


/**
 * 作者：郑朝军 on 2017/3/29 17:03
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */

public class OnRefreshListenerBase<T extends View>
{
    protected final void onLoadComplete(final PullToRefreshBase<T> refreshView, int totalPage, int totalCount)
    {
        String info = "总页数：" + totalPage + "  总数据量：" + totalCount;
        refreshView.setRefreshingLabel(info);
        refreshView.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                refreshView.onRefreshComplete();
            }
        }, 1000);
    }

    protected final void onLoadComplete(final PullToRefreshBase<T> refreshView)
    {
        refreshView.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                refreshView.onRefreshComplete();
            }
        }, 1000);
    }
}
