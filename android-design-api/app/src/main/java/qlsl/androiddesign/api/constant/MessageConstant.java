package qlsl.androiddesign.api.constant;

public interface MessageConstant extends SoftwareConstant
{

    String MSG_PARSEERROR_FAILED = "数据解析异常";

    String MSG_NOCONNECT_FAILED = "无连接异常";

    String MSG_AUTH_FAILED = "用户验证异常";

    String MSG_NODATA_FAILED = "无数据返回异常";

    String MSG_BUSINESS_FAILED = "业务异常";

    String MSG_OTHER_FAILED = "其他异常";

    String MSG_UNKOW_FAILED = "不详";

    String MSG_SUCCESS = "success";

    String MSG_CLIENT_FAILED = "网络连接异常，请检查网络";

    String MSG_SERVER_FAILED = "服务器已关闭，请稍后再试";

    String MSG_SERVER_RESTART = "服务器已重启，请重试";

    String MSG_EMPTY = "";

    String MSG_SYNC_SUCCESS = "同步数据成功";

    String MSG_SYNC_FAILED = "同步数据失败!";

    String MSG_LOGIN_SUCCESS = "登录成功";

    String MSG_UPDATE_PWD_SUCCESS = "修改密码成功";

    String MSG_UPDATE_PHONE_SUCCESS = "修改手机号成功";

    String MSG_PRAISE_SUCCESS = "点赞成功";

    String MSG_PRAISE_CANCEL_SUCCESS = "取消点赞成功";

    String MSG_UPLOAD_FILE_SUCCESS = "上传文件成功";

    String MSG_UPLOAD_FILE_FAILED = "上传文件失败";

    String MSG_MEMBER_INVALID = "您的账号已在别的设备登录";

    String MSG_LOGIN_INVALID = "登录已失效，请重新登录";

    String MSG_SEND_SUCCESS = "发送成功";

    String MSG_UPDATE_SUCCESS = "更新成功";

    String MSG_DELETE_SUCCESS = "删除成功";

    String MSG_CREATE_SUCCESS = "创建成功";

    String MSG_DISMISS_SUCCESS = "解散成功";

    String MSG_EXIT_SUCCESS = "退出成功";

}
