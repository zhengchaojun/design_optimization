package qlsl.androiddesign.api.fragment.commonfragment;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.fragment.basefragment.ZLazyFragment;
import qlsl.androiddesign.api.util.commonutil.ToastUtils;

public class FragmentTab2 extends ZLazyFragment {

    @Override
    public int getLayoutId() {
        return R.layout.tab2;
    }

    @Override
    public Object newP() {
        return null;
    }

    @Override
    protected void initView() {
        ToastUtils.showShort("FragmentTab2");
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {

    }


}
