package qlsl.androiddesign.api.util.commonutil;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import java.io.Serializable;

import qlsl.androiddesign.api.activity.R;

/**
 * 类描述：通知栏工具类
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传：
 * 作者：郑朝军 on 2017/7/27 11:11
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/7/27 11:11
 * 修改备注：
 */
public class NotificationUtil
{
    /**
     * 打开通知栏,点击自动消失,只显示一条通知栏
     *
     * @param context
     * @param title
     * @param content
     * @param targetActivity
     */
    public static void showNotice(Context context, String title, String content, Class<?>
            targetActivity)
    {
        Intent intent = new Intent(context.getApplicationContext(), targetActivity);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        NotificationManager manager = (NotificationManager) context.getSystemService(Context
                .NOTIFICATION_SERVICE);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent
                .FLAG_ONE_SHOT);
        Notification.Builder notificationBuilder = new Notification.Builder(context);
        notificationBuilder.setContentTitle(title);
        notificationBuilder.setContentText(content);
        notificationBuilder.setContentIntent(pendingIntent);

        Notification notification = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN)
        {
            notification = notificationBuilder.build();
        }
        notification.icon = R.mipmap.ic_launcher;
        notification.tickerText = content;
        notification.defaults = Notification.DEFAULT_SOUND;
        notification.audioStreamType = android.media.AudioManager.ADJUST_LOWER;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        manager.notify(0x10000, notification);
    }

    /**
     * 打开通知栏，携带数据,只显示一条通知栏
     *
     * @param context
     * @param title
     * @param content
     * @param targetActivity
     */
    public static void showNotice(Context context, String title, String content, Class<?>
            targetActivity, Serializable data)
    {
        Intent intent = new Intent(context.getApplicationContext(), targetActivity);
        intent.putExtra("data", data);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        NotificationManager manager = (NotificationManager) context.getSystemService(Context
                .NOTIFICATION_SERVICE);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent
                .FLAG_ONE_SHOT);
        Notification.Builder notificationBuilder = new Notification.Builder(context);
        notificationBuilder.setContentTitle(title);
        notificationBuilder.setContentText(content);
        notificationBuilder.setContentIntent(pendingIntent);

        Notification notification = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN)
        {
            notification = notificationBuilder.build();
        }
        notification.icon = R.mipmap.ic_launcher;
        notification.tickerText = content;
        notification.defaults = Notification.DEFAULT_SOUND;
        notification.audioStreamType = android.media.AudioManager.ADJUST_LOWER;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        manager.notify(0x10000, notification);
    }

    /**
     * 打开通知栏，点击不消失,只显示一条通知栏
     *
     * @param context
     * @param title
     * @param content
     * @param targetActivity
     * @param isNotificationAutoCancel 点击通知栏是否关闭通知标识
     */
    public static void showNotice(Context context, String title, String content, Class<?>
            targetActivity, boolean isNotificationAutoCancel)
    {
        Intent intent = new Intent(context.getApplicationContext(), targetActivity);
        intent.putExtra("title", title);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        NotificationManager manager = (NotificationManager) context.getSystemService(Context
                .NOTIFICATION_SERVICE);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent
                .FLAG_ONE_SHOT);
        Notification.Builder notificationBuilder = new Notification.Builder(context);
        notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
        notificationBuilder.setTicker(content);
        notificationBuilder.setContentTitle(title);
        notificationBuilder.setContentText(content);
        notificationBuilder.setContentIntent(pendingIntent);

        Notification notification = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN)
        {
            notification = notificationBuilder.build();
        }
        notification.defaults = Notification.DEFAULT_SOUND;
        notification.audioStreamType = android.media.AudioManager.ADJUST_LOWER;
        if (isNotificationAutoCancel)
            notification.flags |= Notification.FLAG_AUTO_CANCEL;

        manager.notify(0x10000, notification);
    }

    /**
     * 打开通知栏,点击自动消失,显示多条到通知栏
     *
     * @param context
     * @param title
     * @param content
     * @param targetActivity
     */
    public static void showNotice(Context context, String title, String content, Class<?>
            targetActivity, int id)
    {
        Intent intent = new Intent(context.getApplicationContext(), targetActivity);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        NotificationManager manager = (NotificationManager) context.getSystemService(Context
                .NOTIFICATION_SERVICE);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent
                .FLAG_ONE_SHOT);
        Notification.Builder notificationBuilder = new Notification.Builder(context);
        notificationBuilder.setContentTitle(title);
        notificationBuilder.setContentText(content);
        notificationBuilder.setContentIntent(pendingIntent);

        Notification notification = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN)
        {
            notification = notificationBuilder.build();
        }
        notification.icon = R.mipmap.ic_launcher;
        notification.tickerText = content;
        notification.defaults = Notification.DEFAULT_SOUND;
        notification.audioStreamType = android.media.AudioManager.ADJUST_LOWER;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        manager.notify(id, notification);
    }

}
