package qlsl.androiddesign.api.listener;

import android.widget.AbsListView;

import com.itheima.pulltorefreshlib.PullToRefreshBase;

import qlsl.androiddesign.api.entity.commonentity.Pager;


public abstract class OnRefreshListener<T extends AbsListView> extends OnRefreshListenerBase<T> implements
        PullToRefreshBase.OnRefreshListener2<T> {

    public void onPullDownToRefresh(PullToRefreshBase<T> refreshView) {
        onRefresh();
    }

    public void onPullUpToRefresh(PullToRefreshBase<T> refreshView) {
        onRefresh();
    }

    public void onRefresh(PullToRefreshBase<T> refreshView, Pager pager) {
        int pageNo = pager.getPageNo();
        int totalCount = pager.getTotalCount();
        int totalPage = pager.getTotalPage();
        if (pageNo < totalPage) {
            nextPager();
        } else {
            onLoadComplete(refreshView, totalPage, totalCount);
        }
    }

    public abstract void onRefresh();

    public abstract void nextPager();

}
