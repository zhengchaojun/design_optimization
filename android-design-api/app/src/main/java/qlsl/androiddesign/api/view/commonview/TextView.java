package qlsl.androiddesign.api.view.commonview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnLongClickListener;

import qlsl.androiddesign.api.activity.baseactivity.FunctionActivity;
import qlsl.androiddesign.api.popupwindow.subwindow.WidgetPopupWindow;
import qlsl.androiddesign.api.util.commonutil.Log;


/**
 * 艺术字体TextView<br/>
 * 自动播放语音<br/>
 */
public class TextView extends android.widget.TextView implements OnLongClickListener {

    public TextView(Context context) {
        super(context);
        init();
    }

    public TextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        if (Log.isDebug) {
            setOnLongClickListener(this);
        }
    }

    public void setClickable(boolean clickable) {
        super.setClickable(true);
    }

    public boolean onLongClick(View v) {
        FunctionActivity activity = (FunctionActivity) getContext();
        if (activity != null) {
            WidgetPopupWindow widgetWindow = new WidgetPopupWindow(activity, this);
            widgetWindow.showAsCenter(v);
        }
        return false;
    }

}
