package qlsl.androiddesign.api.activity.commonactivity;

import android.view.View;
import android.widget.TextView;

import com.cx.annotation.apt.InstanceFactory;
import com.cx.annotation.aspect.SingleClick;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.activity.baseactivity.CommonActivityBase;
import qlsl.androiddesign.api.http.service.commonservice.ACopyService;
import qlsl.androiddesign.api.manager.AppMethod;

/**
 * 类描述：关于我们页
 * 需要传入的键：****
 * 传入的值类型：****
 * 传入的值含义：****
 * 是否必传：****
 * 作者：郑朝军 on 2017/6/14 11:29
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/14 11:29
 * 修改备注：
 */
@InstanceFactory
public class AboutActivity extends CommonActivityBase<ACopyService>
{
    @Override
    public int getLayoutId()
    {
        return R.layout.activity_about;
    }

    @Override
    public boolean useEventBus()
    {
        return false;
    }

    @Override
    public ACopyService newP()
    {
        return new ACopyService();
    }

    @Override
    protected void initView()
    {
        setTitle("关于我们");
    }


    @Override
    protected void initData()
    {
        setVersionInfo();
    }

    @Override
    protected void initListener()
    {

    }

    @SingleClick
    public void onClick(View view)
    {
        switch (view.getId())
        {
            default:
                break;
        }
    }

    public void showNetWorkSucceedData(String method, Object values)
    {

    }

    public <T> void showData(T... t)
    {

    }

    public <T> void showNoData(T... t)
    {

    }

    private void setVersionInfo()
    {
        TextView tv_version = (TextView) findViewById(R.id.tv_version);
        String versionName = AppMethod.getVersionName();
        tv_version.setText("版本  " + versionName);
    }
}
