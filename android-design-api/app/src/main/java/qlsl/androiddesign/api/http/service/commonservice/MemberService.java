package qlsl.androiddesign.api.http.service.commonservice;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cx.annotation.aspect.CheckNetWork;

import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import qlsl.androiddesign.api.activity.baseactivity.FunctionActivity;
import qlsl.androiddesign.api.config.ZConfig;
import qlsl.androiddesign.api.entity.commonentity.User;
import qlsl.androiddesign.api.http.xutils.HttpProtocol;
import qlsl.androiddesign.api.method.UserMethod;
import qlsl.androiddesign.api.util.commonutil.Log;

/**
 * 系统模块：个人中心模块
 * 作者：郑朝军 on 2017/3/27 22:54
 * 邮箱：1250393285@qq.com
 */

public class MemberService<T extends FunctionActivity> extends SystemService<T>
{
    private String className = getClassName(MemberService.class);

    /**
     * 用户登录
     *
     * @param username
     * @param password
     */
    @CheckNetWork
    public void loginByEmail(final String username, final String password)
    {
        final String method = "loginByEmail";
        getV().showProgressBar();
        Flowable.create(new FlowableOnSubscribe<Object>()
        {
            @Override
            public void subscribe(FlowableEmitter<Object> e) throws Exception
            {
                HttpProtocol protocol = new HttpProtocol();
                try
                {

                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                    outputException(e, exception, className);
                }
            }
        }, BackpressureStrategy.BUFFER)
                .compose(getScheduler())
                .compose(getV().bindToLifecycle())
                .subscribe(createSubscriber(method));
    }


    /**
     * 获取用户信息
     */
    @CheckNetWork
    public void getUserInfo()
    {
        final String method = "getUserInfo";
        getV().showProgressBar();
        Flowable.create(new FlowableOnSubscribe<Object>()
        {
            @Override
            public void subscribe(FlowableEmitter<Object> e) throws Exception
            {
                HttpProtocol protocol = new HttpProtocol();
                try
                {
                    JSONObject jo = null;

                    Log.e("zcj", "getUserInfo===jo===========" + jo);
                    if (isDataInvalid(jo, e))
                    {
                        return;
                    }

                    List<User> list = JSONArray.parseArray(
                            jo.getString("row"), User.class);

                    User user = list.get(0);
                    UserMethod.setUser(user);

                    e.onNext(list);
                    e.onComplete();
                    outputMessageInfo(protocol, jo, className, method);
                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                    outputException(e, exception, className);
                }
            }
        }, BackpressureStrategy.BUFFER)
                .compose(getScheduler())
                .compose(getV().bindToLifecycle())
                .subscribe(createSubscriber(method));
    }

    /**
     * 获取缓存信息
     */
    @CheckNetWork
    public void getSysCache()
    {
        final String method = "getSysCache";
        getV().showProgressBar();
        Log.cx("getSysCache===showProgressBar===");
        Flowable.create(new FlowableOnSubscribe<Object>()
        {
            @Override
            public void subscribe(FlowableEmitter<Object> e) throws Exception
            {
                HttpProtocol protocol = new HttpProtocol();
                try
                {

                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                    outputException(e, exception, className);
                }
            }
        }, BackpressureStrategy.BUFFER)
                .compose(getScheduler())
                .compose(getV().bindToLifecycle())
                .subscribe(createSubscriber(method));
    }

    /**
     * 获取本地缓存信息
     */
    @CheckNetWork
    public void getLocalSysCache()
    {
        final String method = "getLocalSysCache";
        getV().showProgressBar();
        Flowable.create(new FlowableOnSubscribe<Object>()
        {
            @Override
            public void subscribe(FlowableEmitter<Object> e) throws Exception
            {
                HttpProtocol protocol = new HttpProtocol();
                try
                {


                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                    outputException(e, exception, className);
                }
            }
        }, BackpressureStrategy.BUFFER)
                .compose(getScheduler())
                .compose(getV().bindToLifecycle())
                .subscribe(createSubscriber(method));
    }

    /**
     * 查找联系人,从SP里面查询
     */
    public void queryContact()
    {
        final String method = "queryContact";
        getV().showProgressBar();
        Flowable.create(new FlowableOnSubscribe<Object>()
        {
            @Override
            public void subscribe(FlowableEmitter<Object> e) throws Exception
            {
                try
                {

                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                    outputException(e, exception, className);
                }
            }
        }, BackpressureStrategy.BUFFER)
                .compose(getScheduler())
                .compose(getV().bindToLifecycle())
                .subscribe(createSubscriberDb(method));
    }

    public void initTB()
    {

    }
}
