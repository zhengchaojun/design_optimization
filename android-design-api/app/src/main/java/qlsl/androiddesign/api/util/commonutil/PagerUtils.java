package qlsl.androiddesign.api.util.commonutil;


import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import qlsl.androiddesign.api.constant.SoftwareConstant;
import qlsl.androiddesign.api.entity.commonentity.Pager;

public class PagerUtils
{

    /**
     * 对已有List进行分页截取<br/>
     * 每页固定为15条<br/>
     */
    public static <T> Map<String, Object> split(List<T> list, int pageNo)
    {
        int pageSize = 15;
        int totalCount = list.size();
        int totalPage = totalCount / pageSize;
        int start = pageNo * pageSize;
        int end = (pageNo + 1) * pageSize;
        int size = list.size();
        Map<String, Object> map = new HashMap<String, Object>();
        List<T> split_list = list.subList(start, end <= size ? end : size);
        Pager pager = new Pager();
        pager.setPageNo(pageNo);
        pager.setPageSize(pageSize);
        pager.setTotalCount(totalCount);
        pager.setTotalPage(totalPage == 0 ? 0
                : (totalCount % pageSize == 0 ? totalPage - 1 : totalPage));
        map.put("pager", pager);
        map.put("list", split_list);
        return map;
    }

    /**
     * 创建Pager<br/>
     */
    public static Pager createPager(int pageNo, int pageSize, int totalCount,
                                    int totalPage)
    {
        Pager pager = new Pager();
        pager.setPageNo(pageNo);
        pager.setPageSize(pageSize);
        pager.setTotalCount(totalCount);
        pager.setTotalPage(totalPage);
        return pager;
    }

    /**
     * 创建Pager<br/>
     * page从0开始<br/>
     */
    public static Pager createPagerOld(int page, JSONObject jo)
    {
        int pageNo = page;// 1
        int pageSize = SoftwareConstant.PAGER_SIZE;// 15
        int totalCount = jo.getInteger("total");// 70 第二种情况等于2条数据
        int totalPage = totalCount / SoftwareConstant.PAGER_SIZE;// 4.66666666
        // 第二种情况等于0
        totalPage = (totalPage == 0 ? 0
                : (totalCount % pageSize == 0 ? totalPage - 1 : totalPage));
        Pager pager = createPager(pageNo, pageSize, totalCount, totalPage);
        return pager;
    }

    /**
     * 创建Pager<br/>
     * page从1开始<br/>
     */
    public static Pager createPager(int page, JSONObject jo)
    {
        int pageNo = page;// 1
        int pageSize = SoftwareConstant.PAGER_SIZE;// 15
        int totalCount = jo.getInteger("total");// 70 第二种情况等于2条数据
        int totalPage = totalCount / SoftwareConstant.PAGER_SIZE;// 4.66666666
        // 第二种情况等于0
        totalPage = (totalPage == 0 ? 0
                : (totalCount % pageSize == 1 ? totalPage : totalPage + 1));// 4.66666666666约等于5
        // 第二种情况等于0页
        Pager pager = createPager(pageNo, pageSize, totalCount, totalPage);
        return pager;
    }

    /**
     * 创建Pager<br/>
     * page从0开始<br/>
     */
    public static Pager createPagerOld(int page, int totalCount)
    {
        int pageNo = page;// 1
        int pageSize = SoftwareConstant.PAGER_SIZE;
        int totalPage = totalCount / SoftwareConstant.PAGER_SIZE;
        totalPage = (totalPage == 0 ? 0
                : (totalCount % pageSize == 0 ? totalPage - 1 : totalPage));
        Pager pager = createPager(pageNo, pageSize, totalCount, totalPage);
        return pager;
    }

    /**
     * 创建Pager<br/>
     * page从1开始<br/>
     */
    public static Pager createPager(int page, int totalCount)
    {
        int pageNo = page;// 1
        int pageSize = SoftwareConstant.PAGER_SIZE;// 15
        int totalPage = totalCount / SoftwareConstant.PAGER_SIZE;// 4.66666666
        // 第二种情况等于0
        totalPage = (totalPage == 0 ? 0
                : (totalCount % pageSize == 0 ? totalPage - 1 : totalPage));
        Pager pager = createPager(pageNo, pageSize, totalCount, totalPage);
        return pager;
    }

}
