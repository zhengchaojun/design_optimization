package qlsl.androiddesign.api.activity.commonactivity;

import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.activity.baseactivity.CommonActivityBase;
import qlsl.androiddesign.api.adapter.baseadapter.FragmentPagerAdapter;
import qlsl.androiddesign.api.config.ZConfig;
import qlsl.androiddesign.api.constant.Constant;
import qlsl.androiddesign.api.fragment.commonfragment.FragmentTab1;
import qlsl.androiddesign.api.fragment.commonfragment.FragmentTab2;
import qlsl.androiddesign.api.fragment.commonfragment.FragmentTab3;
import qlsl.androiddesign.api.fragment.commonfragment.FragmentTab4;
import qlsl.androiddesign.api.fragment.commonfragment.FragmentTab5;
import qlsl.androiddesign.api.http.service.commonservice.MainService;
import qlsl.androiddesign.api.listener.OnPageChangeListener;
import qlsl.androiddesign.api.manager.ActivityManager;
import qlsl.androiddesign.api.manager.ExitManager;
import qlsl.androiddesign.api.util.commonutil.Log;
import qlsl.androiddesign.api.view.rippleview.Titanic;
import qlsl.androiddesign.api.view.rippleview.TitanicTextView;

/**
 * 作者：郑朝军 on 2017/4/6 14:04
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */

public class MainActivity extends CommonActivityBase<MainService> implements NavigationView
        .OnNavigationItemSelectedListener
{
    NavigationView navView;

    DrawerLayout drawerLayout;

    ViewPager viewPager;

    ViewGroup tabParent;

    private List<Fragment> list;

    private int preIndex;
    @Override
    public int getLayoutId()
    {
        return R.layout.activity_main;
    }

    @Override
    public boolean useEventBus()
    {
        return false;
    }

    @Override
    public MainService newP()
    {
        return new MainService(this);
    }

    @Override
    protected void initView()
    {
        setDrawerLock();
        ActivityManager.getInstance().popActivitysUnderTheSpecify(MainActivity.class);
//        setRootView();
        hideBackButton();
        viewPager.setOffscreenPageLimit(5);

        list = new ArrayList<>();
        list.add(new FragmentTab1());
        list.add(new FragmentTab2());
        list.add(new FragmentTab3());
        list.add(new FragmentTab4());
        list.add(new FragmentTab5());
        FragmentPagerAdapter pagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager(), list);
        viewPager.setAdapter(pagerAdapter);

    }


    @Override
    protected void initData()
    {

    }

    @Override
    protected void initListener()
    {
        navView.setNavigationItemSelectedListener(this);
        viewPager.setOnPageChangeListener(pageListener);
    }

    public void onClick(View view)
    {
        if (view.getParent() == tabParent)
        {
            int index = tabParent.indexOfChild(view);
            viewPager.setCurrentItem(index);
        }
        else
        {
            if (view.getId() == R.id.btn_right)
            {
                startActivity(MemberCenterActivity.class);
                return;
            }
        }
    }

    @Override
    public void onBackPressed()
    {
        if (drawerLayout.isDrawerOpen(GravityCompat.START))
        {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        else
        {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        switch (id)
        {
            case R.id.nav_film:
            {
                Log.e(ZConfig.LOG_TAG, "Constant.province===" + Constant.province);
                Log.e(ZConfig.LOG_TAG, "Constant.city===" + Constant.city);
                Log.e(ZConfig.LOG_TAG, "Constant.area===" + Constant.area);
                Log.e(ZConfig.LOG_TAG, "Constant.latitude===" + Constant.latitude);
                Log.e(ZConfig.LOG_TAG, "Constant.longitude===" + Constant.longitude);
                Log.e(ZConfig.LOG_TAG, "Constant.address===" + Constant.address);


//                showToast("MainActivitySize===" + ActivityManager.getInstance().getActivitySize());
                showToast("province===" + Constant.province + "city===" + Constant.city + "area===" + Constant.area +
                        "address===" + Constant.address);
//                Router.newIntent(this).to(CityDataPickDemo.class).launch();
            }
            break;
            case R.id.nav_tv:
            {
                startActivity(MainActivity.class);
                showToast("nav_tv");
//                Router.newIntent(this).to(CityDataPickDemo.class).launch();
            }
            break;
            case R.id.nav_variety:
            {

            }
            break;
            case R.id.nav_comic:
            {

            }
            break;
            case R.id.nav_game:
            {

            }
            break;
            case R.id.nav_meizi:
            {

            }
            break;

            case R.id.nav_film1:
            {
            }
            break;
            case R.id.nav_tv1:
            {

            }
            break;
            case R.id.nav_variety1:
            {

            }
            break;
            case R.id.nav_comic1:
            {

            }
            break;
            case R.id.nav_game1:
            {

            }
            break;
            case R.id.nav_meizi1:
            {

            }
            break;
            case R.id.nav_setting:
            {

            }
            break;
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    public void onStop()
    {
        super.onStop();
        stopRippleTabText();
    }

    public void onDestroy()
    {
        super.onDestroy();
        ExitManager.clearStaticData();
//        stopFloatService();

        /**
         * 清理垃圾
         */
        System.gc();

        /**
         * 杀掉进程，会阻止某些OnDestroy函数的正常调用<br/>
         * 如果有推送等后台服务，就不要杀掉进程，但要主动清理静态数据<br/>
         * 不杀掉进程，log4j需要在application之后的界面中初始化<br/>
         * 所以application和IntanceActivity都有对log4j初始化<br/>
         */
        System.exit(0);
    }

    private void setDrawerLock()
    {
//        drawerLayout.setDrawerLockMode(Constant.CLOSE_DRAWERLAYOUT_LOCK_MODE ? DrawerLayout.LOCK_MODE_LOCKED_CLOSED :
//                DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    private void onTabChanged(int index)
    {
        rotateTabIcon(index);
        rippleTabText(index);
//        showTabToast(index);
    }

    /**
     * 底部Tab图标切换<br/>
     */
    private void rotateTabIcon(int index)
    {
        View tab_iv_icon = tabParent.getChildAt(index).findViewById(R.id.tab_iv_icon);
        tab_iv_icon.requestFocus();
        tab_iv_icon.requestFocusFromTouch();
        Animation animation_tab = AnimationUtils.loadAnimation(activity, R.anim.tab_shake);
        animation_tab.reset();
        animation_tab.setFillEnabled(true);
        animation_tab.setFillAfter(true);
    }

    /**
     * 底部Tab文本变色
     */
    private void rippleTabText(int index)
    {
        TitanicTextView tab_tv_text = (TitanicTextView) tabParent.getChildAt(index).findViewById(R.id.tab_tv_text);
        tab_tv_text.setTextColor(activity.getResources().getColorStateList(R.drawable.tab_textcolor));
        stopRippleTabText();
        startRippleTabText(tab_tv_text);
    }

    /**
     * 启动底部Tab文本产生波纹
     */
    private void startRippleTabText(TitanicTextView textView)
    {
        Titanic.getInstance().start(textView);
    }

    /**
     * 停止底部Tab文本产生波纹
     */
    private void stopRippleTabText()
    {
        Titanic.getInstance().cancel();
    }

    /**
     * 控制tab_indicator中tab_toast的显示与隐藏<br/>
     */
    private void showTabToast(int index)
    {
        if (!Constant.showTabToast)
        {
            return;
        }
        for (int childIndex = 0, childCount = tabParent.getChildCount(); childIndex < childCount; childIndex++)
        {
            TextView currentTabToast = (TextView) tabParent.getChildAt(childIndex).findViewById(R.id.tab_toast);
            if (index == childIndex)
            {
                currentTabToast.setVisibility(View.VISIBLE);
            }
            else
            {
                currentTabToast.setVisibility(View.GONE);
            }
        }
    }

    private OnPageChangeListener pageListener = new OnPageChangeListener()
    {

        public void onPageSelected(int index)
        {
            onTabChanged(index);
            preIndex = index;
        }
    };
}
