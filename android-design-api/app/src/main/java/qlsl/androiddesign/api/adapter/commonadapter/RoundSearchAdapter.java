package qlsl.androiddesign.api.adapter.commonadapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.adapter.baseadapter.listviewadapter.ListViewBaseAdapter;

/**
 * 类描述：周边半径搜索设置Adapter
 * 需要传入的键：****
 * 传入的值类型：****
 * 传入的值含义：****
 * 是否必传：****
 * 作者：郑朝军 on 2017/6/14 11:29
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/14 11:29
 * 修改备注：
 */
public class RoundSearchAdapter extends ListViewBaseAdapter<HashMap<String, String>>
{
    public RoundSearchAdapter(Activity activity, List<HashMap<String, String>> list)
    {
        super(activity, list);
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        convertView = getItemView(convertView, R.layout.listitem_member_center, parent);

        TextView tv_text = getView(convertView, R.id.tv_text);
        ImageView iv_choose = getView(convertView, R.id.iv_arrow);
        ImageView iv_icon = getView(convertView, R.id.iv_icon);

        HashMap<String, String> map = getItem(position);
        String text = (String) map.get("text");

        tv_text.setText(text);
//        iv_choose.setVisibility(View.INVISIBLE);
        iv_icon.setVisibility(View.GONE);

        return convertView;
    }


}
