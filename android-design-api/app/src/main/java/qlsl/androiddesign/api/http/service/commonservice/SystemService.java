package qlsl.androiddesign.api.http.service.commonservice;

import com.cx.annotation.aspect.CheckNetWork;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import qlsl.androiddesign.api.activity.baseactivity.FunctionActivity;
import qlsl.androiddesign.api.http.service.baseservice.ServiceBase;

/**
 * 系统模块：系统模块
 * 作者：郑朝军 on 2017/3/27 22:54
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */

public class SystemService<T extends FunctionActivity> extends ServiceBase<T>
{
    private String className = getClassName(SystemService.class);

    /**
     * 版本更新
     */
    @CheckNetWork
    public void getUpdateInfo()
    {
        final String method = "getUpdateInfo";
        getV().showProgressBar();
        Flowable.create(new FlowableOnSubscribe<Object>()
        {
            @Override
            public void subscribe(FlowableEmitter<Object> e) throws Exception
            {
                try
                {


                    e.onComplete();
                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                    outputException(e, exception, className);
                }
            }
        }, BackpressureStrategy.BUFFER)
                .compose(getScheduler())
                .compose(getV().bindToLifecycle())
                .subscribe(createSubscriber(method));
    }

}
