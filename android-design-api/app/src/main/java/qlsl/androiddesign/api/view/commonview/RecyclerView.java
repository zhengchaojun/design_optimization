package qlsl.androiddesign.api.view.commonview;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.AttributeSet;

/**
 * 类描述：包含布局管理器
 * 需要传入的键：****
 * 传入的值类型：****
 * 传入的值含义：****
 * 是否必传：****
 * 作者：郑朝军 on 2017/6/14 11:29
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/14 11:29
 * 修改备注：
 */
public class RecyclerView extends android.support.v7.widget.RecyclerView
{
    public RecyclerView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public RecyclerView(Context context)
    {
        super(context);
    }

    public RecyclerView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    public RecyclerView verticalLayoutManager(Context context)
    {
        LinearLayoutManager manager = new LinearLayoutManager(context);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        setLayoutManager(manager);
        return this;
    }

    public RecyclerView horizontalLayoutManager(Context context)
    {
        LinearLayoutManager manager = new LinearLayoutManager(context);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        setLayoutManager(manager);
        return this;
    }

    public RecyclerView gridLayoutManager(Context context, int spanCount)
    {
        GridLayoutManager manager = new GridLayoutManager(context, spanCount);
        setLayoutManager(manager);
        return this;
    }

    public RecyclerView verticalStaggeredLayoutManager(int spanCount)
    {
        StaggeredGridLayoutManager manager = new StaggeredGridLayoutManager(spanCount,
                StaggeredGridLayoutManager.VERTICAL);
        setLayoutManager(manager);
        return this;
    }

    public RecyclerView horizontalStaggeredLayoutManager(int spanCount)
    {
        StaggeredGridLayoutManager manager = new StaggeredGridLayoutManager(spanCount,
                StaggeredGridLayoutManager.VERTICAL);
        setLayoutManager(manager);
        return this;
    }

}