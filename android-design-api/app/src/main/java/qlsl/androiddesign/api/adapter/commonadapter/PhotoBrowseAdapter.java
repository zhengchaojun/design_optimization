package qlsl.androiddesign.api.adapter.commonadapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.File;
import java.util.List;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.activity.baseactivity.FunctionActivity;
import qlsl.androiddesign.api.adapter.baseadapter.BasePagerAdapter;
import qlsl.androiddesign.api.util.commonutil.ImageUtils;


/**
 * 兼容网络地址<br/>
 */
public class PhotoBrowseAdapter extends BasePagerAdapter<String>
{
    public PhotoBrowseAdapter(FunctionActivity activity, List<String> list)
    {
        super(activity, list);
    }

    public View instantiateItem(ViewGroup container, int position)
    {
        View convertView = getItemView(container,
                R.layout.listitem_watermark_photo_browse);

        ImageView imageView = getView(convertView, R.id.photoView);

        String url = getItem(position);

        if (url.contains("storage"))
        {
            ImageUtils.rectOri(activity, new File(url), imageView);
        }
        else
        {
            ImageUtils.rectOri(activity, url, imageView);
        }
        return convertView;
    }

}
