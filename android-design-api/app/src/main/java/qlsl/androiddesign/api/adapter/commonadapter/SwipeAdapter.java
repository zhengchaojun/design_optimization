package qlsl.androiddesign.api.adapter.commonadapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.adapter.baseadapter.listviewadapter.ListViewBaseAdapter;

public class SwipeAdapter extends ListViewBaseAdapter<HashMap<String, Object>>
{
    public SwipeAdapter(Activity activity, List<HashMap<String, Object>> list)
    {
        super(activity, list);
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        convertView = getItemView(convertView, R.layout.listitem_chat_queue, parent);

        ImageView iv_icon = getView(convertView, R.id.iv_icon);
        TextView tv_name = getView(convertView, R.id.tv_name);
        TextView tv_content = getView(convertView, R.id.tv_content);
        TextView tv_time = getView(convertView, R.id.tv_time);


        HashMap<String, Object> map = getItem(position);
        String text = (String) map.get("text");
        String value = (String) map.get("value");

        tv_name.setText(text);
        tv_content.setText(value);

        return convertView;
    }
}
