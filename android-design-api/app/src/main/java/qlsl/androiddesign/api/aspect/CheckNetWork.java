package qlsl.androiddesign.api.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

import qlsl.androiddesign.api.constant.MessageConstant;
import qlsl.androiddesign.api.manager.ActivityManager;
import qlsl.androiddesign.api.method.HttpMethod;
import qlsl.androiddesign.api.util.commonutil.ToastUtils;

/**
 * 类描述：检测是否有网络
 * 需要传入的键：****
 * 传入的值类型：****
 * 传入的值含义：****
 * 是否必传：****
 * 作者：郑朝军 on 2017/6/14 11:29
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/14 11:29
 * 修改备注：
 */
@Aspect
public class CheckNetWork
{
    @Pointcut("execution(@com.cx.annotation.aspect.CheckNetWork * *(..))")//方法切入点
    public void methodPointcut()
    {
    }

    @Around("methodPointcut()")//在连接点进行方法替换
    public Object aroundJoinPoint(ProceedingJoinPoint joinPoint) throws Throwable
    {
        if (!HttpMethod.isNetworkConnect())
        {
            ToastUtils.showSnackbar(ActivityManager.getInstance().currentActivity(), MessageConstant.MSG_CLIENT_FAILED, true);
            return null;
        }
        Object result = joinPoint.proceed();//执行原方法
        return result;
    }
}

