package qlsl.androiddesign.api.activity.commonactivity;

import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.activity.baseactivity.CommonActivityBase;
import qlsl.androiddesign.api.constant.Constant;
import qlsl.androiddesign.api.entity.commonentity.FragmentArgs;
import qlsl.androiddesign.api.listener.WebViewLoadListener;
import qlsl.androiddesign.api.util.other.Utils;

/**
 * 功能：最基本的webviwe
 * 作者：郑朝军 on 2017/4/17 18:34
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */

public class WebViewActivity extends CommonActivityBase
{
    protected WebView webView;

    protected Utils utils = Utils.getInstance();

    protected FragmentArgs item;

    @Override
    public int getLayoutId()
    {
        return R.layout.activity_web;
    }

    @Override
    public boolean useEventBus()
    {
        return false;
    }

    @Override
    public Object newP()
    {
        return null;
    }

    @Override
    protected void initView()
    {
        webView = (WebView) findView(R.id.webView);
        item = (FragmentArgs) utils.getItem(activity);
        if (item == null)
        {
            setTitle("数据浏览");
        }
        else
        {
            setTitle("" + (item.get("title") == null ? "数据浏览" : item.get("title")));
        }
        webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webView.getSettings().setLayoutAlgorithm(
                WebSettings.LayoutAlgorithm.NORMAL);
        webView.getSettings().setUseWideViewPort(false);
        webView.setHorizontalScrollBarEnabled(false);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.getSettings().setDefaultTextEncodingName("utf-8");
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.setWebViewClient(new WebViewLoadListener());
        showProgressBar();
    }

    @Override
    protected void initData()
    {
        setWebViewData();
    }

    @Override
    protected void initListener()
    {

    }

    protected void setWebViewData()
    {
        webView.loadUrl("" + (item.get("url") == null ? Constant.ZCJ_GIT_HOME : item.get("url")));
    }
}
