package qlsl.androiddesign.api.service.baseservice;

import android.app.Service;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.support.annotation.IdRes;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;

import org.greenrobot.eventbus.EventBus;

import qlsl.androiddesign.api.activity.baseactivity.CommonContract;
import qlsl.androiddesign.api.util.commonutil.Log;
import qlsl.androiddesign.api.util.commonutil.ToastUtils;

/**
 * 类描述：四大组件之服务基类
 * 需要传入的键：****
 * 传入的值类型：****
 * 传入的值含义：****
 * 是否必传：****
 * 作者：郑朝军 on 2017/6/14 16:18
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/14 16:18
 * 修改备注：
 */
public abstract class BaseService<P extends CommonContract.IService> extends Service implements
        CommonContract.CommonView<P>
{
    private P p;

    public void onCreate()
    {
        super.onCreate();
        outputCreateServiceInfo();
        bindEvent();
    }

    public IBinder onBind(Intent intent)
    {
        outputOnBindServiceInfo();
        return null;
    }

    public boolean onUnbind(Intent intent)
    {
        outputOnUnbindServiceInfo();
        return super.onUnbind(intent);
    }

    public void onDestroy()
    {
        super.onDestroy();
        outputonDestroyServiceInfo();

        if (getP() != null)
        {
            getP().detachV();
        }
        p = null;
    }

    private void outputCreateServiceInfo()
    {
        Log.i("onCreate：Service：<br/>" + getClass().getName());
    }

    private void outputOnBindServiceInfo()
    {
        Log.i("OnBind：Service：<br/>" + getClass().getName());
    }

    private void outputOnUnbindServiceInfo()
    {
        Log.i("OnUnbind：Service：<br/>" + getClass().getName());
    }

    private void outputonDestroyServiceInfo()
    {
        Log.i("onDestroy：Service：<br/>" + getClass().getName());
    }

    @Override
    public void bindUI(View rootView)
    {

    }

    @Override
    public void bindEvent()
    {
        if (useEventBus())
        {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public int getLayoutId()
    {
        return 0;
    }

    @Override
    public boolean useEventBus()
    {
        return false;
    }

    protected P getP()
    {
        if (p == null)
        {
            p = newP();
            if (p != null)
            {
                p.attachV(this);
            }
        }
        return p;
    }
    //------------------------------------以下为提供的API----------------------------------------

    /**
     * 创建悬浮窗
     */
    public View createFloatView(WindowManager wm, LayoutParams wmParams, int gravity, int resource)
    {
        wmParams.type = LayoutParams.TYPE_PHONE;
        wmParams.flags = LayoutParams.FLAG_NOT_TOUCH_MODAL
                | LayoutParams.FLAG_NOT_FOCUSABLE;
        wmParams.gravity = gravity;
        wmParams.x = 0;
        wmParams.y = 0;
        wmParams.format = PixelFormat.RGBA_8888;

        View view = LayoutInflater.from(getApplicationContext()).inflate(
                resource, null);
        wm.addView(view, wmParams);
        return view;
    }

    @Override
    public void hideProgressBar()
    {

    }

    @Override
    public void showProgressBar()
    {

    }

    public <V extends View> V findView(@IdRes int id)
    {
        View rootView = getRootView();
        if (rootView != null)
        {
            return (V) rootView.findViewById(id);
        }
        return null;
    }

    private View getRootView()
    {
        return null;
    }

    /**
     * 显示长Toast
     */
    public void showToast(String text)
    {
        if (!TextUtils.isEmpty(text))
        {
            ToastUtils.showLongToast(text);
        }
    }

    /**
     * 显示短Toast
     */
    public void showShortToast(String text)
    {
        if (!TextUtils.isEmpty(text))
        {
            ToastUtils.showShortToast(text);
        }
    }
    //------------------------------------接口方法--网络层----数据库层等其他回调方法----------------------------------------

    /**
     * 显示数据:<br/>
     * 待接收对象：showData函数中的t容器对象<br/>
     */
    @SuppressWarnings("hiding")
    public void showNetWorkSucceedData(String method, Object values)
    {

    }

    /**
     * 提示没有数据:<br/>
     * 待接收对象：showNoData函数中的t容器对象<br/>
     */
    @SuppressWarnings("hiding")
    public void showNetWorkFaildData(String method, Object values)
    {
        ToastUtils.showNoDataToast(values.toString());
    }

    /**
     * 显示数据库数据:<br/>
     * 待接收对象：showData函数中的t容器对象<br/>
     */
    @SuppressWarnings("hiding")
    public void showDbSucceedData(String method, Object values)
    {

    }

    /**
     * 提示数据库没有数据:<br/>
     * 待接收对象：showNoData函数中的t容器对象<br/>
     */
    @SuppressWarnings("hiding")
    public void showFaildDbData(String method, Object values)
    {
        ToastUtils.showNoDataToast(values.toString());
    }

    /**
     * 显示其他数据:<br/>
     * 待接收对象：showData函数中的t容器对象<br/>
     */
    @SuppressWarnings("hiding")
    public void showOtherSucceedData(String method, Object values)
    {

    }

    /**
     * 提示其他没有数据:<br/>
     * 待接收对象：showData函数中的t容器对象<br/>
     */
    @SuppressWarnings("hiding")
    public void showFaildOtherData(String method, Object values)
    {

    }

    public <T extends Object> void showData(T... t)
    {

    }

    public <T extends Object> void showNoData(T... t)
    {

    }
}
