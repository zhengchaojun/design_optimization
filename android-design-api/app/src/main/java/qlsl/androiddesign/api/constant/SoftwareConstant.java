package qlsl.androiddesign.api.constant;

public interface SoftwareConstant {

	/**
	 * 分页数量<br/>
	 */
	int PAGER_SIZE = 15;

	/**
	 * 空数据标记位<br/>
	 */
	String NULL = "null";

	/**
	 * 分页初始值<br/>
	 */
	int PAGER_INIT_INDEX = 1;
	
	/**
	 * sdcard路径标记<br/>
	 * 用于区分网络路径与设备路径<br/>
	 */
	String SDCARD_FLAG = "storage";
}
