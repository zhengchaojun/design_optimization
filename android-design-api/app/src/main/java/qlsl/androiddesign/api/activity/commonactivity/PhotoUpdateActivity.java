package qlsl.androiddesign.api.activity.commonactivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.View;
import android.widget.BaseAdapter;

import com.alibaba.fastjson.JSONObject;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.adapter.commonadapter.PictureUpdateAdapter;
import qlsl.androiddesign.api.constant.IntentCodeConstant;
import qlsl.androiddesign.api.http.service.commonservice.ACopyService;
import qlsl.androiddesign.api.http.xutils.HttpProtocol;
import qlsl.androiddesign.api.listener.OnUploadFileListener;
import qlsl.androiddesign.api.util.commonutil.WindowsUtils;

/**
 * 类描述：公用图片添加,删除,修改,查看页(网络本地混合)
 * 需要传入的键：max_count,urls,type,photoids
 * 传入的值类型：int,List <String> ,int,List
 * 传入的值含义：最大图片个数,图片流,type:[0,1]表示[二手车，商品],图片id表
 * 是否必传：否，是，是，是
 * 作者：郑朝军 on 2017/6/14 11:29
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/14 11:29
 * 修改备注：
 */
public class PhotoUpdateActivity extends GridViewBaseActivity<String, ACopyService>
{
    private int max_count = 9;

    private int position;

    private String destPath;

    @Override
    public int getLayoutId()
    {
        return R.layout.common_grid_view_btn;
    }

    @Override
    protected void initView()
    {
        super.initView();
        setTitle("修改图片");
        setBtnText("确定");
        list = (List<String>) activity.getIntent().getSerializableExtra("urls");
        list = list == null ? new ArrayList<String>() : list;
        int count = activity.getIntent().getIntExtra("max_count", max_count);
        for (int index = list.size(); index < count; index++)
        {
            list.add(null);
        }
    }


    @Override
    protected void initData()
    {
        super.initData();
    }

    @Override
    protected void initListener()
    {

    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.grid_item:
                doClickGridItem(view);
                break;
            case R.id.btn_ok:
                doClickOKView();
                break;
        }
    }

    public void showNetWorkSucceedData(String method, Object values)
    {

    }

    public <T> void showData(T... t)
    {

    }

    public <T> void showNoData(T... t)
    {

    }

    @Override
    public BaseAdapter getAdapter()
    {
        return new PictureUpdateAdapter(activity, list);
    }

    private void doClickGridItem(View view)
    {
        startActivityForResult(WindowsUtils.getImageFileIntent(), IntentCodeConstant
                .REQUEST_CODE_PHOTO_PICKED);
    }

    private void doClickOKView()
    {
        Intent intent = new Intent();
        intent.putExtra("urls", (Serializable) list);
        activity.setResult(0, intent);
        activity.finish();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == IntentCodeConstant.REQUEST_CODE_PHOTO_PICKED && resultCode == Activity
                .RESULT_OK && data != null)
        {
            Uri uri = data.getData();
            if (uri == null)
            {
                showToast("无法加载图片");
                return;
            }
            cropPhoto(WindowsUtils.getImagePathForUri(activity, uri));
        }
        else if (requestCode == IntentCodeConstant.CROP_PHOTO)
        {
            if (data == null)
            {
                return;
            }
            Bitmap bitmap = data.getExtras().getParcelable("data");
            File file = saveBitmapToFile(bitmap);
            new HttpProtocol().setMethod("uploadheadphoto").uploadFile(file, null, listener);
        }
    }

    private OnUploadFileListener listener = new OnUploadFileListener()
    {
        public void onSuccess(JSONObject jo)
        {

        }
    };

}
