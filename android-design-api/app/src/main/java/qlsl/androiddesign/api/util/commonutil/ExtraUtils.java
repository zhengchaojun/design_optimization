package qlsl.androiddesign.api.util.commonutil;

import android.os.Bundle;
import android.text.TextUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by Administrator on 2016/9/6 0006.
 */
public class ExtraUtils implements Serializable
{
    private static final long serialVersionUID = 5526514482404853100L;
    private Map<String, Serializable> values = new HashMap();

    public ExtraUtils()
    {
    }

    public ExtraUtils add(String key, Serializable value)
    {
        if (!TextUtils.isEmpty(key) && value != null)
        {
            this.values.put(key, value);
        }

        return this;
    }

    public Serializable get(String key)
    {
        return (Serializable) this.values.get(key);
    }

    public static void setToBundle(Bundle bundle, ExtraUtils args)
    {
        Set keys = args.values.keySet();
        Iterator i$ = keys.iterator();

        while (i$.hasNext())
        {
            String key = (String) i$.next();
            Serializable value = args.get(key);
            if (value != null)
            {
                bundle.putSerializable(key, value);
            }
        }

    }

    public static ExtraUtils transToArgs(Bundle bundle)
    {
        ExtraUtils args = new ExtraUtils();
        Iterator i$ = bundle.keySet().iterator();

        while (i$.hasNext())
        {
            String s = (String) i$.next();
            Object o = bundle.get(s);
            if (o != null)
            {
                args.add(s, (Serializable) o);
            }
        }

        return args;
    }

    public static Bundle transToBundle(ExtraUtils args)
    {
        Bundle bundle = new Bundle();
        setToBundle(bundle, args);
        return bundle;
    }
}
