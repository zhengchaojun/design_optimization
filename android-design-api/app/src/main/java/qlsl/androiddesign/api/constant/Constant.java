package qlsl.androiddesign.api.constant;


/**
 * 功能：应用层级api的配置一般在这
 * 作者：郑朝军 on 2017/3/20 14:49
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
public class Constant
{

//    public static CookieStore COOKIE_STORE = null;


    public static String ZCJ_GIT_HOME = "https://git.oschina.net/dererere/myandroiddesign";//
    // 注意关闭应用的时候要注意销毁



    public static final String TAG_NET = "网络请求返回:";

    public static String APPID;

    public static String APPCODE;

    public static String TOKEN;

    public static boolean showTabToast = true;

    public static String longitude = "114.3690";// 注意关闭应用的时候要注意销毁

    public static String city = "武汉市";// 注意关闭应用的时候要注意销毁

    public static String province = "湖北省";// 注意关闭应用的时候要注意销毁

    public static String area = "孝昌县";// 注意关闭应用的时候要注意销毁

    public static String address = "";// 注意关闭应用的时候要注意销毁

    public static String latitude = "30.410619";// 注意关闭应用的时候要注意销毁

}
