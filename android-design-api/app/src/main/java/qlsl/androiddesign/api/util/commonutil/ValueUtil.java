package qlsl.androiddesign.api.util.commonutil;

import java.util.List;

/**
 * 类描述：
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传：
 * 作者：郑朝军 on 2017/9/7 9:12
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/9/7 9:12
 * 修改备注：
 */
public class ValueUtil
{
    public static boolean isEmpty(Object value)
    {
        return value == null ? true : 0 == value.toString().length();
    }

    public static boolean isListEmpty(List<? extends Object> list)
    {
        return list == null ? true : list.size() == 0;
    }
}
