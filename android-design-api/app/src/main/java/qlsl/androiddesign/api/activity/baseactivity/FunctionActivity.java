package qlsl.androiddesign.api.activity.baseactivity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.support.design.widget.AppBarLayout;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.itheima.pulltorefreshlib.PullToRefreshAdapterViewBase;
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.activity.commonactivity.PhotoBrowseActivity;
import qlsl.androiddesign.api.constant.IntentCodeConstant;
import qlsl.androiddesign.api.manager.ActivityManager;
import qlsl.androiddesign.api.service.baseservice.BaseService;
import qlsl.androiddesign.api.util.commonutil.Log;
import qlsl.androiddesign.api.util.commonutil.SDCardUtils;
import qlsl.androiddesign.api.util.commonutil.SystemBarTintManager;
import qlsl.androiddesign.api.util.commonutil.ToastUtils;

/**
 * 提供给子类方法调用
 * 作者：郑朝军 on 2017/3/18 11:33
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
public abstract class FunctionActivity<P extends CommonContract.IService> extends
        RxAppCompatActivity implements DialogInterface.OnClickListener, CommonContract.CommonView<P>
{
    /**
     * 公用标题栏
     */
    protected Toolbar toolbar;

    /**
     * 返回按钮
     */
    protected AppCompatImageButton btn_back;

    /**
     * 标题栏公用标题控件
     */
    protected TextView tv_title;

    /**
     * btn_right控件
     */
    protected ActionMenuItemView btn_right;

    /**
     * iv_border控件
     */
    protected ActionMenuItemView iv_border;

    /**
     * BaseActivity及BaseFragment的内容布局
     */
    protected ViewGroup contentView;

    /**
     * 进度条parent
     */
    protected ViewGroup progressBar;

    /**
     * 进度条文本
     */
    protected TextView tv_progressbar;

    /**
     * 设置BaseActivity子类的内容布局
     */
    public void setFunctionContentView(int resource)
    {
        super.setContentView(R.layout.common_root_fit);
        findRootView();
        ViewGroup commoon_content = (ViewGroup) findViewById(R.id.viewgroup_common_content);
        contentView = (ViewGroup) getLayoutInflater().inflate(resource, null);
        contentView.setId(R.id.contentView);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        commoon_content.addView(contentView, params);
        init();
    }

    /**
     * 设置BaseActivity子类的内容布局
     */
    public void setFunctionContentView(ViewGroup view)
    {
        super.setContentView(R.layout.common_root_fit);
        findRootView();
        ViewGroup commoon_content = (ViewGroup) findViewById(R.id.viewgroup_common_content);
        contentView = view;
        contentView.setId(R.id.contentView);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        commoon_content.addView(contentView, params);
        init();
    }

    /**
     * 设置所有BaseActivity子类的根布局为layout中的common_root.xml布局文件<br/>
     */
    @SuppressWarnings("ResourceType")
    public void findRootView()
    {
        toolbar = (Toolbar) findViewById(R.id.title_view);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setTitle("");
        toolbar.inflateMenu(R.menu.title_right);
        btn_back = (AppCompatImageButton) toolbar.getChildAt(1);
        btn_back.setId(R.id.btn_back);
        tv_title = (TextView) findViewById(R.id.tv_title);
        btn_right = (ActionMenuItemView) findViewById(R.id.btn_right);
        iv_border = (ActionMenuItemView) findViewById(R.id.iv_border);
        btn_right.setVisibility(View.GONE);
        iv_border.setVisibility(View.GONE);
        btn_right.setTextColor(getResources().getColorStateList(R.drawable.selector_text_color));
        iv_border.setTextColor(getResources().getColorStateList(R.drawable.selector_text_color));
        progressBar = (ViewGroup) findViewById(R.id.viewgroup_progressbar);
        tv_progressbar = (TextView) progressBar.getChildAt(1);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                finish();
            }
        });
        btn_right.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                doClickRightButton(view);
            }
        });
        iv_border.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                doClickBorderView(view);
            }
        });
    }

    /**
     * 查找和添加公有控件，初始化数据及监听<br/>
     */
    public void init()
    {
        initView();
        initData();
        initListener();
    }

    /**
     * 用于控制标题栏及初始化控件<br/>
     */
    protected abstract void initView();


    /**
     * 用于初始化数据<br/>
     */
    protected abstract void initData();


    /**
     * 用于设置事件监听<br/>
     */
    protected abstract void initListener();

    //-----------------------------------------------------以下为标题栏设置方法----------------------------------------

    /**
     * btn_right点击事件<br/>
     */
    protected void doClickRightButton(View view)
    {
        Toast.makeText(this, "正在开发中", Toast.LENGTH_SHORT).show();
    }

    /**
     * iv_border点击事件<br/>
     */
    protected void doClickBorderView(View view)
    {
        Toast.makeText(this, "正在开发中", Toast.LENGTH_SHORT).show();
    }

    /**
     * 显示标题栏<br/>
     */
    public void showTitleBar()
    {
        toolbar.setVisibility(View.VISIBLE);
    }

    /**
     * 隐藏标题栏<br/>
     */
    public void hideTitleBar()
    {
        toolbar.setVisibility(View.GONE);
    }

    /**
     * 标题栏背景图片<br/>
     */
    public void setTitleBarBackground(int resid)
    {
        toolbar.setBackgroundResource(resid);
    }

    /**
     * 设置标题栏的背景色<br/>
     */
    public void setTitleBarBackgroundResource(int resid)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        SystemBarTintManager tintManager = new SystemBarTintManager(this);
        tintManager.setStatusBarTintEnabled(true);
        tintManager.setStatusBarTintResource(resid);
        toolbar.setBackgroundResource(resid);
    }

    /**
     * 设置标题栏返回按钮的资源图片<br/>
     */
    public void setBackButtonResource(int resid)
    {
        btn_back.setImageResource(resid);
    }

    /**
     * 重置返回按钮的点击事件<br/>
     */
    public void setBackButtonClickListener(View.OnClickListener onClickListener)
    {
        btn_back.setOnClickListener(onClickListener);
    }

    /**
     * 显示标题栏的返回按钮<br/>
     */
    public void showBackButton()
    {
        btn_back.setVisibility(View.VISIBLE);
    }

    /**
     * 隐藏标题栏的返回按钮<br/>
     */
    public void hideBackButton()
    {
        btn_back.setVisibility(View.GONE);
    }

    /**
     * 设置标题栏的标题<br/>
     */
    public void setTitle(String name)
    {
        tv_title.setText(name);
    }

    /**
     * 设置btn_right的资源图片<br/>
     */
    public void setRightButtonResource(int resid)
    {
        btn_right.setIcon(getResources().getDrawable(resid));
    }

    /**
     * 设置btn_right的文本<br/>
     */
    public void setRightButtonText(String text)
    {
        btn_right.setIcon(null);
        btn_right.setText(text);
    }

    /**
     * 显示btn_right<br/>
     */
    public void showRightButton()
    {
        btn_right.setVisibility(View.VISIBLE);
    }

    /**
     * 隐藏btn_right<br/>
     */
    public void hideRightButton()
    {
        btn_right.setVisibility(View.GONE);
    }

    /**
     * 设置iv_border的资源图片<br/>
     */
    public void setBorderViewResource(int resid)
    {
        iv_border.setIcon(getResources().getDrawable(resid));
    }

    /**
     * 设置iv_border的文本<br/>
     */
    public void setBorderViewText(String text)
    {
        iv_border.setIcon(null);
        iv_border.setText(text);
    }

    /**
     * 显示iv_border<br/>
     */
    public void showBorderView()
    {
        iv_border.setVisibility(View.VISIBLE);
    }

    /**
     * 隐藏iv_border<br/>
     */
    public void hideBorderView()
    {
        iv_border.setVisibility(View.GONE);
    }

    /**
     * 设置进度栏文本<br/>
     */
    public void setProgressBarText(String text)
    {
        tv_progressbar.setText(text);
    }

    /**
     * 重置进度栏文本<br/>
     */
    public void resetProgressBarText()
    {
        tv_progressbar.setText(getString(R.string.progressbar_text));
    }

    /**
     * 设置Toolbar允许滚动<br/>
     */
    public void setScrollFlags()
    {
        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL);
    }

    /**
     * 显示进度条及隐藏输入法<br/>
     */
    public void showProgressBar()
    {
        progressBar.setVisibility(View.VISIBLE);
        hideSoftInput();
    }

    /**
     * 设置背景<br/>
     */
    public void setContentBarBackgroundResource(int resid)
    {
        findViewById(R.id.viewgroup_common_content).setBackgroundResource(
                resid);
    }

    /**
     * 设置背景<br/>
     */
    public void setContentBarBackgroundColor(int color)
    {
        findViewById(R.id.viewgroup_common_content).setBackgroundColor(
                color);
    }

    /**
     * 隐藏进度条<br/>
     */
    public void hideProgressBar()
    {
        progressBar.setVisibility(View.GONE);
    }

    //-----------------------------------------------------END结束----------------------------------------

    //-----------------------------------------------------以下为获取标题栏对象方法----------------------------------------

    /**
     * 获取根布局<br/>
     */
    public ViewGroup getRootView()
    {
        return contentView;
    }

    /**
     * 获取内容栏根布局<br/>
     */
    public ViewGroup getContentView()
    {
        ViewGroup parent = (ViewGroup) findViewById(R.id.viewgroup_common_content);
        return (ViewGroup) parent.getChildAt(1);
    }
    //-----------------------------------------------------END结束--------------------------------------------------

    //-----------------------------------------------------通用方法1--------------------------------------------------
    public void onClick(View view)
    {

    }

    /**
     * 隐藏输入法<br/>
     */
    public void hideSoftInput()
    {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context
                .INPUT_METHOD_SERVICE);
        if (getCurrentFocus() != null)
        {
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    /**
     * 显示长Toast<br/>
     */
    public void showToast(String text)
    {
        if (!isEmpty(text))
        {
            ToastUtils.showLongToast(text);
        }
    }

    /**
     * 显示短Toast<br/>
     */
    public void showShortToast(String text)
    {
        if (!isEmpty(text))
        {
            ToastUtils.showShortToast(text);
        }
    }


    /**
     * 查找contentView下的控件<br/>
     */
    public <V extends View> V findView(@IdRes int id)
    {
        V currentView = (V) contentView.findViewById(id);
        return currentView;
    }

    /**
     * 查找指定序号的同名id控件<br/>
     * 序号从1开始<br/>
     */
    public <V extends View> V findView(int id, int order)
    {
        return findView(contentView, id, order, new AtomicInteger(0));
    }

    /**
     * 查找指定序号的同名id控件<br/>
     * 序号从1开始<br/>
     */
    public <V extends View> V findView(ViewGroup rootGroup, int id, int order)
    {
        return findView(rootGroup, id, order, new AtomicInteger(0));
    }

    /**
     * 实现查找指定序号的同名id控件<br/>
     */
    @SuppressWarnings("unchecked")
    private <V extends View> V findView(ViewGroup rootGroup, int id, int order, AtomicInteger
            order_num)
    {
        for (int childIndex = 0, childCount = rootGroup.getChildCount(); childIndex < childCount;
             childIndex++)
        {
            View childView = rootGroup.getChildAt(childIndex);
            if (childView.getId() == id)
            {
                if (order_num.incrementAndGet() == order)
                {
                    return (V) childView;
                }
            }
            if (childView instanceof ViewGroup)
            {
                View resultView = findView((ViewGroup) childView, id, order, order_num);
                if (resultView == null)
                {
                    continue;
                }
                else
                {
                    return (V) resultView;
                }
            }
        }
        return null;
    }

    /**
     * 实现查找指定id所有同名控件<br/>
     */
    @SuppressWarnings("unchecked")
    public void findView(ViewGroup rootGroup, int id, List<View> views)
    {
        for (int childIndex = 0, childCount = rootGroup.getChildCount(); childIndex < childCount;
             childIndex++)
        {
            View childView = rootGroup.getChildAt(childIndex);
            if (childView.getId() == id)
            {
                views.add(childView);
            }
            if (childView instanceof ViewGroup)
            {
                findView((ViewGroup) childView, id, views);
            }
        }
    }

    public boolean isEmpty(CharSequence src)
    {
        return TextUtils.isEmpty(src);
    }

    /**
     * 全屏<br/>
     */
    public void setFullScreen()
    {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    /**
     * 重启该Activity<br/>
     */
    public void reload()
    {
        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
    }

    public void finish()
    {
        super.finish();
        ActivityManager.getInstance().removeActivity(this);
    }

    /**
     * ListView:<br/>
     * 返回BaseAdapter类型<br/>
     * 位置以第一行itemview的位置算起<br/>
     * 用于更新界面<br/>
     * GridView:<br/>
     * 返回BaseAdapter类型<br/>
     */
    public BaseAdapter getBaseAdapter(PullToRefreshAdapterViewBase pullToRefreshAdapterViewBase)
    {
        AbsListView mRefreshableView = (AbsListView) pullToRefreshAdapterViewBase.getRefreshableView();
        if (mRefreshableView instanceof ListView)
        {
            HeaderViewListAdapter headerAdapter = (HeaderViewListAdapter) mRefreshableView.getAdapter();
            if (headerAdapter != null)
            {
                BaseAdapter adapter = (BaseAdapter) headerAdapter.getWrappedAdapter();
                return adapter;
            }
        }
        else if (mRefreshableView instanceof GridView)
        {
            return (BaseAdapter) mRefreshableView.getAdapter();
        }
        return null;
    }

    /**
     * ListView:<br/>
     * 返回HeaderViewListAdapter类型<br/>
     * 位置以headView的位置算起<br/>
     * 用于获取item对象<br/>
     * GridView:<br/>
     * 返回BaseAdapter类型<br/>
     */
    public ListAdapter getAdapter(PullToRefreshAdapterViewBase pullToRefreshAdapterViewBase)
    {
        AbsListView mRefreshableView = (AbsListView) pullToRefreshAdapterViewBase.getRefreshableView();
        if (mRefreshableView instanceof ListView)
        {
            HeaderViewListAdapter headerAdapter = (HeaderViewListAdapter) mRefreshableView
                    .getAdapter();
            return headerAdapter;
        }
        else if (mRefreshableView instanceof GridView)
        {
            return mRefreshableView.getAdapter();
        }
        return null;
    }
    //-----------------------------------------------------通用方法1END--------------------------------------------------

    //-----------------------------------------------------通用方法2-----------------------------------------------------

    /**
     * 在5.0系统之后：onstart方法之后调用的<br/>
     */
    protected void onPostCreate(Bundle savedInstanceState)
    {
        super.onPostCreate(savedInstanceState);
        if (savedInstanceState != null
                && savedInstanceState.getBoolean("needForceExit", true))
        {
            Log.d("检测到窗口在重新创建，正在阻止创建并安全退出");
            Toast.makeText(this, "程序异常，正在逐一退出，请稍等", Toast.LENGTH_LONG).show();
            finish();
        }
    }


    /**
     * 打开相册浏览
     *
     * @param position
     * @param name
     * @param list
     */
    public void startActivityToPhotoBrowseActivity(int position, String name,
                                                   ArrayList<String> list)
    {
        Intent intent = new Intent(this, PhotoBrowseActivity.class);
        intent.putExtra("position", position);
        intent.putExtra("name", name);
        intent.putExtra("list", list);
        super.startActivity(intent);
    }

    public void startService(Class<? extends BaseService> destClass)
    {
        Intent intent = new Intent(this, destClass);
        super.startService(intent);
    }

    public void startService(Serializable data,
                             Class<? extends BaseService> destClass)
    {
        Intent intent = new Intent(this, destClass);
        intent.putExtra("data", data);
        super.startService(intent);
    }

    @SuppressWarnings("unused")
    public void startActivity(Class<? extends Activity> destClass)
    {
        Intent intent = new Intent(this, destClass);
        super.startActivity(intent);
        if (false)
        {
//            overridePendingTransition(R.anim.push_right_in,
//                    R.anim.push_left_out);
        }
    }

    public void startActivity(Serializable data,
                              Class<? extends Activity> destClass)
    {
        Intent intent = new Intent(this, destClass);
        intent.putExtra("data", data);
        super.startActivity(intent);
    }

    public void stopService(Class<? extends BaseService> destClass)
    {
        Intent intent = new Intent(this, destClass);
        stopService(intent);
    }

    public void saveScreenShotImage(Bitmap bitmap)
    {
        if (!SDCardUtils.isSDCardEnable())
        {
            ToastUtils.showShortToast("您的设备没有存储卡，不能保存图片");
        }
        else
        {
            String path = SDCardUtils.getSDCardPath() + "DCIM/qlsl/ScreenShot/";
            File file = new File(path);
            if (!file.exists())
            {
                file.mkdirs();
            }
            path = path + System.currentTimeMillis() + ".jpg";
            SDCardUtils.saveImage(bitmap, path);
            ToastUtils.showShortToast("图片已保存在DCIM/qlsl/ScreenShot中！");
        }

    }

    public File saveBitmapToFile(Bitmap bitmap)
    {
        if (!SDCardUtils.isSDCardEnable())
        {
            return null;
        }
        else
        {
            String path = SDCardUtils.getSDCardPath() + "DCIM/qlsl/ScreenShot/";
            File file = new File(path);
            if (!file.exists())
            {
                file.mkdirs();
            }
            path = path + System.currentTimeMillis() + ".jpg";
            SDCardUtils.saveImage(bitmap, path);
            File newFile = new File(path);
            return newFile;
        }

    }

    public void cropPhoto(String path)
    {
        Uri uri = Uri.parse("file://" + path);
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", 320);
        intent.putExtra("outputY", 320);
        intent.putExtra("scale", true);
        intent.putExtra("scaleUpIfNeeded", true);
        intent.putExtra("return-data", true);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        intent.putExtra("noFaceDetection", true);
        startActivityForResult(intent, IntentCodeConstant.CROP_PHOTO);
    }

    public void cropPhoto(String path, String destPath)
    {
        Uri uri = Uri.parse("file://" + path);
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", 960);
        intent.putExtra("outputY", 960);
        intent.putExtra("scale", true);
        intent.putExtra("scaleUpIfNeeded", true);
        intent.putExtra("return-data", false);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.parse("file://" + destPath));
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        intent.putExtra("noFaceDetection", true);
        startActivityForResult(intent, IntentCodeConstant.CROP_PHOTO);
    }

    public void cropPhotoExpand(String path, String destPath)
    {
        Uri uri = Uri.parse("file://" + path);
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("scale", true);
        intent.putExtra("scaleUpIfNeeded", true);
        intent.putExtra("return-data", false);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.parse("file://" + destPath));
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        intent.putExtra("noFaceDetection", true);
        startActivityForResult(intent, IntentCodeConstant.CROP_PHOTO);
    }

    public void cropPhoto(String path, int aspectX, int aspectY, int outputX,
                          int outputY)
    {
        Uri uri = Uri.parse("file://" + path);
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", aspectX);
        intent.putExtra("aspectY", aspectY);
        intent.putExtra("outputX", outputX);
        intent.putExtra("outputY", outputY);
        intent.putExtra("scale", true);
        intent.putExtra("scaleUpIfNeeded", true);
        intent.putExtra("return-data", true);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        intent.putExtra("noFaceDetection", true);
        startActivityForResult(intent, IntentCodeConstant.CROP_PHOTO);
    }

    public void setRefresh(boolean refresh)
    {
        Intent intent = new Intent();
        intent.putExtra("refresh", refresh);
        setResult(0, intent);
    }

    public boolean hasRefresh(Intent intent)
    {
        if (intent != null && intent.getBooleanExtra("refresh", false))
        {
            return true;
        }
        return false;
    }

    public void onClick(DialogInterface dialog, int which)
    {
    }

    /**
     * 网络请求成功回传统一调用此方法<br/>
     * 作用：聚合分发式<br/>
     * 待接收对象：showData函数中的t容器对象<br/>
     */
    @SuppressWarnings("hiding")
    public void showNetWorkSucceedData(String method, Object values)
    {

    }

    /**
     * 网络请求失败回传统一调用此方法<br/>
     * 作用：聚合分发式<br/>
     * 待接收对象：showNoData函数中的t容器对象<br/>
     */
    @SuppressWarnings("hiding")
    public void showNetWorkFaildData(String method, Object values)
    {
        ToastUtils.showNoDataToast(values.toString());
    }

    /**
     * 数据库操作成功回传统一调用此方法<br/>
     * 作用：聚合分发式<br/>
     * 待接收对象：showData函数中的t容器对象<br/>
     */
    @SuppressWarnings("hiding")
    public void showDbSucceedData(String method, Object values)
    {

    }

    /**
     * 数据库操作失败回传统一调用此方法<br/>
     * 作用：聚合分发式<br/>
     * 待接收对象：showNoData函数中的t容器对象<br/>
     */
    @SuppressWarnings("hiding")
    public void showFaildDbData(String method, Object values)
    {
        ToastUtils.showNoDataToast(values.toString());
    }

    /**
     * 其他操作成功回传统一调用此方法<br/>
     * 作用：聚合分发式<br/>
     * 待接收对象：showData函数中的t容器对象<br/>
     */
    @SuppressWarnings("hiding")
    public void showOtherSucceedData(String method, Object values)
    {
        ToastUtils.showNoDataToast(values.toString());
    }

    /**
     * 其他操作失败回传统一调用此方法<br/>
     * 作用：聚合分发式<br/>
     * 待接收对象：showData函数中的t容器对象<br/>
     */
    @SuppressWarnings("hiding")
    public void showFaildOtherData(String method, Object values)
    {
        ToastUtils.showNoDataToast(values.toString());
    }

    /**
     * 作用：处理网络，数据库及其他回传成功，主要业务<br/>
     *
     * @param t
     * @param <T>
     */
    public <T extends Object> void showData(T... t)
    {

    }

    /**
     * 作用：处理网络，数据库及其他回传失败，主要业务<br/>
     *
     * @param t
     * @param <T>
     */
    public <T extends Object> void showNoData(T... t)
    {

    }
    //-----------------------------------------------------END----------------------------------------------------------
}
