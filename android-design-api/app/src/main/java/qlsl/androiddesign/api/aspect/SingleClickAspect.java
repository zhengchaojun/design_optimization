package qlsl.androiddesign.api.aspect;

import android.view.View;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

import java.util.Calendar;

import qlsl.androiddesign.api.activity.R;

/**
 * 类描述：防止View被连续点击,间隔时间600ms,（注意事项：Fragment）
 * 需要传入的键：****
 * 传入的值类型：****
 * 传入的值含义：****
 * 是否必传：****
 * 作者：郑朝军 on 2017/6/14 11:29
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/14 11:29
 * 修改备注：
 */
@Aspect
public class SingleClickAspect
{
    static int TIME_TAG = R.id.tv_tag;
    public static final int MIN_CLICK_DELAY_TIME = 600;

    @Pointcut("execution(@com.cx.annotation.aspect.SingleClick * *(..))")//方法切入点
    public void methodAnnotated()
    {
    }

    @Around("methodAnnotated()")//在连接点进行方法替换
    public void aroundJoinPoint(ProceedingJoinPoint joinPoint) throws Throwable
    {
        View view = null;
        for (Object arg : joinPoint.getArgs())
            if (arg instanceof View) view = (View) arg;
        if (view != null)
        {
            Object tag = view.getTag(TIME_TAG);
            long lastClickTime = ((tag != null) ? (long) tag : 0);
//            ToastUtils.showLongToast("lastClickTime===" + lastClickTime);
            long currentTime = Calendar.getInstance().getTimeInMillis();
            if (currentTime - lastClickTime > MIN_CLICK_DELAY_TIME)
            {//过滤掉600毫秒内的连续点击
                view.setTag(TIME_TAG, currentTime);
//                ToastUtils.showLongToast("lastClickTime===" + currentTime);
                joinPoint.proceed();//执行原方法
            }
        }
    }
}
