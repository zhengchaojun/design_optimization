package qlsl.androiddesign.api.http.service.commonservice;

import android.os.Handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cx.annotation.aspect.CheckNetWork;

import qlsl.androiddesign.api.activity.commonactivity.CopyActivity;
import qlsl.androiddesign.api.constant.WhatConstant;
import qlsl.androiddesign.api.http.xutils.HttpProtocol;
import qlsl.androiddesign.api.util.commonutil.Log;
import qlsl.androiddesign.api.util.commonutil.ThreadManager;

/**
 * 类描述：****页
 * 需要传入的键：****
 * 传入的值类型：****
 * 传入的值含义：****
 * 是否必传：****
 * 作者：郑朝军 on 2017/6/14 11:29
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/14 11:29
 * 修改备注：
 */
public class ACopy2Service extends CommonUtilsService<CopyActivity>
{
    private String className = getClassName(ACopy2Service.class);

    public void test()
    {
        Log.e("zcj", "1111111111111111111test1111111");
    }

    /**
     * *******
     */
    @CheckNetWork
    public void copy()
    {
        HandlerThread("test03", className, new ServiceBaseHandlerThreadDelegate()
        {
            @Override
            public JSONObject serviceBaseHandlerThreadCallBack(Handler handler)
            {
                HttpProtocol protocol = new HttpProtocol();

                JSONObject jo = protocol.setMethod("getsuppliergoodstypelist").post();
                if (isDataInvalid(jo, handler))
                {
                    return null;
                }

                Object object = JSON.toJavaObject(jo, Object.class);


                handler.sendMessage(handler.obtainMessage(WhatConstant.WHAT_NET_DATA_SUCCESS,
                        object));
                return null;
            }
        });
    }

    /**
     * *******
     */
    @CheckNetWork
    public void copy2()
    {
        getV().showProgressBar();
        final String method = "copy";
        final Handler handler = getHandler("copy");
        ThreadManager.getThreadPool(className, "copy").execute(
                new Runnable()
                {
                    @Override
                    public void run()
                    {
                        HttpProtocol protocol = new HttpProtocol();
                        try
                        {
                            JSONObject jo = protocol.setMethod("getsuppliergoodstypelist").post();
                            if (isDataInvalid(jo, handler))
                            {
                                return;
                            }

                            Object object = JSON.toJavaObject(jo, Object.class);


                            outputMessageInfo(protocol, jo, className, method);
                            handler.sendMessage(handler.obtainMessage(WhatConstant.WHAT_NET_DATA_SUCCESS,
                                    object));
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                            handler.sendMessage(handler.obtainMessage(WhatConstant.WHAT_NET_DATA_FAIL, e));
                        }
                    }
                });
    }


}
