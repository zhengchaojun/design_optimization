package qlsl.androiddesign.api.activity.commonactivity;

import org.apache.log4j.Level;

import de.mindpipe.android.logging.log4j.LogConfigurator;
import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.activity.baseactivity.CommonActivityBase;
import qlsl.androiddesign.api.constant.MessageConstant;
import qlsl.androiddesign.api.entity.commonentity.UpdateInfo;
import qlsl.androiddesign.api.http.service.commonservice.InstanceService;
import qlsl.androiddesign.api.manager.AppMethod;
import qlsl.androiddesign.api.manager.ExitManager;
import qlsl.androiddesign.api.manager.VersionUpdateManager;
import qlsl.androiddesign.api.method.UserMethod;
import qlsl.androiddesign.api.service.subservice.LogFloatService;
import qlsl.androiddesign.api.util.commonutil.Log;
import qlsl.androiddesign.api.util.commonutil.StatusBarUtil;
import qlsl.androiddesign.api.view.commonview.TextView;

/**
 * 类描述：初始化页面
 * 创建人：郑朝军
 * 创建时间：2017/5/12 8:49
 * 修改人：郑朝军
 * 修改时间：2017/5/12 8:49
 * 修改备注：
 * 公司:武汉智博创享科技有限公司
 */
public class InstanceActivity extends CommonActivityBase<InstanceService> implements
        VersionUpdateManager.OnCancelListener
{
    private TextView tvVersion;

    @Override
    public int getLayoutId()
    {
        return R.layout.activity_instance;
    }

    @Override
    public boolean useEventBus()
    {
        return false;
    }

    @Override
    public InstanceService newP()
    {
        return new InstanceService();
    }


    @Override
    protected void initView()
    {
        tvVersion = findView(R.id.tv_version);
        StatusBarUtil.setColorTransparent(activity, R.color.transparent);
        hideTitleBar();
        getmSwipeBackLayout().setEnableScroll(false);
    }


    @Override
    protected void initData()
    {
        initLog4j();
        showVersionName();
        forceLogin();
    }

    @Override
    protected void initListener()
    {

    }

    /**
     * @param method
     * @param values
     */
    @Override
    public void showNetWorkSucceedData(String method, Object values)
    {
        if (method.equals("getUserInfo"))
        {

        }
        else if (method.equals("getSysCache"))
        {
            getP().getUpdateInfo();
        }
        else if (method.equals("getUpdateInfo"))
        {
            UpdateInfo updateInfo = (UpdateInfo) values;
            VersionUpdateManager manager = new VersionUpdateManager(this);
            manager.setOnCancelListener(this);
            if (!manager.checkUpdate(updateInfo))
            {
                startToHomePage();
            }
        }
    }


    @Override
    public void showNetWorkFaildData(String method, Object values)
    {
        if (method.equals("getUserInfo") || method.equals("getSysCache"))
        {
            UserMethod.setToken(null);
            forceLogin();
        }

        if ((MessageConstant.MSG_MEMBER_INVALID.equals(values))
                && (MessageConstant.MSG_LOGIN_INVALID.equals(values)))
        {
            stopService(LogFloatService.class);
            ExitManager.ExitDelayed();
        }
    }

    private void initLog4j()
    {
        if (!Log.isDebug)
            return;
        String path = Log.getLogPath(this);
        LogConfigurator logConfigurator = new LogConfigurator();
        logConfigurator.setFileName(path);
        logConfigurator.setRootLevel(Level.DEBUG);
        logConfigurator.setLevel("org.apache", Level.ERROR);
        logConfigurator.setFilePattern("%d %-5p [%c{2}]-[%L] %m%n");
        logConfigurator.setMaxFileSize(1024 * 1024 * 5);
        logConfigurator.setImmediateFlush(true);
        logConfigurator.configure();

        Log.notifyDataSetChanged(this);
        startFloatService();
    }

    private void showVersionName()
    {
        String versionName = AppMethod.getVersionName();
        tvVersion.setText("v" + versionName);
    }

    /**
     * 调试模式下开启悬浮窗服务(有bug，暂时不需要)
     */
    private void startFloatService()
    {
//        startService(LogFloatService.class);
    }

    /**
     * 强制登陆
     */
    public void forceLogin()
    {
        String token = UserMethod.getTokenFromProperty();
        if (token != null)
        {
            UserMethod.setToken(token);
            getP().getUserInfo();
        }
        else
        {
            startActivity(MemberLoginActivity.class);
            finish();
        }
    }

    private void startToHomePage()
    {
        startActivity(MainActivity.class);
    }


    @Override
    public void cancel()
    {
        startToHomePage();
    }
}
