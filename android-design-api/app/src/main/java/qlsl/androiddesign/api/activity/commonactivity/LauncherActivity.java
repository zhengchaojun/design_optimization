package qlsl.androiddesign.api.activity.commonactivity;

import android.Manifest;
import android.view.View;

import com.cx.annotation.aspect.Permission;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.activity.baseactivity.CommonActivityBase;
import qlsl.androiddesign.api.listener.OnPageChangeListener;
import qlsl.androiddesign.api.util.commonutil.SPUtils;
import qlsl.androiddesign.api.util.commonutil.StatusBarUtil;
import qlsl.androiddesign.api.view.indicatorview.ImageIndicatorView;
import qlsl.androiddesign.api.view.indicatorview.IndicatorView;

/**
 * 作者：郑朝军 on 2017/4/2 23:46
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */

public class LauncherActivity extends CommonActivityBase implements
        ImageIndicatorView.OnItemChangeListener
{
    /**
     * 引导页
     */
    private IndicatorView indicatorView;

    /**
     * 启动
     */
    private View iv_launcher;

    @Override
    public int getLayoutId()
    {
        return R.layout.activity_launcher;
    }

    @Override
    public boolean useEventBus()
    {
        return false;
    }

    @Override
    public Object newP()
    {
        return null;
    }


    @Override
    protected void initView()
    {
        StatusBarUtil.setColorTransparent(activity, R.color.transparent);
        indicatorView = (IndicatorView) findViewById(R.id.net_indicate_view);
        indicatorView.setIndicateStyle(IndicatorView.INDICATE_PAGER_STYLE);
        iv_launcher = findViewById(R.id.iv_launcher);
    }

    @Override
    @Permission(value = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
    })
    protected void initData()
    {
        Boolean isFirstStart = (Boolean) SPUtils.get(activity, SPUtils.IS_FIRST_START, true);
        if (isFirstStart)
        {
            Integer[] resArray = new Integer[]{R.drawable.suggest_01, R.drawable.suggest_02, R
                    .drawable.suggest_03};
            indicatorView.setupLayoutByDrawable(resArray);
            indicatorView.show();
        }
        else
        {
            startActivity(InstanceActivity.class);
            activity.finish();
        }
    }

    @Override
    protected void initListener()
    {
        indicatorView.setOnItemChangeListener(this);
    }


    private OnPageChangeListener onPageChangeListener = new OnPageChangeListener()
    {
        @Override
        public void onPageSelected(int position)
        {
            if (position == 2)
            {
                findViewById(R.id.layout_touch).setVisibility(View.VISIBLE);
            }
            else
            {
                findViewById(R.id.layout_touch).setVisibility(View.GONE);
            }
        }
    };


    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.iv_launcher:
                doClickLauncherView();
                break;
        }
    }

    @Permission(value = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
    })
    private void doClickLauncherView()
    {
        SPUtils.put(activity, SPUtils.IS_FIRST_START, false);
        startActivity(InstanceActivity.class);
        activity.finish();
    }

    public void onPosition(int position, int totalCount)
    {
        findViewById(R.id.tv_pager).setVisibility(View.GONE);
        if (position == totalCount - 1)
        {
            iv_launcher.setVisibility(View.VISIBLE);
            findViewById(R.id.layout_touch).setVisibility(View.VISIBLE);
        }
        else
        {
            findViewById(R.id.layout_touch).setVisibility(View.GONE);
        }
    }
}
