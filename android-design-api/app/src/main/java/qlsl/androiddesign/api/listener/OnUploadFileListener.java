package qlsl.androiddesign.api.listener;

import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import xutils.exception.HttpException;
import xutils.http.ResponseInfo;
import xutils.http.callback.RequestCallBack;

import qlsl.androiddesign.api.activity.baseactivity.FunctionActivity;
import qlsl.androiddesign.api.application.SoftwareApplication;
import qlsl.androiddesign.api.constant.MessageConstant;
import qlsl.androiddesign.api.manager.ActivityManager;
import qlsl.androiddesign.api.util.commonutil.Log;


/**
 * 上传图片,音频等文件的公用回调<br/>
 */
public abstract class OnUploadFileListener extends RequestCallBack<String>
{
    public void onSuccess(ResponseInfo<String> responseInfo)
    {
        hideProgressBar();
        String responseStr = responseInfo.result;
        Log.e("zcj", "responseStr :" + responseStr);
//        JSONObject jo = JSONObject.parseObject(responseStr);
//        if (jo == null)
//        {
//            ActivityManager.getInstance().showToast(MessageConstant.MSG_SERVER_FAILED);
//            return;
//        }
//        if ("success".equals(jo.getString("msg")))
//        {
//            ActivityManager.getInstance().showToast("上传成功");
//            onSuccess(jo);
//        }
//        else if ("fail".equals(jo.getString("msg")))
//        {
//            String errorCode = jo.getString("errorCode");
//            if ("40001".equals(errorCode))
//            {
//                ActivityManager.getInstance().showToast("上传失败");
//            }
//            else
//            {
//                ActivityManager.getInstance().showToast("服务器错误");
//            }
//        }
//        else
//        {
//            ActivityManager.getInstance().showToast("参数错误");
//        }
    }

    public void onLoading(long total, long current, boolean isUploading)
    {
        if (isUploading)
        {
            long progress = current * 100 / total;
            String text = "正在上传：" + progress + "%";
            FunctionActivity<?> functionView = ((FunctionActivity) ActivityManager.getInstance()
                    .currentActivity());
            functionView.setProgressBarText(text);
            functionView.showProgressBar();
        }
    }

    public void onFailure(HttpException error, String msg)
    {
        hideProgressBar();
        Toast.makeText(SoftwareApplication.getInstance(), "上传失败：" + msg, Toast.LENGTH_SHORT).show();
    }

    private void hideProgressBar()
    {
        ((FunctionActivity) ActivityManager.getInstance().currentActivity()).hideProgressBar();
    }

    public abstract void onSuccess(JSONObject jo);

}
