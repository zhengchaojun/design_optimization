package qlsl.androiddesign.api.util.commonutil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * 类描述：排序相关工具类
 * 作者：郑朝军 on 2018/3/26
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2018/3/26
 * 修改备注：
 */
public class SortUtil
{
    /**
     * 针对Map的key进行升序排列
     *
     * @param map 举例：map.put("2", "aaaaa");map.put("3", "bbbbb");map.put("1", "ddddd");map.put("4", "eeeee");
     * @return 举例：map.put("1", "ddddd");map.put("2", "aaaaa");map.put("3", "bbbbb");map.put("4", "eeeee");
     */
    public static List sortMapKey(Map<Integer, Double> map)
    {
        if (map == null)
        {
            return null;
        }
        List<Map.Entry<Integer, Double>> list = new ArrayList<Map.Entry<Integer, Double>>(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<Integer, Double>>()
        {//升序排序
            public int compare(Map.Entry<Integer, Double> o1, Map.Entry<Integer, Double> o2)
            {
                return o1.getKey().compareTo(o2.getKey());
            }
        });
        return list;
    }
}
