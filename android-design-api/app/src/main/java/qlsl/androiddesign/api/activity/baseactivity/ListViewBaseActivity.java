package qlsl.androiddesign.api.activity.baseactivity;

import android.view.View;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.activity.baseactivity.CommonActivityBase;
import qlsl.androiddesign.api.activity.baseactivity.CommonContract;
import qlsl.androiddesign.api.entity.commonentity.Pager;
import qlsl.androiddesign.api.util.commonutil.ReflectUtil;

/**
 * 类描述：listView基类页
 * 需要传入的键：D P
 * 传入的值类型：Object CommonContract.IService
 * 传入的值含义：数据源  服务
 * 是否必传：是 否
 * 作者：郑朝军 on 2017/6/14 11:29
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/14 11:29
 * 修改备注：
 */
public abstract class ListViewBaseActivity<D, P extends CommonContract.IService> extends
        CommonActivityBase<P>
{
    protected ListView listView;

    protected Button btn_common;

    protected List<D> list = new ArrayList<D>();

    protected Pager pager = new Pager();

    @Override
    public int getLayoutId()
    {
        return R.layout.common_list_view_no_margin_divider;
    }

    @Override
    public boolean useEventBus()
    {
        return false;
    }

    @Override
    public P newP()
    {
        return ReflectUtil.getT(this,1);
    }

    @Override
    protected void initView()
    {
        listView = (ListView) findViewById(R.id.listView);
        if (getLayoutId() == R.layout.common_list_view_btn)
        {
            btn_common = (Button) findViewById(R.id.btn_common);
        }
    }

    @Override
    protected void initData()
    {
        notifyDataSetChanged();
    }

    @Override
    protected void initListener()
    {

    }

    public void onClick(View view)
    {

    }

    public void showNetWorkSucceedData(String method, Object values)
    {

    }

    public <T> void showData(T... t)
    {
        if (t[0] instanceof HashMap)
        {
            HashMap<String, Object> map = (HashMap<String, Object>) t[0];
            List<D> list_result = (List<D>) map.get("list");
            pager = (Pager) map.get("pager");
            if (pager.getPageNo() == 1)
            {
                resetList(list_result);
            }
            else
            {
                list.addAll(list_result);
            }
            notifyDataSetChanged();
        }
    }

    public <T> void showNoData(T... t)
    {

    }

    protected void resetList(List<D> list_result)
    {
        list.clear();
        list.addAll(list_result);
    }

    protected void notifyDataSetChanged()
    {
        BaseAdapter adapter = (BaseAdapter) listView.getAdapter();
        if (adapter == null)
        {
            adapter = getAdapter();
            listView.setAdapter(adapter);
        }
        else
        {
            adapter.notifyDataSetChanged();
        }
    }

    public abstract BaseAdapter getAdapter();

    /**
     * 设置按钮文字
     *
     * @param text
     */
    public void setBtnText(String text)
    {
        btn_common.setText("" + text);
    }
}
