package qlsl.androiddesign.api.listener;

import android.content.Context;
import android.widget.AbsListView;

import com.squareup.picasso.Picasso;

/**
 * 类描述：ListViewScrollView滑动监听器
 * 需要传入的键：****
 * 传入的值类型：****
 * 传入的值含义：****
 * 是否必传：****
 * 作者：郑朝军 on 2017/6/14 11:29
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/14 11:29
 * 修改备注：
 */
public class ListViewScrollViewListener implements AbsListView.OnScrollListener
{
    private final Context context;

    public ListViewScrollViewListener(Context context)
    {
        this.context = context;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState)
    {
        final Picasso picasso = Picasso.with(context);
        if (scrollState == SCROLL_STATE_IDLE || scrollState == SCROLL_STATE_TOUCH_SCROLL)
        {
            picasso.resumeTag(context);
        }
        else
        {
            picasso.pauseTag(context);
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                         int totalItemCount)
    {
    }
}
