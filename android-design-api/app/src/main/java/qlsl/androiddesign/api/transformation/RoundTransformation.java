package qlsl.androiddesign.api.transformation;

import android.graphics.Bitmap;

import com.squareup.picasso.Transformation;

import qlsl.androiddesign.api.util.commonutil.BitmapUtils;

public class RoundTransformation implements Transformation
{

    public Bitmap transform(Bitmap source)
    {
        Bitmap result = BitmapUtils.getRoundedCornerBitmap(source, 10);
        if (result != source)
        {
            source.recycle();
        }
        return result;
    }

    public String key()
    {
        return "round";
    }
}
