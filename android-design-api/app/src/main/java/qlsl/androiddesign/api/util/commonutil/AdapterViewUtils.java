package qlsl.androiddesign.api.util.commonutil;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.lang.reflect.Field;

public class AdapterViewUtils
{

    public static void setListViewHeight(ListView listView)
    {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
        {
            return;
        }

        int totalHeight = 0;
        for (int i = 0, len = listAdapter.getCount(); i < len; i++)
        {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    public static void setGridViewHeight(GridView gridView)
    {
        ListAdapter listAdapter = gridView.getAdapter();
        if (listAdapter == null)
        {
            return;
        }
        int rows;
        int columns = 0;
        int horizontalBorderHeight = 0;
        Class<?> clazz = gridView.getClass();
        try
        {
            Field column = clazz.getDeclaredField("mRequestedNumColumns");
            column.setAccessible(true);
            columns = (Integer) column.get(gridView);
            Field horizontalSpacing = clazz
                    .getDeclaredField("mRequestedHorizontalSpacing");
            horizontalSpacing.setAccessible(true);
            horizontalBorderHeight = (Integer) horizontalSpacing.get(gridView);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        if (listAdapter.getCount() % columns > 0)
        {
            rows = listAdapter.getCount() / columns + 1;
        }
        else
        {
            rows = listAdapter.getCount() / columns;
        }
        int totalHeight = 0;
        for (int i = 0; i < rows; i++)
        {
            View listItem = listAdapter.getView(i, null, gridView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = totalHeight + horizontalBorderHeight * (rows - 1);
        gridView.setLayoutParams(params);
    }

    public static void setGridViewMinHeight(GridView gridView, int minHeight)
    {
        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = minHeight;
        gridView.setLayoutParams(params);
    }

    /**
     * 测试未通过
     * @param activity
     * @param recyclerView
     */
    public static void setRecyclerViewHeight(final Activity activity,final RecyclerView recyclerView)
    {
        if (recyclerView == null)
        {
            return;
        }
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    Thread.sleep(1000);
                    int totalHeight = 0;
                    for (int i = 0, len = recyclerView.getAdapter().getItemCount()-1; i < len; i++)
                    {
                        totalHeight += recyclerView.getChildAt(i).getHeight();
                    }

                    final ViewGroup.LayoutParams params = recyclerView.getLayoutParams();
//                    params.height = totalHeight + (recyclerView.getde * (recyclerView
// .getChildCount() - 1));
                    params.height = totalHeight;

                    activity.runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            recyclerView.setLayoutParams(params);
                        }
                    });
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    Log.e("zcj", "存在异常！！！"+e);
                }
            }
        }).start();


    }
}
