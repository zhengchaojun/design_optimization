package qlsl.androiddesign.api.listener;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import qlsl.androiddesign.api.activity.baseactivity.FunctionActivity;


/**
 * WebView加载监听<br/>
 */
public class WebViewLoadListener extends WebViewClient {

    /**
     * 兼容公式显示<br/>
     */
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        if ("0".equals(view.getTag() + "")) {
            FunctionActivity activity = (FunctionActivity) view.getContext();
            activity.resetProgressBarText();
            activity.showProgressBar();
        }
        super.onPageStarted(view, url, favicon);
    }

    /**
     * 兼容公式显示<br/>
     */
    public void onPageFinished(WebView view, String url) {
        FunctionActivity activity = (FunctionActivity) view.getContext();
        activity.hideProgressBar();
        super.onPageFinished(view, url);
    }

    /**
     * 兼容日志记录中的接口数据查看<br/>
     * 兼容网址浏览页<br/>
     */
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (view.getContext().getClass().getSimpleName().equals("WebActivity")) {
            view.loadUrl(url);
            return true;
        }
//        Intent intent = new Intent(view.getContext(), JsonFormatActivity.class);
//        intent.putExtra("url", url);
//        view.getContext().startActivity(intent);
        return true;
    }

    public void onReceivedError(WebView view, int errorCode,
                                String description, String failingUrl) {
        FunctionActivity activity = (FunctionActivity) view.getContext();
        activity.hideProgressBar();
        super.onReceivedError(view, errorCode, description, failingUrl);
    }

}
