package qlsl.androiddesign.api.activity.commonactivity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;

import com.cx.annotation.aspect.SingleClick;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.activity.baseactivity.CommonActivityBase;
import qlsl.androiddesign.api.adapter.commonadapter.ContactAdapter;
import qlsl.androiddesign.api.entity.commonentity.Contacts;
import qlsl.androiddesign.api.http.service.commonservice.MemberService;
import qlsl.androiddesign.api.listener.OnTextChangeListener;
import qlsl.androiddesign.api.util.commonutil.Log;
import qlsl.androiddesign.api.view.stickylist.SideBar;
import qlsl.androiddesign.api.view.stickylist.StickyListHeadersListView;

/**
 * 类描述：联系人页
 * 需要传入的键：****
 * 传入的值类型：****
 * 传入的值含义：****
 * 是否必传：****
 * 作者：郑朝军 on 2017/6/14 11:29
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/14 11:29
 * 修改备注：
 */
public class ContactActivity extends CommonActivityBase<MemberService> implements
        AdapterView.OnItemClickListener, SideBar.OnTouchingLetterChangedListener
{
    private EditText et_search;

    private StickyListHeadersListView stickyView;

    private SideBar sideBar;

    private List<Contacts> list_all = new ArrayList<Contacts>();

    private List<Contacts> list_current = new ArrayList<Contacts>();

    @Override
    public int getLayoutId()
    {
        return R.layout.activity_contact;
    }

    @Override
    public boolean useEventBus()
    {
        return false;
    }

    @Override
    public MemberService newP()
    {
        return new MemberService();
    }

    @Override
    protected void initView()
    {
        setTitle("联系人");
        et_search = findView(R.id.et_search);
        stickyView = findView(R.id.stickyView);
        stickyView.setStickyHeaderTopOffset(0);
        stickyView.setAreHeadersSticky(true);
        stickyView.setDrawingListUnderStickyHeader(true);
        stickyView.setFastScrollEnabled(false);
        stickyView.setFastScrollAlwaysVisible(false);
        stickyView.setDivider(null);
        sideBar = findView(R.id.sidebar);
        TextView tv_dialog = findView(R.id.tv_dialog);
        sideBar.setTextView(tv_dialog);
    }


    @Override
    protected void initData()
    {
        getP().queryContact();
    }

    @Override
    protected void initListener()
    {
        stickyView.setOnItemClickListener(this);
        et_search.addTextChangedListener(onTextChangeListener);
        sideBar.setOnTouchingLetterChangedListener(this);
    }

    @SingleClick
    public void onClick(View view)
    {

    }

    @Override
    public void showDbSucceedData(String method, Object values)
    {
        List<Contacts> list_result = (List<Contacts>) values;
        if (list_result.size() > 0)
        {
            list_all.clear();
            list_all.addAll(list_result);
            list_current.clear();
            list_current.addAll(list_result);
            notifyDataSetChanged();
        }
        else
        {
            getP().showEmptyView("暂无数据");
        }
    }

    public <T> void showData(T... t)
    {

    }

    public <T> void showNoData(T... t)
    {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        Contacts contacts = (Contacts) stickyView.getItemAtPosition(position);

    }

    @Override
    public void onTouchingLetterChanged(String letter)
    {
        for (int index = 0; index < list_current.size(); index++)
        {
            String currentLetter = list_current.get(index).getInitials();
            Log.e("zcj","letter===="+letter);
            Log.e("zcj","currentLetter====="+currentLetter);
            if (currentLetter.compareTo(letter) >= 0)
            {
                stickyView.smoothScrollToPosition(index);
                return;
            }
        }
    }

    private void notifyDataSetChanged()
    {
        ContactAdapter adapter = (ContactAdapter) stickyView.getAdapter();
        if (adapter == null)
        {
            adapter = new ContactAdapter(activity, list_current);
            stickyView.setAdapter(adapter);
        }
        else
        {
            adapter.notifyDataSetChanged();
        }
    }

    private OnTextChangeListener onTextChangeListener = new OnTextChangeListener()
    {
        public void afterTextChanged(android.text.Editable s)
        {
            if (list_all.size() > 0)
            {
                if (s.length() > 0)
                {
                    List<Contacts> list_result = new ArrayList<Contacts>();
                    for (Contacts CarBrand : list_all)
                    {
                        if (CarBrand.getContact().contains(s.toString()))
                        {
                            list_result.add(CarBrand);
                        }
                    }
                    list_current.clear();
                    list_current.addAll(list_result);
                    Collections.sort(list_current);
                    notifyDataSetChanged();
                }
                else
                {
                    list_current.clear();
                    list_current.addAll(list_all);
                    Collections.sort(list_current);
                    notifyDataSetChanged();
                }
            }
        }
    };

}
