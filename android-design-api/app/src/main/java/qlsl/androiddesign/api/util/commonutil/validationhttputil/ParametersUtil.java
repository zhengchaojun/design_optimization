package qlsl.androiddesign.api.util.commonutil.validationhttputil;

import java.util.HashMap;
import java.util.TreeMap;

import qlsl.androiddesign.api.constant.HttpKeyConstant;

/**
 * 网络请求参数工具类
 * 作者：郑朝军 on 2016/12/2 11:49
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */

public class ParametersUtil {

    private TreeMap<String, Object> params = new TreeMap<String, Object>();

    private boolean removeUserParam;

    private static class UtilHodel {
        public static final ParametersUtil UTIL = new ParametersUtil();
    }

    public static ParametersUtil getInstance() {
        return ParametersUtil.UtilHodel.UTIL;
    }


    public static HashMap getHeadParams(boolean isUseID) {
        HashMap params = new HashMap();
        String timestamp = TimeUtil.getNowYMDHMSTime();
        String nonce = RandomUtil.create_nonce_str();
        params.put("signature", ConnUtils.getSignature(ConnUtils.token, timestamp, nonce));
        params.put("timestamp", timestamp);
        params.put("nonce", nonce);

        if (isUseID)
            params.put("userid", "16");
//            params.put("userid", UserMethod.getUser().getId());
        else
            params.put("fuserid", "16");
//            params.put("fuserid", UserMethod.getUser().getId());

        return params;
    }

    public ParametersUtil addParam(String key, Object value) {
        if (value instanceof Boolean) {
            value = (Boolean) value ? 1 : 0;
        }
        if (value != null) {
            if (key.equals(HttpKeyConstant.PARAM_USER_ID) && removeUserParam) {
                return this;
            }
            params.put(key, value);
        }
        return this;
    }

    public ParametersUtil removeUserParam() {
        removeUserParam = true;
        return this;
    }

    public TreeMap<String, Object> getParams() {
        return params;
    }

    public void setParams(TreeMap<String, Object> params) {
        this.params = params;
    }

    public boolean isRemoveUserParam() {
        return removeUserParam;
    }

    public void setRemoveUserParam(boolean removeUserParam) {
        this.removeUserParam = removeUserParam;
    }
}
