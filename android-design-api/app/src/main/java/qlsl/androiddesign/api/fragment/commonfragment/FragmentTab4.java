package qlsl.androiddesign.api.fragment.commonfragment;

import android.support.annotation.IdRes;
import android.view.View;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.fragment.basefragment.ZLazyFragment;
import qlsl.androiddesign.api.util.commonutil.ToastUtils;

public class FragmentTab4 extends ZLazyFragment {

    @Override
    public int getLayoutId() {
        return R.layout.tab4;
    }

    @Override
    public Object newP() {
        return null;
    }

    @Override
    protected void initView() {
        ToastUtils.showShort("FragmentTab4");
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {

    }



}
