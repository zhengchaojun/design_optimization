package qlsl.androiddesign.api.view.commonview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnLongClickListener;

import qlsl.androiddesign.api.util.commonutil.Log;


/**
 * 艺术字体Button<br/>
 */
public class Button extends android.widget.Button implements OnLongClickListener {

    public Button(Context context) {
        super(context);
        init();
    }

    public Button(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Button(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();

    }

    private void init() {
        setClickable(true);
        if (Log.isDebug) {
            setOnLongClickListener(this);
        }
    }

    public void setClickable(boolean clickable) {
        super.setClickable(true);
    }

    public boolean onLongClick(View v) {

        return false;
    }

}
