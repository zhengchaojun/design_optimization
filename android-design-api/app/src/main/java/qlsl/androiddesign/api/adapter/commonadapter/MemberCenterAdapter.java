package qlsl.androiddesign.api.adapter.commonadapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.adapter.baseadapter.listviewadapter.ListViewBaseAdapter;
import qlsl.androiddesign.api.util.commonutil.DensityUtils;

/**
 * 类描述：
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传：
 * 作者：郑朝军 on 2017/6/19 19:12
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/19 19:12
 * 修改备注：
 */
public class MemberCenterAdapter extends ListViewBaseAdapter<HashMap<String, Object>>
{
    public MemberCenterAdapter(Activity activity, List<HashMap<String, Object>> list)
    {
        super(activity, list);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        convertView = getItemView(convertView, R.layout.listitem_member_center, parent);

        ViewGroup itemView = getView(convertView, R.id.list_item);
        ImageView iv_icon = getView(convertView, R.id.iv_icon);
        ImageView iv_arrow = getView(convertView, R.id.iv_arrow);
        TextView tv_name = getView(convertView, R.id.tv_text);

        HashMap<String, Object> map = getItem(position);
        Integer icon = (Integer) map.get("icon");
        String text = (String) map.get("text");
        int paddingRight = DensityUtils.dp2px(parent.getContext(), 10);
        int paddingOther = DensityUtils.dp2px(parent.getContext(), 5);

        tv_name.setText(text);

        if (icon != null) {
            iv_icon.setImageResource(icon);
            iv_icon.setVisibility(View.VISIBLE);
            iv_arrow.setVisibility(View.VISIBLE);
            itemView.setActivated(true);
        } else {
            iv_icon.setImageResource(0);
            iv_icon.setVisibility(View.GONE);
            iv_arrow.setVisibility(View.GONE);
            itemView.setActivated(false);
        }

        return convertView;
    }
}
