package qlsl.androiddesign.api.config;

import qlsl.androiddesign.api.activity.BuildConfig;

/**
 * 功能：系统api级别的配置一般在这设置
 * 作者：郑朝军 on 2017/3/20 14:49
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
public class ZConfig
{
    //-------------分界线----log----------------------------------------
    public static final boolean IS_DEBUG = BuildConfig.DEBUG;
    public static final String LOG_TAG = "zcj";
    public static final String TAG = "输出";
    public static final boolean IS_OUTPHONE = false;

    //-------------分界线---cache----------------------------------------
    public static final String CACHE_SP_NAME = "config";
    public static final String CACHE_DISK_DIR = "cache";

    //-------------分界线---greendao----------------------------------------
    public static final String DB_NAME = "zcj";

    //-------------分界线---JPush----------------------------------------
    public static final boolean IS_OPEN_JPUSH = false;

    //-------------分界线---ShareSDK----------------------------------------
    public static final boolean IS_OPEN_SHARESDK = false;

    //-------------分界线---GaoDeSdk----------------------------------------
    public static final boolean IS_OPEN_GAODE_SDK = false;

    //-------------分界线---是否使用Okhttp----------------------------------------
    public static final boolean IS_USE_OKHTTP = false;

    //-------------分界线---设置DrawerLayout是否打开----------------------------------------
    public static boolean CLOSE_DRAWERLAYOUT_LOCK_MODE = false;
}
