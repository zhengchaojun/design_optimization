package qlsl.androiddesign.api.adapter.commonadapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.adapter.baseadapter.listviewadapter.ListViewBaseAdapter;

/**
 * 类描述：设置页
 * 需要传入的键：****
 * 传入的值类型：****
 * 传入的值含义：****
 * 是否必传：****
 * 作者：郑朝军 on 2017/6/14 11:29
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/14 11:29
 * 修改备注：
 */
public class SettingAdapter extends ListViewBaseAdapter<HashMap<String, Object>>
{
    public SettingAdapter(Activity activity, List<HashMap<String, Object>> list)
    {
        super(activity, list);
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        convertView = getItemView(convertView, R.layout.listitem_member_info_new, parent);

        TextView tv_text = getView(convertView, R.id.tv_text);
        TextView tv_value = getView(convertView, R.id.tv_value);
        ImageView iv_value = getView(convertView, R.id.iv_value);

        HashMap<String, Object> map = getItem(position);
        String text = (String) map.get("text");
        String value = (String) map.get("value");

        tv_text.setText(text);

        tv_value.setVisibility(View.VISIBLE);
        iv_value.setVisibility(View.GONE);
        tv_value.setText(value);

        return convertView;
    }
}
