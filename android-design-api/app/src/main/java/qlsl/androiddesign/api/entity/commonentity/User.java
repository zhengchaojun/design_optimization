package qlsl.androiddesign.api.entity.commonentity;

import com.alibaba.fastjson.annotation.JSONField;

import qlsl.androiddesign.api.entity.baseentity.BaseModel;


/**
 * 系统用户配置表
 */
public class User extends BaseModel
{
    private String address;
    private Integer creator;
    private String no;
    private String nickname;
    private String id;
    private Integer glid;
    private String zipcode;
    private String name;
    private String email;
    private Integer gender;
    private String telno;
    private String cell;
    private String faxno;
    @JSONField(alternateNames = {"photo"})
    private String photo;



    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public Integer getCreator()
    {
        return creator;
    }

    public void setCreator(Integer creator)
    {
        this.creator = creator;
    }

    public String getNo()
    {
        return no;
    }

    public void setNo(String no)
    {
        this.no = no;
    }

    public String getNickname()
    {
        return nickname;
    }

    public void setNickname(String nickname)
    {
        this.nickname = nickname;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public Integer getGlid()
    {
        return glid;
    }

    public void setGlid(Integer glid)
    {
        this.glid = glid;
    }

    public String getZipcode()
    {
        return zipcode;
    }

    public void setZipcode(String zipcode)
    {
        this.zipcode = zipcode;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public Integer getGender()
    {
        return gender;
    }

    public void setGender(Integer gender)
    {
        this.gender = gender;
    }

    public String getTelno()
    {
        return telno;
    }

    public void setTelno(String telno)
    {
        this.telno = telno;
    }

    public String getCell()
    {
        return cell;
    }

    public void setCell(String cell)
    {
        this.cell = cell;
    }

    public String getFaxno()
    {
        return faxno;
    }

    public void setFaxno(String faxno)
    {
        this.faxno = faxno;
    }

    public String getPhoto()
    {
        return photo;
    }

    public void setPhoto(String photo)
    {
        this.photo = photo;
    }
}
