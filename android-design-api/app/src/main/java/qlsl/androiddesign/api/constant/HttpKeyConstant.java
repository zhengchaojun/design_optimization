package qlsl.androiddesign.api.constant;

public interface HttpKeyConstant {

	/**
	 * 网络参数key
	 */
	String API_METHOD = "method";
	String PARAM_TICK = "timestamp";
	String PARAM_NONCE = "nonce";
	String PARAM_PARTNER = "partner";
	String PARAM_SIGN = "signature";
	String PARAM_TOKEN = "token";
	String PARAM_USER_ID = "userid";

	/**
	 * 网络参数value
	 */
	// String VALUE_SIGN_SUFFIX = "";
	String VALUE_SIGN_SUFFIX = "";
	String VALUE_PARTNER = "";

}
