package qlsl.androiddesign.api.util.commonutil.validationhttputil;

import java.util.Random;

/**
 * Created by Administrator on 2016/9/18 0018.
 */
public class RandomUtil {

    public static String create_nonce_str() {
        String str = "";
        for (int i = 0; i < 32; i++) {
            Random r = new Random();
            int num = r.nextInt(61);
            if (num >= 0 && num <= 9) {
                str += num;
            } else if (num > 9 && num <= 35) {
                num += 55;
                str += (char) num;
            } else if (num > 35 && num <= 61) {
                num += 61;
                str += (char) num;
            }
        }
        return str;
    }

}
