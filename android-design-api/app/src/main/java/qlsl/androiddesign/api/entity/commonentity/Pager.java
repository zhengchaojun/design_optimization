package qlsl.androiddesign.api.entity.commonentity;

import java.io.Serializable;

public class Pager implements Serializable {

	private static final long serialVersionUID = 8092671304634984363L;

	private int totalCount;// 总数量

	private int pageSize;// 每一页数量

	private int pageNo;// 第几页

	private int totalPage;// 总页数

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public String toString() {
		return getClass().getSimpleName() + "<br/>[totalCount=" + totalCount
				+ ",<br/>totalPage=" + totalPage + ",<br/>pageNo=" + pageNo
				+ ",<br/>pageSize" + pageSize + "]";
	}

}
