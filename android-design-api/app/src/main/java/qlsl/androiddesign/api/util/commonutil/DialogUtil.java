package qlsl.androiddesign.api.util.commonutil;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.activity.baseactivity.FunctionActivity;
import qlsl.androiddesign.api.adapter.baseadapter.listviewadapter.ListViewBaseAdapter;

public class DialogUtil
{

    /**
     * 显示下拉列表EditTextDialog<br/>
     * activity回调<br/>
     */
    public static void showEditTextDialog(Activity activity, String title, String msg, int
            resource, String tag,
                                          String[] texts, ListViewBaseAdapter<?> adapter, int
                                                  childIndex)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);
        builder.setMessage(msg);
        ViewGroup rootView = (ViewGroup) LayoutInflater.from(activity).inflate(resource, null);
        for (int index = 0; index < rootView.getChildCount(); index++)
        {
            EditText et = (EditText) rootView.getChildAt(index);
            if (texts != null)
            {
                et.setText(texts[index]);
            }
            if (childIndex == index)
            {
                if (adapter != null)
                {
                    AutoCompleteTextView actv = (AutoCompleteTextView) et;
                    adapter.transport(et);
                    actv.setAdapter(adapter);
                }
            }
        }
        rootView.setTag(tag);
        builder.setView(rootView);
        builder.setCancelable(true);
        builder.setPositiveButton("确定", (OnClickListener) activity);
        builder.setNegativeButton("取消", (OnClickListener) activity);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * 显示下拉列表EditTextDialog<br/>
     * functionView回调<br/>
     */
    public static void showEditTextDialog(FunctionActivity activity, String title, String msg,
                                          int resource,
                                          String tag, String[] texts, ListViewBaseAdapter<?>
                                                  adapter, int
                                                  childIndex)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);
        builder.setMessage(msg);
        ViewGroup rootView = (ViewGroup) LayoutInflater.from(activity).inflate(resource, null);
        for (int index = 0; index < rootView.getChildCount(); index++)
        {
            EditText et = (EditText) rootView.getChildAt(index);
            if (texts != null)
            {
                et.setText(texts[index]);
            }
            if (childIndex == index)
            {
                if (adapter != null)
                {
                    AutoCompleteTextView actv = (AutoCompleteTextView) et;
                    adapter.transport(et);
                    actv.setAdapter(adapter);
                }
            }
        }
        rootView.setTag(tag);
        builder.setView(rootView);
        builder.setCancelable(true);
        builder.setPositiveButton("确定", activity);
        builder.setNegativeButton("取消", activity);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * 显示EditTextDialog<br/>
     * functionView回调<br/>
     */
    public static void showEditTextDialog(FunctionActivity activity, String title, String msg,
                                          String hint,
                                          String text)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title == null ? activity.getString(R.string.app_name) : title);
        builder.setMessage(msg);
        View rootView = LayoutInflater.from(activity).inflate(R.layout.common_dialog_edit_text,
                null);
        EditText et_content = (EditText) rootView.findViewById(R.id.et_content);
        if (!TextUtils.isEmpty(hint))
        {
            et_content.setHint(hint);
        }
        if (!TextUtils.isEmpty(text))
        {
            et_content.setText(text);
        }
        builder.setView(rootView);
        builder.setCancelable(true);
        builder.setPositiveButton("确定", activity);
        builder.setNegativeButton("取消", activity);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * 显示EditTextDialog<br/>
     * functionView回调<br/>
     */
    public static void showEditTextDialog(FunctionActivity activity, String title, String msg,
                                          String hint,
                                          String text, String tag)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title == null ? activity.getString(R.string.app_name) : title);
        builder.setMessage(msg);
        View rootView = LayoutInflater.from(activity).inflate(R.layout.common_dialog_edit_text,
                null);
        EditText et_content = (EditText) rootView.findViewById(R.id.et_content);
        if (!TextUtils.isEmpty(hint))
        {
            et_content.setHint(hint);
        }
        if (!TextUtils.isEmpty(text))
        {
            et_content.setText(text);
        }
        et_content.setTag(tag);
        builder.setView(rootView);
        builder.setCancelable(true);
        builder.setPositiveButton("确定", activity);
        builder.setNegativeButton("取消", activity);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * 显示确定，取消按钮对话框<br/>
     */
    public static void showDialog(FunctionActivity activity, String title, String msg)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title == null ? activity.getString(R.string.app_name) : title);
        builder.setMessage(msg);
        builder.setCancelable(true);
        builder.setPositiveButton("确定", activity);
        builder.setNegativeButton("取消", activity);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * 显示注册，登陆按钮对话框<br/>
     */
    public static void showLoginDialog(FunctionActivity activity)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(activity.getResources().getString(R.string.app_name));
        builder.setMessage("未登录");
        builder.setCancelable(true);
        builder.setPositiveButton("注册", (OnClickListener) activity);
        builder.setNegativeButton("登陆", (OnClickListener) activity);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * 显示下拉列表TextViewDialog<br/>
     * activity回调<br/>
     */
    public static void showTextViewDialog(Activity activity, String title, String msg, int
            resource, String tag,
                                          String[] texts, ListViewBaseAdapter<?> adapter, int
                                                  childIndex)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);
        builder.setMessage(msg);
        ViewGroup rootView = (ViewGroup) LayoutInflater.from(activity).inflate(resource, null);
        for (int index = 0; index < rootView.getChildCount(); index++)
        {
            TextView et = (TextView) rootView.getChildAt(index);
            if (texts != null)
            {
                et.setText(texts[index]);
            }
            if (childIndex == index)
            {
                if (adapter != null)
                {
                    AutoCompleteTextView actv = (AutoCompleteTextView) et;
                    adapter.transport(et);
                    actv.setAdapter(adapter);
                }
            }
        }
        rootView.setTag(tag);
        builder.setView(rootView);
        builder.setCancelable(true);
        builder.setPositiveButton("确定", (OnClickListener) activity);
        builder.setNegativeButton("取消", (OnClickListener) activity);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * 显示服务类型对话框<br/>
     */
   /* public static void showServiceDialog(FunctionActivity activity, String title,
                                         String msg)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);
        builder.setMessage(msg);
        ViewGroup rootView = (ViewGroup) LayoutInflater.from(activity).inflate(R.layout
                        .dialog_choose_server_type,
                null);
        ListView listView = (ListView) rootView.findViewById(R.id.listView);

        String[] provinces = new String[]{"不限", "北京", "天津", "上海", "重庆", "河北", "山西", "辽宁", "吉林",
                "黑龙江", "江苏", "浙江",
                "安徽", "福建", "江西", "山东", "河南", "湖北", "湖南", "广东", "海南", "四川", "贵州", "云南", "陕西",
                "甘肃", "青海", "台湾", "内蒙古",
                "广西", "西藏", "宁夏", "新疆", "香港", "澳门"};
        List<String> list = new ArrayList<String>();
        for (String text : provinces)
        {
            list.add(text);
        }
        ServerTypeAdapter adapter = new ServerTypeAdapter(activity, list);
        listView.setAdapter(adapter);

        builder.setView(rootView);
        builder.setCancelable(true);
        AlertDialog dialog = builder.create();
        dialog.show();
    }*/

    /**
     * 显示性别对话框<br/>
     */
    public static void showSexDialog(final HashMap<String, Object> map, final ListViewBaseAdapter<?>
            adapter,
                                     FunctionActivity activity)
    {
        String sex = (String) map.get("value");
        final Dialog dialog = new Dialog(activity, R.style.dialog_translucent);
        ViewGroup rootView = (ViewGroup) LayoutInflater.from(activity).inflate(R.layout
                .dialog_sex, null);
        ImageView iv_selected_man = (ImageView) rootView.findViewById(R.id.iv_selected_man);
        ImageView iv_selected_woman = (ImageView) rootView.findViewById(R.id.iv_selected_woman);
        if (sex.contains("男"))
        {
            iv_selected_man.setVisibility(View.VISIBLE);
        }
        else if (sex.contains("女"))
        {
            iv_selected_woman.setVisibility(View.VISIBLE);
        }
        LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams
                .WRAP_CONTENT);
        dialog.addContentView(rootView, params);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
        ((View) iv_selected_man.getParent()).setOnClickListener(new View.OnClickListener()
        {

            public void onClick(View view)
            {
                dialog.dismiss();
                map.put("value", "男士");
                adapter.notifyDataSetChanged();
            }
        });
        ((View) iv_selected_woman.getParent()).setOnClickListener(new View.OnClickListener()
        {

            public void onClick(View view)
            {
                dialog.dismiss();
                map.put("value", "女士");
                adapter.notifyDataSetChanged();
            }
        });
    }

    /**
     * 显示权限提示对话框
     */
    public static void showTipsDialog(final Context context)
    {
        new android.support.v7.app.AlertDialog.Builder(context)
                .setTitle("提示信息")
                .setMessage("当前应用缺少必要权限，该功能暂时无法使用。如若需要，请单击【确定】按钮前往设置中心进行权限授权。")
                .setNegativeButton("取消", null)
                .setPositiveButton("确定", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        TDevice.openAppSettings(context);
                    }
                }).show();
    }

    /**
     * 判断Activity是否销毁
     *
     * @param activity
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static boolean isValidActivity(Activity activity)
    {
        if (activity.isDestroyed() || activity.isFinishing())
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
