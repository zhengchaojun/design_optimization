package qlsl.androiddesign.api.fragment.commonfragment;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.fragment.basefragment.ZLazyFragment;
import qlsl.androiddesign.api.util.other.Utils;

public class FragmentTab1 extends ZLazyFragment
{
    private RecyclerView recyclerView;

    private Utils utils = Utils.getInstance();

    private View headerView;

    @Override
    public int getLayoutId()
    {
        return R.layout.tab1;
    }

    @Override
    public Object newP()
    {
        return null;
    }

    @Override
    protected void initView()
    {
        recyclerView = (RecyclerView) findView(R.id.recyclerView);
//        recyclerView.setLayoutManager(new GridLayoutManager(recyclerView.getContext(), 8,
// GridLayoutManager.VERTICAL, false));
//        ForumMenuAdapter mAdapter = (ForumMenuAdapter) recyclerView.getAdapter();
//        if (mAdapter == null) {
//            mAdapter = new ForumMenuAdapter(recyclerView);
//            mAdapter.setOnRVItemClickListener(this);
//            mAdapter.setOnRVItemLongClickListener(this);
//            mAdapter.setOnItemChildClickListener(this);
//            mAdapter.setOnItemChildLongClickListener(this);
//            mAdapter.addHeaderView(headerView = utils.getCustomHeaderView(activity, this, this));
//        }
//        recyclerView.setAdapter(mAdapter.getHeaderAndFooterAdapter());
    }

    @Override
    protected void initData()
    {
        // 底部设置
        Integer[] bottomImags = {R.drawable.shopping_h, R.drawable.shopping_h, R.drawable
                .shopping_h, R.drawable.shopping_h,
                R.drawable.shopping_h, R.drawable.shopping_h,};
        Integer[] imgs = new Integer[]{R.drawable.shopping_h, R.drawable.shopping_h, R.drawable
                .shopping_h,
                R.drawable.shopping_h, R.drawable.shopping_h, R.drawable.shopping_h, R.drawable
                .shopping_h,
                R.drawable.shopping_h};
        String[] texts = new String[]{"商城", "论坛", "评估", "二手车", "签到", "招聘", "快修", "快拖"};
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        for (int index = 0; index < imgs.length; index++)
        {
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("icon", imgs[index]);
            map.put("text", texts[index]);
            list.add(map);
        }
        for (int index = 0; index < bottomImags.length; index++)
        {
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("icon", bottomImags[index]);
            list.add(map);
        }
//        ((ForumMenuAdapter) ((BGAHeaderAndFooterAdapter) recyclerView.getAdapter())
// .getInnerAdapter()).setData(list);


        ArrayList<String> urls = new ArrayList<String>();
        urls.add("https://timgsa.baidu" +
                ".com/timg?image&quality=80&size=b9999_10000&sec=1491799501432&di" +
                "=3c97d4383d125002673c24c699354fdb&imgtype=0&src=http%3A%2F%2Fww2.sinaimg" +
                ".cn%2Flarge%2F85d77acdgw1f4hzleh53tg20fw07n1kx.jpg");
        urls.add("https://timgsa.baidu" +
                ".com/timg?image&quality=80&size=b9999_10000&sec=1491799501432&di" +
                "=3c97d4383d125002673c24c699354fdb&imgtype=0&src=http%3A%2F%2Fww2.sinaimg" +
                ".cn%2Flarge%2F85d77acdgw1f4hzleh53tg20fw07n1kx.jpg");
        urls.add("https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=1735850437," +
                "744616131&fm=23&gp=0.jpg");
        urls.add("https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=1735850437," +
                "744616131&fm=23&gp=0.jpg");
        urls.add("https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=1735850437," +
                "744616131&fm=23&gp=0.jpg");
//        ((BGABanner) headerView.getTag()).setData(urls, null);
    }

    @Override
    protected void initListener()
    {

    }


//    @Override
//    public void onItemChildClick(ViewGroup parent, View childView, int position) {
//        showToast("position===" + position);
//        Log.e(Config.LOG_TAG, "position===" + position);
//    }
//
//    @Override
//    public boolean onItemChildLongClick(ViewGroup parent, View childView, int position) {
//        return false;
//    }
//
//    @Override
//    public void onRVItemClick(ViewGroup parent, View itemView, int position) {
//        showToast("position===" + position);
//        Log.e(Config.LOG_TAG, "position===" + position);
//    }
//
//    @Override
//    public boolean onRVItemLongClick(ViewGroup parent, View itemView, int position) {
//        return false;
//    }
//
//    @Override
//    public void fillBannerItem(BGABanner banner, ImageView itemView, String model, int position) {
//        ILFactory.getLoader().loadNetRound(itemView, model, new ILoader.Options(R.drawable
// .iv_default, R.drawable.iv_default).scaleType(ImageView.ScaleType.FIT_XY));
//    }
//
//    @Override
//    public void onBannerItemClick(BGABanner banner, ImageView itemView, String model, int
// position) {
//        showToast("position===" + position);
//    }
}
