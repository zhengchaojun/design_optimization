package qlsl.androiddesign.api.adapter.baseadapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import java.util.List;

public class FragmentPagerAdapter extends android.support.v4.app.FragmentPagerAdapter {

    private List<Fragment> list;

    public FragmentPagerAdapter(FragmentManager manager, List<Fragment> list) {
        super(manager);
        this.list = list;
    }

    public int getCount() {
        return list.size();
    }

    public Fragment getItem(int position) {
        return list.get(position);
    }

}
