package qlsl.androiddesign.api.util.commonutil;

/*
 * Copyright (C) 2015 Drakeet <drakeet.me@gmail.com>
 *
 * This file is part of Meizhi
 *
 * Meizhi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Meizhi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Meizhi.  If not, see <http://www.gnu.org/licenses/>.
 */

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;

import qlsl.androiddesign.api.activity.R;


/**
 * 避免同样的信息多次触发重复弹出的问题
 */
public class ToastUtils {

    public static Context sContext;

    private ToastUtils() {
    }

    public static void register(Context context) {
        sContext = new WeakReference<Context>(context.getApplicationContext()).get();
    }

    private static void check() {
        if (sContext == null) {
            throw new NullPointerException(
                    "Must initial call ToastUtils.register(Context activity) in your "
                            + "<? " + "extends Application class>");
        }
    }

    public static void showShort(int resId) {
        check();
        Toast.makeText(sContext, resId, Toast.LENGTH_SHORT).show();
    }

    public static void showShort(String message) {
        check();
        showShortToast(message);
    }

    public static void showLong(int resId) {
        check();
        Toast.makeText(sContext, resId, Toast.LENGTH_LONG).show();
    }

    public static void showLong(String message) {
        check();
        showLongToast(message);
    }

    public static void showLongX2(String message) {
        showLong(message);
        showLong(message);
    }

    public static void showLongX2(int resId) {
        showLong(resId);
        showLong(resId);
    }

    public static void showLongX3(int resId) {
        showLong(resId);
        showLong(resId);
        showLong(resId);
    }

    public static void showLongX3(String message) {
        showLong(message);
        showLong(message);
        showLong(message);
    }

    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private static String oldMsg;
    protected static Toast toast = null;
    private static long oneTime = 0;
    private static long twoTime = 0;

    private static String oldMsgShot;
    protected static Toast toastShot = null;
    private static long oneTimeShot = 0;
    private static long twoTimeShot = 0;

    private static String oldMsgLong;
    protected static Toast toastLong = null;
    private static long oneTimeLong = 0;
    private static long twoTimeLong = 0;

    public static void showNoDataToast(String s) {
        check();
        if (toast == null) {
            toast = Toast.makeText(sContext, s, Toast.LENGTH_LONG);

            LinearLayout toastView = (LinearLayout) toast.getView();
            toastView.setOrientation(LinearLayout.HORIZONTAL);
            toastView.setGravity(Gravity.CENTER_VERTICAL);
            toastView.setBackgroundResource(R.drawable.bg_toast);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
            params.leftMargin = 10;
            TextView textView = (TextView) toastView.getChildAt(0);
            textView.setLayoutParams(params);
            textView.setGravity(Gravity.CENTER);
            ImageView imageView = new ImageView(sContext);
            imageView.setImageResource(R.drawable.iv_no_data_icon);
            toastView.addView(imageView, 0);

            toast.show();
            oneTime = System.currentTimeMillis();
        } else {
            twoTime = System.currentTimeMillis();
            if (s.equals(oldMsg)) {
                if (twoTime - oneTime > Toast.LENGTH_SHORT) {
                    toast.show();
                }
            } else {
                oldMsg = s;
                toast.setText(s);
                toast.show();
            }
            oneTime = twoTime;
        }
    }


    public static void showShortToast(String s) {
        check();
        if (toastShot == null) {
            toastShot = Toast.makeText(sContext, s, Toast.LENGTH_SHORT);

            LinearLayout toastView = (LinearLayout) toastShot.getView();
            toastView.setBackgroundResource(R.drawable.bg_toast);

            toastShot.show();
            oneTimeShot = System.currentTimeMillis();
        } else {
            twoTimeShot = System.currentTimeMillis();
            if (s.equals(oldMsgShot)) {
                if (twoTimeShot - oneTimeShot > Toast.LENGTH_SHORT) {
                    toastShot.show();
                }
            } else {
                oldMsgShot = s;
                toastShot.setText(s);
                toastShot.show();
            }
            oneTimeShot = twoTimeShot;
        }
    }


    public static void showLongToast(String s) {
        check();
        if (toastLong == null) {
            toastLong = Toast.makeText(sContext, s, Toast.LENGTH_LONG);

            LinearLayout toastView = (LinearLayout) toastLong.getView();
            toastView.setBackgroundResource(R.drawable.bg_toast);

            toastLong.show();
            oneTimeLong = System.currentTimeMillis();
        } else {
            twoTimeLong = System.currentTimeMillis();
            if (s.equals(oldMsgLong)) {
                if (twoTimeLong - oneTimeLong > Toast.LENGTH_SHORT) {
                    toastLong.show();
                }
            } else {
                oldMsgLong = s;
                toastLong.setText(s);
                toastLong.show();
            }
            oneTimeLong = twoTimeLong;
        }
    }

    public static void showEnterToast(String s, int resId) {
        check();
        if (toast == null) {
            toast = Toast.makeText(sContext, s, Toast.LENGTH_LONG);

            toast.setGravity(Gravity.CENTER, 0, 0);
            LinearLayout toastView = (LinearLayout) toast.getView();
            toastView.setOrientation(LinearLayout.HORIZONTAL);
            toastView.setGravity(Gravity.CENTER_VERTICAL);
            toastView.setBackgroundResource(R.drawable.bg_toast);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
            params.leftMargin = 10;
            TextView textView = (TextView) toastView.getChildAt(0);
            textView.setLayoutParams(params);
            textView.setGravity(Gravity.CENTER);
            ImageView imageView = new ImageView(sContext);
            imageView.setImageResource(resId);
            toastView.addView(imageView, 0);

            toast.show();
            oneTime = System.currentTimeMillis();
        } else {
            twoTime = System.currentTimeMillis();
            if (s.equals(oldMsg)) {
                if (twoTime - oneTime > Toast.LENGTH_SHORT) {
                    toast.show();
                }
            } else {
                oldMsg = s;
                toast.setText(s);
                toast.show();
            }
            oneTime = twoTime;
        }
    }

    public static void showExitToast(String s, int resId) {
        check();
        if (toast == null) {
            toast = Toast.makeText(sContext, s, Toast.LENGTH_LONG);

            toast.setGravity(Gravity.CENTER, 0, 0);
            LinearLayout toastView = (LinearLayout) toast.getView();
            toastView.setOrientation(LinearLayout.HORIZONTAL);
            toastView.setGravity(Gravity.CENTER_VERTICAL);
            toastView.setBackgroundResource(R.drawable.bg_toast);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
            params.leftMargin = 10;
            TextView textView = (TextView) toastView.getChildAt(0);
            textView.setLayoutParams(params);
            textView.setGravity(Gravity.CENTER);
            ImageView imageView = new ImageView(sContext);
            imageView.setImageResource(resId);
            toastView.addView(imageView, 0);

            toast.show();
            oneTime = System.currentTimeMillis();
        } else {
            twoTime = System.currentTimeMillis();
            if (s.equals(oldMsg)) {
                if (twoTime - oneTime > Toast.LENGTH_SHORT) {
                    toast.show();
                }
            } else {
                oldMsg = s;
                toast.setText(s);
                toast.show();
            }
            oneTime = twoTime;
        }
    }


    /**
     * 显示 Snackbar
     *
     * @param activity
     * @param message
     * @param isLong
     */
    public static void showSnackbar(Activity activity, String message, boolean isLong) {
        if (activity == null) {
            return;
        }
        View view = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(view, message, isLong ? Snackbar.LENGTH_LONG : Snackbar.LENGTH_SHORT).show();
    }


    /**
     * 显示 Snackbar
     *
     * @param activity
     * @param message
     * @param isLong
     */
    public static void showSnackbar(Activity activity, String message, boolean isLong, String clickName, final View.OnClickListener listener) {
        if (activity == null) {
            return;
        }
        View view = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar snackbar = Snackbar.make(view, message, isLong ? Snackbar.LENGTH_LONG : Snackbar.LENGTH_SHORT);
        View snackbarView = snackbar.getView();
        if (snackbarView != null) {
            snackbarView.setBackgroundColor(ContextCompat.getColor(activity, R.color.darkgray));
        }
        snackbar.setAction(clickName, listener).show();
    }
}
