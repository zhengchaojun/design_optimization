package qlsl.androiddesign.api.entity.commonentity;

import com.alibaba.fastjson.annotation.JSONField;

import qlsl.androiddesign.api.entity.baseentity.BaseModel;

/**
 * 类描述：
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传：
 * 作者：郑朝军 on 2017/7/6 9:21
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/7/6 9:21
 * 修改备注：
 */
public class ReceiveMessage extends BaseModel
{// 消息列表：判断接收人是否为当前登录的用户，判断是否已读,是为群发，,显示的是发送人，计算发送人发送了多少条，显示最后发送事件，显示最后发送内容，
    //
    /**
     * tuser : xq (接收人)
     * id : 103
     * mst : 0 (消息状态) 0 是未读 1 是已读
     * tuserid : 601 (接收人ID)
     * fsnr : 好好好 (发送内容)
     * isqf : 0   (是否群发)【0，1】【0为否，1为是】
     * fuser : admin  (发送人)
     * fuserid : 101  (发送人ID)
     * fsrq : 2017-06-27 14:53:35  (发送日期)}
     */
    private String tuser;
    private Integer id;
    private Integer mst;
    private Integer tuserid;
    private String fsnr;
    private Integer isqf;
    private String fuser;
    private Integer fuserid;

    private String fsrq;

    public String getTuser()
    {
        return tuser;
    }

    public void setTuser(String tuser)
    {
        this.tuser = tuser;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Integer getMst()
    {
        return mst;
    }

    public void setMst(Integer mst)
    {
        this.mst = mst;
    }

    public Integer getTuserid()
    {
        return tuserid;
    }

    public void setTuserid(Integer tuserid)
    {
        this.tuserid = tuserid;
    }

    public String getFsnr()
    {
        return fsnr;
    }

    public void setFsnr(String fsnr)
    {
        this.fsnr = fsnr;
    }

    public Integer getIsqf()
    {
        return isqf;
    }

    public void setIsqf(Integer isqf)
    {
        this.isqf = isqf;
    }

    public String getFuser()
    {
        return fuser;
    }

    public void setFuser(String fuser)
    {
        this.fuser = fuser;
    }

    public Integer getFuserid()
    {
        return fuserid;
    }

    public void setFuserid(Integer fuserid)
    {
        this.fuserid = fuserid;
    }

    public String getFsrq()
    {
        return fsrq;
    }

    public void setFsrq(String fsrq)
    {
        this.fsrq = fsrq;
    }
}
