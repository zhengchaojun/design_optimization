package qlsl.androiddesign.api.http.service.commonservice;

import android.view.View;

import org.greenrobot.eventbus.EventBus;

import java.lang.ref.WeakReference;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.activity.baseactivity.FunctionActivity;
import qlsl.androiddesign.api.activity.commonactivity.MainActivity;
import qlsl.androiddesign.api.application.SoftwareApplication;
import qlsl.androiddesign.api.fragment.basefragment.ZLazyFragment;
import qlsl.androiddesign.api.http.service.baseservice.ServiceBase;
import qlsl.androiddesign.api.util.commonutil.ExtraUtils;
import qlsl.androiddesign.api.util.commonutil.TDevice;
import qlsl.androiddesign.api.util.other.Utils;

/**
 * 系统模块：主界面
 * 作者：郑朝军 on 2017/3/27 22:54
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */

public class MainService extends ServiceBase<MainActivity>
{
    private String className = getClassName(MainService.class);

    private Utils m_utils = Utils.getInstance();

    protected ExtraUtils m_itemExtra;// 上个页面传递过来的对象

    public MainService(FunctionActivity activity)
    {
        super(activity);
    }


    public void initView()
    {

    }

    public void initData()
    {
        if (!TDevice.isServiceRunning(SoftwareApplication.getInstance(), "qlsl.androiddesign.api" +
                ".domain.service.PollingService"))
        {
            getV().showToast("正在开启消息接收服务");
            startPollingService();
        }
        System.gc();
    }

    protected void initListener()
    {

    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
            default:
                ((ZLazyFragment) getV().getSupportFragmentManager().findFragmentById(R.id
                        .fragment_zmapmain)).onClick(view);
                break;
        }
    }


    private void startPollingService()
    {

    }

    private void eventBusPost()
    {
        EventBus.getDefault().post(m_itemExtra);
    }
}
