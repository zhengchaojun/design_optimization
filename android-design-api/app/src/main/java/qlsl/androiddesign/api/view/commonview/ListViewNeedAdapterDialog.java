package qlsl.androiddesign.api.view.commonview;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ListView;

import qlsl.androiddesign.api.activity.R;


/**
 * 类描述：公共ListViewDialog
 * 需要传入的键：需要传递adapter 适用于小批量数据
 * 传入的值类型：****
 * 传入的值含义：****
 * 是否必传：****
 * 作者：郑朝军 on 2017/6/14 11:29
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/14 11:29
 * 修改备注：
 */

public class ListViewNeedAdapterDialog extends Dialog implements
        View.OnClickListener
{

    public enum Type
    {
        CENTER, BOTTOM
    }

    private ListView listView;

    private Activity activity;

    private BaseAdapter adapter;

    private int dialogWidth = 0;

    private int listHeight = 0;

    private Type type = Type.CENTER;

    private Object tag;// 设置临时存放对象

    public interface OnQuickOptionformClick
    {
        void onQuickOptionClick(int id);
    }

    private OnQuickOptionformClick mListener;

    private ListViewNeedAdapterDialog(Context context, boolean flag,
                                      OnCancelListener listener)
    {
        super(context, flag, listener);
    }

    /**
     * 首先调用这个方法
     *
     * @param context
     * @param defStyle
     */
    @SuppressLint("InflateParams")
    private ListViewNeedAdapterDialog(Context context, int defStyle)
    {
        super(context, defStyle);
        View contentView = getLayoutInflater().inflate(
                R.layout.common_list_view_no_margin_divider, null);
        listView = (ListView) contentView.findViewById(R.id.listView);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.setContentView(contentView);

    }

    /**
     * 再调用这个方法
     */
    public ListViewNeedAdapterDialog(Activity activity, BaseAdapter adapter)
    {
        this(activity, R.style.quick_option_dialog);
        this.activity = activity;
        this.adapter = adapter;
    }

    /**
     * 最后调用这个方法
     */
    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        if (type == Type.BOTTOM)
        {
            getWindow().setGravity(Gravity.BOTTOM);
        }
        else
        {
            getWindow().setGravity(Gravity.CENTER);
        }

        WindowManager m = getWindow().getWindowManager();
        Display d = m.getDefaultDisplay();
        WindowManager.LayoutParams p = getWindow().getAttributes();

        p.width = dialogWidth == 0 ? (int) (d.getWidth() * 0.8) : dialogWidth;

        getWindow().setAttributes(p);
        initData();

        if (listHeight != 0)
        {
            setListHeight();
        }
    }

    private void initData()
    {
        listView.setAdapter(adapter);
    }

    public void setOnQuickOptionformClickListener(OnQuickOptionformClick lis)
    {
        mListener = lis;
    }

    @Override
    public void onClick(View v)
    {
        final int id = v.getId();
        if (mListener != null)
        {
            mListener.onQuickOptionClick(id);
        }
        dismiss();
    }

    public int getDialogWidth()
    {
        return dialogWidth;
    }

    public void setDialogWidth(int dialogWidth)
    {
        this.dialogWidth = dialogWidth;
    }

    public Type getType()
    {
        return type;
    }

    public void setType(Type type)
    {
        this.type = type;
    }

    public int getListHeight()
    {
        return listHeight;
    }

    public void setListHeight(int listHeight)
    {
        this.listHeight = listHeight;
    }

    /**
     * 重新设置listView高度
     */
    private void setListHeight()
    {
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = listHeight;
        listView.setLayoutParams(params);
    }

    public Object getSelectItem(View view)
    {
        int position = listView.getPositionForView(view);
        BaseAdapter adapter = (BaseAdapter) listView.getAdapter();
        return adapter.getItem(position);
    }

    public Object getTag()
    {
        return tag;
    }

    public void setTag(Object tag)
    {
        this.tag = tag;
    }

    public ListView getListView()
    {
        return listView;
    }

    public void setListView(ListView listView)
    {
        this.listView = listView;
    }

}
