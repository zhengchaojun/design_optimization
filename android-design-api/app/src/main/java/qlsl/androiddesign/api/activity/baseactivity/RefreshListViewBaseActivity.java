package qlsl.androiddesign.api.activity.baseactivity;

import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.cx.annotation.aspect.SingleClick;
import com.itheima.pulltorefreshlib.PullToRefreshListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.entity.commonentity.Pager;
import qlsl.androiddesign.api.listener.OnPullRefreshListener;
import qlsl.androiddesign.api.util.commonutil.ReflectUtil;

/**
 * 类描述：带下拉刷新ListView基类
 * 需要传入的键：****
 * 传入的值类型：****
 * 传入的值含义：****
 * 是否必传：****
 * 作者：郑朝军 on 2017/6/14 11:29
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/14 11:29
 * 修改备注：
 */
public abstract class RefreshListViewBaseActivity<D, P extends CommonContract.IService> extends
        CommonActivityBase<P>
{
    protected PullToRefreshListView refreshView;

    protected List<D> list = new ArrayList<D>();

    protected Pager pager = new Pager();

    @Override
    public int getLayoutId()
    {
        return R.layout.activity_refresh_list_view_base;
    }

    @Override
    public boolean useEventBus()
    {
        return false;
    }

    @Override
    public P newP()
    {
        return ReflectUtil.getT(this, 1);
    }

    @Override
    protected void initView()
    {
        refreshView = findView(R.id.refreshListView);
    }


    @Override
    protected void initData()
    {
        notifyDataSetChanged();
    }

    @Override
    protected void initListener()
    {
        refreshView.setOnRefreshListener(refreshListener2);
    }

    @SingleClick
    public void onClick(View view)
    {

    }

    public void showNetWorkSucceedData(String method, Object values)
    {

    }

    public <T> void showData(T... t)
    {
        if (t[0] instanceof HashMap)
        {
            HashMap<String, Object> map = (HashMap<String, Object>) t[0];
            List<D> list_result = (List<D>) map.get("list");
            pager = (Pager) map.get("pager");
            if (pager.getPageNo() == 1)
            {
                resetList(list_result);
            }
            else
            {
                list.addAll(list_result);
            }
            notifyDataSetChanged();
        }
    }

    public <T> void showNoData(T... t)
    {

    }


    protected void resetList(List<D> list_result)
    {
        list.clear();
        list.addAll(list_result);
    }

    protected void notifyDataSetChanged()
    {
        BaseAdapter adapter = getBaseAdapter(refreshView);
        if (adapter == null)
        {
            adapter = getAdapter();
            refreshView.setAdapter(adapter);
        }
        else
        {
            adapter.notifyDataSetChanged();
        }
        refreshView.onRefreshComplete();
    }

    public abstract BaseAdapter getAdapter();

    private OnPullRefreshListener<ListView> refreshListener2 = new OnPullRefreshListener<ListView>()
    {
        public void onPullDown()
        {

        }

        public void onPullUp()
        {
            super.onPullUp(refreshView, pager);
        }

        public void nextPager()
        {

        }
    };
}
