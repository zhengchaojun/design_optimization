package qlsl.androiddesign.api.listener;


import android.view.View;

import com.itheima.pulltorefreshlib.PullToRefreshBase;

import qlsl.androiddesign.api.entity.commonentity.Pager;


public abstract class OnPullRefreshListener<T extends View> extends OnRefreshListenerBase<T> implements
        PullToRefreshBase.OnRefreshListener2<T>
{
    public OnPullRefreshListener()
    {
    }

    @Override
    public void onPullDownToRefresh(com.itheima.pulltorefreshlib.PullToRefreshBase<T> pullToRefreshBase)
    {
        onPullDown();
    }

    @Override
    public void onPullUpToRefresh(com.itheima.pulltorefreshlib.PullToRefreshBase<T> pullToRefreshBase)
    {
        onPullUp();
    }

    @SuppressWarnings("deprecation")
    public void onPullUp(PullToRefreshBase<T> refreshView, Pager pager)
    {
        if (pager == null)
        {
            onLoadComplete(refreshView);
            return;
        }
        int pageNo = pager.getPageNo();// 1
        int totalCount = pager.getTotalCount();// 70
        int totalPage = pager.getTotalPage();// 5
        if (pageNo < totalPage)
        {// 如果传递过去的参数小于5就调用nextPager()
            nextPager();
        }
        else
        {
            onLoadComplete(refreshView, totalPage, totalCount);
        }
    }

    public abstract void onPullDown();

    public abstract void onPullUp();

    public abstract void nextPager();
}
