package qlsl.androiddesign.api.http.service.baseservice;


import qlsl.androiddesign.api.activity.baseactivity.CommonContract;

/**
 * 功能：网络，数据库操作，等等其他耗时操作公共的服务(Service)基类
 * 作者：郑朝军 on 2017/4/5 19:41
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
public class CommonServiceBase<V extends CommonContract.IView> implements CommonContract.IService<V>
{
    private V v;

    @Override
    public void attachV(V view)
    {
        v = view;
    }

    @Override
    public void detachV()
    {
        v = null;
    }

    protected V getV()
    {
        return v;
    }
}
