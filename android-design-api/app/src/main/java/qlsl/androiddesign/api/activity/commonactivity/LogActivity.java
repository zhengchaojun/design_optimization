package qlsl.androiddesign.api.activity.commonactivity;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.activity.baseactivity.CommonActivityBase;
import qlsl.androiddesign.api.library.email.MultiMailsender;
import qlsl.androiddesign.api.listener.WebViewLoadListener;
import qlsl.androiddesign.api.popupwindow.subwindow.quickaction.ActionItem;
import qlsl.androiddesign.api.popupwindow.subwindow.quickaction.QuickAction;
import qlsl.androiddesign.api.properties.OrderedProperties;
import qlsl.androiddesign.api.util.commonutil.Log;


/**
 * 作者：郑朝军 on 2017/4/5 13:05
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */

public class LogActivity extends CommonActivityBase implements QuickAction.OnActionItemClickListener
{

    WebView webView;
    /**
     * 存储日志接收者信息的文件的名称<br/>
     * 文件路径在data/data/包名/files目录下<br/>
     */
    private final String LOG_RECEIVER_FILE_NAME = "log-receiver.txt";

    private OrderedProperties properties;

    private QuickAction quickAction;

    private final String[] actionItemNames = new String[]{"发送日志", "发送管理", "搜索标签", "日志设置", "清除日志"};

    private final int[] actionItemIcons = new int[]{R.drawable.app_icon_default, R.drawable
            .app_icon_default,
            R.drawable.app_icon_default, R.drawable.app_icon_default, R.drawable.app_icon_default};

    @Override
    public int getLayoutId()
    {
        return R.layout.activity_log;
    }

    @Override
    public boolean useEventBus()
    {
        return false;
    }

    @Override
    public Object newP()
    {
        return null;
    }

    @Override
    protected void initView()
    {
        setTitle("程序日志");
        webView = (WebView) findView(R.id.webView);
        setRightButtonResource(R.drawable.btn_menu);
        showRightButton();
        setTitleBarBackgroundResource(R.drawable.common_title_view);
        if (Log.isOutPhone)
        {
            webView.setBackgroundColor(activity.getResources().getColor(R.color.bg_log));
        }
        else
        {
            webView.setBackgroundResource(R.drawable.common_content_view);
        }
        webView.setHorizontalScrollBarEnabled(false);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDefaultTextEncodingName("utf-8");
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.setWebViewClient(new WebViewLoadListener());

        quickAction = new QuickAction(activity, QuickAction.VERTICAL);
        quickAction.setAnimStyle(QuickAction.ANIM_AUTO);
        for (int index = 0; index < actionItemNames.length; index++)
        {
            ActionItem actionItem = new ActionItem(index, actionItemNames[index],
                    activity.getResources().getDrawable(actionItemIcons[index]));
            actionItem.setSticky(true);
            quickAction.addActionItem(actionItem);
        }
    }

    @Override
    protected void initData()
    {
        showLogInfo();
        presettingReceivers();
    }

    @Override
    protected void initListener()
    {
        quickAction.setOnActionItemClickListener(this);
    }

    protected void doClickRightButton(View view)
    {
        quickAction.show(view);
    }

    private void showLogInfo()
    {
        String url = Log.getLogPath(activity);
        File file = new File(url);
        if (file.exists())
        {
            webView.setVisibility(View.VISIBLE);
            webView.loadUrl("file://" + url);
        }
        else
        {
            webView.setVisibility(View.GONE);
        }
    }

    /**
     * 预设收件人<br/>
     */
    private void presettingReceivers()
    {
        String file_dir = activity.getFilesDir().getAbsolutePath() + File.separator;
        File file = new File(file_dir + LOG_RECEIVER_FILE_NAME);
        properties = new OrderedProperties();
        try
        {
            if (!file.exists())
            {
                String[] receivers = activity.getResources().getStringArray(R.array
                        .presetting_log_receiver);
                for (String current_receiver : receivers)
                {
                    String[] receiver = current_receiver.split(":");
                    properties.put(receiver[0], receiver[1]);
                }
                synchToFile();
            }
            else
            {
                FileInputStream in = activity.openFileInput(LOG_RECEIVER_FILE_NAME);
                properties.load(in);
                in.close();
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

    }

    private void synchToFile()
    {
        try
        {
            FileOutputStream out = activity.openFileOutput(LOG_RECEIVER_FILE_NAME, Context
                    .MODE_PRIVATE);
            properties.store(out, "收件人信息");
            out.close();
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void onClick(DialogInterface dialogInterface, int which)
    {
//        if (which == -1) {
//            String url = Log.getLogPath(activity);
//            File file = new File(url);
//            if (file.exists()) {
//                AlertDialog dialog = (AlertDialog) dialogInterface;
//                EditText et_name = (EditText) dialog.findViewById(R.id.et_name);
//                EditText et_email = (EditText) dialog.findViewById(R.id.et_email);
//                EditText et_title = (EditText) dialog.findViewById(R.id.et_title);
//                EditText et_content = (EditText) dialog.findViewById(R.id.et_content);
//
//                String name = et_name.getText().toString();
//                String email = et_email.getText().toString();
//                String title = et_title.getText().toString();
//                String content = et_content.getText().toString();
//
//                if (StringUtils.isEmail(email)) {
//                    if (TextUtils.isEmpty(name)) {
//                        name = "姓名";
//                    }
//                    if (TextUtils.isEmpty(title)) {
//                        title = "标题";
//                    }
//                    sendLogFileToEmail(file, name, email, title, content);
//                } else {
//                    showToast("邮箱输入不合法！");
//                }
//            } else {
//                showToast("Log日志文件不存在!");
//            }
//        }
    }

    /**
     * 后台发送含附件(Log日志)的邮件<br/>
     */
    private void sendLogFileToEmail(final File file, final String name, final String email, final
    String title,
                                    final String content)
    {
        new Thread()
        {
            public void run()
            {
                Log.i("mail", "正在发送邮件：Log日志<br/>文件：" + file);
                MultiMailsender.MultiMailSenderInfo mailInfo = new MultiMailsender
                        .MultiMailSenderInfo();
                mailInfo.setMailServerHost("smtp.qq.com");
                mailInfo.setMailServerPort("25");
                mailInfo.setValidate(true);
                mailInfo.setUserName("3301436616@qq.com");
                mailInfo.setPassword("cunonzupwfgscida");
                mailInfo.setFromAddress("3301436616@qq.com");
                mailInfo.setToAddress(email);
                mailInfo.setSubject("Log日志-" + name + "-" + title);
                mailInfo.setContent(content);
                mailInfo.setFiles(new File[]{file});
                MultiMailsender mailSender = new MultiMailsender();
                mailSender.sendMailtoMultiReceiver(mailInfo);

            }

            ;
        }.start();

    }

    private void doClickRightView(View view)
    {
        quickAction.show(view);
    }

    public void onItemClick(QuickAction source, int pos, int actionId)
    {
        switch (pos)
        {
            case 0:
                doClickSendView();
                break;
            case 1:
                doClickSendManagerView();
                break;
            case 2:
                showToast("正在开发中");
                break;
            case 3:
                doClickLogSettingView();
                break;
            case 4:
                diClickClearLogView();
                break;
        }
    }

    private void doClickSendView()
    {
//        if (webView.getVisibility() == View.VISIBLE) {
//            List<Map<String, Object>> list = properties.getList("name", "email");
//            LogEmailAdapter adapter = new LogEmailAdapter(activity, list);
//            DialogUtil.showEditTextDialog(activity, "发送", null, R.layout.dialog_send_log, null,
// null, adapter, 1);
//        }
    }

    private void doClickSendManagerView()
    {
//        startActivity(SendManagerActivity.class);
    }

    private void doClickLogSettingView()
    {
//        Intent intent = new Intent(activity, LogSettingActivity.class);
//        activity.startActivity(intent);
    }

    private void diClickClearLogView()
    {
        boolean isSucceed = Log.deleteLogFile(activity);
        if (isSucceed)
        {
            showLogInfo();
        }
        else
        {
            showToast("清除失败！");
        }
    }
}
