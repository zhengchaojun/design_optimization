package qlsl.androiddesign.api.activity.commonactivity;

import android.content.Intent;
import android.view.View;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.activity.baseactivity.CommonActivityBase;
import qlsl.androiddesign.api.http.service.commonservice.ACopyService;
import qlsl.androiddesign.api.util.commonutil.ValueUtil;
import qlsl.androiddesign.api.view.commonview.SingleEditText;
import qlsl.androiddesign.api.view.commonview.TextView;

/**
 * 类描述：编辑页
 * 需要传入的键：title,content,maxLength,rightText
 * 传入的值类型：String,String,int,String
 * 传入的值含义：标题，内容，最大长度,右按钮文本
 * 是否必传：是，是，是，否
 * 作者：郑朝军 on 2017/6/14 11:29
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/14 11:29
 * 修改备注：
 */
public class EditActivity extends CommonActivityBase<ACopyService>
{
    private int maxLength = 40;

    private SingleEditText et_content;

    private TextView tv_sum;

    @Override
    public int getLayoutId()
    {
        return R.layout.activity_edit;
    }

    @Override
    public boolean useEventBus()
    {
        return false;
    }

    @Override
    public ACopyService newP()
    {
        return new ACopyService();
    }

    @Override
    protected void initView()
    {
        Intent intent = activity.getIntent();
        String title = intent.getStringExtra("title");
        setTitle(title);
        String rightText = intent.getStringExtra("rightText");
        setRightButtonText(rightText == null ? "保存" : rightText);
        showRightButton();
        et_content = (SingleEditText) findViewById(R.id.et_content);
        tv_sum = (TextView) findViewById(R.id.tv_sum);
        maxLength = intent.getIntExtra("maxLength", maxLength);
    }


    @Override
    protected void initData()
    {
        et_content.setSleepSpan(80);
        et_content.startDrawThread(
                activity.getIntent().getStringExtra("content"), tv_sum,
                maxLength);
    }

    @Override
    protected void initListener()
    {

    }

    public void onClick(View view)
    {

    }


    public void showNetWorkSucceedData(String method, Object values)
    {
        setRefresh(true);
        finish();
    }

    public <T> void showData(T... t)
    {

    }

    public <T> void showNoData(T... t)
    {

    }

    protected void doClickRightButton(View view)
    {
        String title = activity.getIntent().getStringExtra("title");
        String content = et_content.getText().toString().trim();
        if (ValueUtil.isEmpty(content))
        {
            showToast("未输入");
            return;
        }
        if (title.contains("编辑用户昵称"))
        {

        }
    }

    public void onPause()
    {
        super.onPause();
        et_content.onPause();
    }

}
