package qlsl.androiddesign.api.adapter.commonadapter;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.List;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.adapter.baseadapter.listviewadapter.ListViewBaseAdapter;
import qlsl.androiddesign.api.constant.SoftwareConstant;
import qlsl.androiddesign.api.util.commonutil.ImageUtils;

/**
 * 公用图片修改适配器<br/>
 */
public class PictureUpdateAdapter extends ListViewBaseAdapter<String>
{
    public PictureUpdateAdapter(Activity activity, List<String> list)
    {
        super(activity, list);
    }

    public View getView(final int position, View convertView, final ViewGroup parent)
    {
        convertView = getItemView(convertView, R.layout.griditem_photo_update, parent);

        ImageView iv_icon = getView(convertView, R.id.iv_icon);
        ImageView iv_delete = getView(convertView, R.id.iv_delete);
        TextView tv_add = getView(convertView, R.id.tv_add);

        final String url = getItem(position);

        if (!TextUtils.isEmpty(url) && url.contains(SoftwareConstant.SDCARD_FLAG))
        {
            ImageUtils.rect(parent.getContext(), new File(url), iv_icon);
        }
        else
        {
            ImageUtils.rect(parent.getContext(), url, iv_icon);
        }

        if (!TextUtils.isEmpty(url))
        {
            iv_icon.setVisibility(View.VISIBLE);
            tv_add.setVisibility(View.GONE);
            iv_delete.setVisibility(View.VISIBLE);
        }
        else
        {
            iv_icon.setVisibility(View.GONE);
            tv_add.setVisibility(View.VISIBLE);
            iv_delete.setVisibility(View.GONE);
        }

        return convertView;
    }
}
