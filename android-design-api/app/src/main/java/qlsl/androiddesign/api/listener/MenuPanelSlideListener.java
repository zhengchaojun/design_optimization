package qlsl.androiddesign.api.listener;

import android.view.View;

import qlsl.androiddesign.api.view.slidinguppanel.SlidingUpPanelLayout;

/**
 * 类描述：滑动菜单监听
 * 创建人：郑朝军
 * 创建时间：2017/6/7 16:14
 * 修改人：郑朝军
 * 修改时间：2017/6/7 16:14
 * 修改备注：
 * 公司:武汉智博创享科技有限公司
 */
public class MenuPanelSlideListener implements SlidingUpPanelLayout.PanelSlideListener
{
    @Override
    public void onPanelSlide(View panel, float slideOffset)
    {

    }

    @Override
    public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState,
                                    SlidingUpPanelLayout.PanelState newState)
    {

    }
}
