package qlsl.androiddesign.api.http.service.baseservice;

import org.json.JSONException;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import io.reactivex.subscribers.ResourceSubscriber;
import qlsl.androiddesign.api.exception.NetError;
import qlsl.androiddesign.api.util.commonutil.Log;

/**
 * 处理公共错误
 */
public abstract class SubcriberBase<T> extends ResourceSubscriber<T>
{
    @Override
    public void onError(Throwable e)
    {
        NetError error = null;
        if (e != null)
        {
            if (!(e instanceof NetError))
            {
                if (e instanceof Exception)
                {
                    error = new NetError(e, NetError.MSG_ECPITON_FAILED);
                } else if (e instanceof UnknownHostException)
                {
                    error = new NetError(e, NetError.MSG_NOCONNECT_FAILED);
                } else if (e instanceof JSONException)
                {
                    error = new NetError(e, NetError.MSG_PARSEERROR_FAILED);
                } else if (e instanceof SocketTimeoutException)
                {
                    error = new NetError(e, NetError.MSG_SERVER_FAILED);
                } else
                {
                    error = new NetError(e, NetError.MSG_OTHER_FAILED);
                }
            } else
            {
                error = (NetError) e;
            }
            onFail(error);
        }

    }

    protected abstract void onFail(NetError error);

    @Override
    public void onComplete()
    {

    }


    protected boolean useCommonErrorHandler()
    {
        return true;
    }


}
