package qlsl.androiddesign.api.adapter.commonadapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.adapter.baseadapter.listviewadapter.ListViewBaseAdapter;

public class CopyAdapter<T> extends ListViewBaseAdapter<T>
{
    public CopyAdapter(Activity activity, List<T> list)
    {
        super(activity, list);
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        // convertView = getItemView(convertView, R.layout.listitem_copy,parent);

        TextView tv_text = getView(convertView, R.id.tv_text);

        T t = getItem(position);

        tv_text.setText(t.toString());

        return convertView;
    }
}
