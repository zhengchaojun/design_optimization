package qlsl.androiddesign.api.activity.commonactivity;

import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.cx.annotation.aspect.SingleClick;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.activity.baseactivity.CommonActivityBase;
import qlsl.androiddesign.api.adapter.commonadapter.MemberCenterAdapter;
import qlsl.androiddesign.api.http.service.commonservice.ACopyService;
import qlsl.androiddesign.api.manager.ActivityManager;
import qlsl.androiddesign.api.method.UserMethod;
import qlsl.androiddesign.api.util.commonutil.DialogUtil;
import qlsl.androiddesign.api.util.commonutil.ImageUtils;
import qlsl.androiddesign.api.util.commonutil.Log;

/**
 * 类描述：个人中心页
 * 需要传入的键：****
 * 传入的值类型：****
 * 传入的值含义：****
 * 是否必传：****
 * 作者：郑朝军 on 2017/6/14 11:29
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/14 11:29
 * 修改备注：
 */
public class MemberCenterActivity extends CommonActivityBase<ACopyService>
{
    private ListView listView;

    @Override
    public int getLayoutId()
    {
        return R.layout.activity_member_center;
    }

    @Override
    public boolean useEventBus()
    {
        return false;
    }

    @Override
    public ACopyService newP()
    {
        return new ACopyService();
    }

    @Override
    protected void initView()
    {
        setTitle("个人中心");
        listView = (ListView) findViewById(R.id.listView);
        ((Button) findViewById(R.id.btn_common)).setText("退出");
    }

    @Override
    protected void initData()
    {
        setListViewData();
    }

    @Override
    protected void initListener()
    {

    }

    @SingleClick
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.list_item:
                doClickListItem(view);
                break;
            case R.id.btn_common:
                doClickExitView();
                break;
            case R.id.photoView:
                ArrayList<String> list = new ArrayList<String>();
                list.add("https://ss0.bdstatic" +
                        ".com/94oJfD_bAAcT8t7mm9GUKT-xh_/timg?image&quality=100&size=b4000_4000" +
                        "&sec=1498203570&di=48646148f07836b737ad204027a03175&src=http://pic.58pic" +
                        ".com/58pic/13/61/00/61a58PICtPr_1024.jpg");
                startActivityToPhotoBrowseActivity(0, null, list);
                break;
        }
    }

    public void showNetWorkSucceedData(String method, Object values)
    {
        if (method.equals("getSysCache"))
        {
            Log.e("zcj", "showNetWorkSucceedData111");
        }
    }

    public <T> void showData(T... t)
    {

    }

    public <T> void showNoData(T... t)
    {

    }

    private void setListViewData()
    {
        Integer[] icons = new Integer[]{R.drawable.ic_myself, R.drawable.ic_myself, R.drawable
                .ic_myself, R.drawable.ic_myself};
        String[] names = new String[]{"个人信息", "消息中心", "关于我们", "设置"};
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        int length = names.length;
        for (int index = 0; index < length; index++)
        {
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("icon", icons[index]);
            map.put("text", names[index]);
            list.add(map);
        }
        MemberCenterAdapter adapter = new MemberCenterAdapter(activity, list);
        listView.setAdapter(adapter);
    }

    private void doClickListItem(View view)
    {
        int position = listView.getPositionForView(view);
        if (position == 0)
        {
            startActivity(MemberInfoActivity.class);
        }
        else if (position == 1)
        {
            showToast("position===" + position);
        }
        else if (position == 2)
        {
            startActivity(AboutActivity.class);
        }
        else if (position == 3)
        {
            startActivity(SettingActivity.class);
        }
    }

    private void doClickExitView()
    {
        DialogUtil.showDialog(this, "注销登录", "确定退出当前登录用户?");
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        setMemberInfo();
    }

    private void setMemberInfo()
    {
        String name = UserMethod.getUser().getNickname();
        String photoUrl = UserMethod.getUser().getPhoto();
        TextView tv_name = (TextView) findViewById(R.id.tv_name);
        ImageView iv_photo = (ImageView) findViewById(R.id.photoView);
        tv_name.setText(name);
        ImageUtils.circle(activity, photoUrl, iv_photo);
    }

    public void onClick(DialogInterface dialog, int which)
    {
        if (which == -1)
        {
            UserMethod.setUser(null);
            UserMethod.setToken(null);
            UserMethod.setRole(null);
            ActivityManager.getInstance().popAllActivity();
            startActivity(MemberLoginActivity.class);
        }
    }

}
