package qlsl.androiddesign.api.util.commonutil;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import qlsl.androiddesign.api.config.ZConfig;

public class ViewOutUtils {

    public static void outputAllView(ViewGroup rootGroup) {
        for (int index = 0; index < rootGroup.getChildCount(); index++) {
            View view = rootGroup.getChildAt(index);
            Log.e(ZConfig.LOG_TAG, "index：" + index + "\nview：" + view + "\ntag： "
                    + view.getTag());
            if (view instanceof ViewGroup) {
                outputAllView((ViewGroup) view);
            }
        }
    }
}
