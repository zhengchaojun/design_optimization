package qlsl.androiddesign.api.exception;

/**
 * 类描述：
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传：
 * 作者：郑朝军 on 2017/7/6 9:21
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/7/6 9:21
 * 修改备注：
 */
public class NetError extends Exception
{
    private Throwable exception;
    private int type = MSG_NOCONNECT_FAILED;

    public static final int MSG_PARSEERROR_FAILED = 0;   //数据解析异常
    public static final int MSG_NOCONNECT_FAILED = 1;   //无连接异常
    public static final int MSG_AUTH_FAILED = 2;   //用户验证异常
    public static final int MSG_NODATA_FAILED = 3;   //无数据返回异常
    public static final int MSG_BUSINESS_FAILED = 4;   //业务异常
    public static final int MSG_OTHER_FAILED = 5;   //其他异常
    public static final int MSG_SERVER_FAILED = 6;   //服务器已关闭，请稍后再试
    public static final int MSG_UNKOW_FAILED = 7;   //不详
    public static final int MSG_ECPITON_FAILED = 8;   //网络异常
    public static final int MSG_SERVER_RESTART = 9;   //服务器已重启


    public NetError(String detailMessage)
    {
        super(detailMessage);
    }

    public NetError(String detailMessage, int type)
    {
        super(detailMessage);
        this.type = type;
    }

    public NetError(Throwable exception, int type)
    {
        this.exception = exception;
        this.type = type;
    }

    public NetError(Throwable exception, String detailMessage, int type)
    {
        super(detailMessage);
        this.exception = exception;
        this.type = type;
    }

    @Override
    public String getMessage()
    {
        if (exception != null) return exception.getMessage();
        return super.getMessage();
    }

    public int getType()
    {
        return type;
    }
}
