package qlsl.androiddesign.api.util.commonutil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 类描述：基本数据类型处理工具类
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传：
 * 作者：郑朝军 on 2017/7/10 9:58
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/7/10 9:58
 * 修改备注：
 */
public class BDTUtils
{
    /////////////////////////////////List//
    // 与数组工具/////////////////////////////////////////////////////////////////////

    /**
     * List数组转String数组
     *
     * @param stringList
     * @return
     */
    public static String[] ListArrayTOStringArray(List<String> stringList)
    {
        return stringList.toArray(new String[stringList.size()]);
    }

    /**
     * List数组转String数组
     *
     * @param stringList
     * @return
     */
    public static Double[] ListArrayTODoubleArray(List<Double> stringList)
    {
        return stringList.toArray(new Double[stringList.size()]);
    }

    /**
     * 一维数组转List数组
     *
     * @param strings
     * @return
     */
    public static <T> List<T> ArrayTOListArray(T[] strings)
    {
        return Arrays.asList(strings);
    }

////////////////////////////////////////以下方法待优化//////////////////////////////////////////////////////////////////////

    /**
     * StringArrayList 转换为LongArrayList
     *
     * @param stringArrayList
     * @return
     */
    public static ArrayList<Long> stringArrayTOLongArray(ArrayList<String> stringArrayList)
    {
        ArrayList<Long> arrayList = new ArrayList<>();
        for (int i = 0; i < stringArrayList.size(); i++)
        {
            arrayList.add(Long.parseLong(stringArrayList.get(i)));
        }
        return arrayList;
    }

    /**
     * String数组转Long数组
     *
     * @param strings
     * @return
     */
    public static Long[] StringArrayTOLongArray(String[] strings)
    {
        Long[] str2 = new Long[strings.length];
        for (int i = 0; i < strings.length; i++)
        {
            str2[i] = Long.valueOf(strings[i]);
        }
        return str2;
    }

    /**
     * Long数组转String数组
     *
     * @param longs
     * @return
     */
    public static String[] LongArrayTOStringArray(Long[] longs)
    {
        String[] str2 = new String[longs.length];
        for (int i = 0; i < longs.length; i++)
        {
            str2[i] = String.valueOf(longs[i]);
        }
        return new String[longs.length];
    }
    /////////////////////////////////结束/////////////////////////////////////////////////////////////////////

    public static <T> String ArrayTOString(T[] longs)
    {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0, count = longs.length; i < count; i++)
        {
            stringBuffer.append(longs[i] + ",");
        }
        return stringBuffer.toString();
    }

    public static String ArrayTOString(double[] longs)
    {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0, count = longs.length; i < count; i++)
        {
            stringBuffer.append(i == count - 1 ? longs[i] : longs[i] + ",");
        }
        return stringBuffer.toString();
    }
}
