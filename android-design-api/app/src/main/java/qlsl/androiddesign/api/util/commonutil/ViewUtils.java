package qlsl.androiddesign.api.util.commonutil;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/**
 * 类描述：视图动态添加工具类
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传：
 * 作者：郑朝军 on 2017/8/8 15:51
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/8/8 15:51
 * 修改备注：
 */
public class ViewUtils
{
    /**
     * 添加布局到rootGroup中
     *
     * @param rootGroup
     * @param layoutID
     * @param activity
     */
    public static void addView(ViewGroup rootGroup, int layoutID, Activity activity)
    {
        ViewGroup conentGroup = (ViewGroup) LayoutInflater.from(activity).inflate(layoutID, rootGroup, false);
        rootGroup.addView(conentGroup);
    }

    /**
     * 添加布局到rootGroup中
     *
     * @param rootGroup
     * @param layoutID
     * @param activity
     * @return 返回内容布局
     */
    public static ViewGroup addViewReturn(ViewGroup rootGroup, int layoutID, Activity activity)
    {
        ViewGroup conentGroup = (ViewGroup) LayoutInflater.from(activity).inflate(layoutID, rootGroup, false);
        rootGroup.addView(conentGroup);
        return conentGroup;
    }

    /**
     * 添加布局到rootGroup中
     *
     * @param rootGroup
     * @param layoutID
     * @param activity
     */
    public static void addViewUnroot(ViewGroup rootGroup, int layoutID, Activity activity)
    {
        ViewGroup conentGroup = (ViewGroup) LayoutInflater.from(activity).inflate(layoutID, null);
        rootGroup.addView(conentGroup);
    }

    /**
     * 添加布局到rootGroup中(填充整个窗口)
     *
     * @param rootGroup
     * @param layoutID
     * @param activity
     */
    public static void addViewMatchMatch(ViewGroup rootGroup, int layoutID, Activity activity)
    {
        ViewGroup conentGroup = (ViewGroup) LayoutInflater.from(activity).inflate(layoutID, rootGroup, false);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT);
        rootGroup.addView(conentGroup, params);
    }

    /**
     * 添加布局到rootGroup中(确保在主线程中执行)
     *
     * @param rootGroup    根视图
     * @param contentGroup 内容视图
     */
    public static void addView(final ViewGroup rootGroup, final ViewGroup contentGroup)
    {
        UIUtils.runOnUIThread(new Runnable()
        {
            @Override
            public void run()
            {
                rootGroup.addView(contentGroup);
            }
        });
    }
}
