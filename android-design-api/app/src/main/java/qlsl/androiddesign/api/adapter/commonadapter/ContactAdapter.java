package qlsl.androiddesign.api.adapter.commonadapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SectionIndexer;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.adapter.baseadapter.listviewadapter.ListViewBaseAdapter;
import qlsl.androiddesign.api.entity.commonentity.Contacts;
import qlsl.androiddesign.api.view.stickylist.StickyListHeadersAdapter;

public class ContactAdapter extends ListViewBaseAdapter<Contacts> implements
        StickyListHeadersAdapter, SectionIndexer
{
    private int[] mSectionIndices;

    private Character[] mSectionLetters;

    private List<Contacts> list;

    public ContactAdapter(Activity activity, List<Contacts> list)
    {
        super(activity, list);
        this.list = list;
        mSectionIndices = getSectionIndices();
        mSectionLetters = getSectionLetters();
    }

    private int[] getSectionIndices()
    {
        ArrayList<Integer> sectionIndices = new ArrayList<Integer>();
        char lastFirstChar = list.get(0).getInitials().charAt(0);
        sectionIndices.add(0);
        for (int i = 1; i < list.size(); i++)
        {
            String i_str = list.get(i).getInitials();
            if (i_str.charAt(0) != lastFirstChar)
            {
                lastFirstChar = i_str.charAt(0);
                sectionIndices.add(i);
            }
        }
        int[] sections = new int[sectionIndices.size()];
        for (int i = 0; i < sectionIndices.size(); i++)
        {
            sections[i] = sectionIndices.get(i);
        }
        return sections;
    }

    private Character[] getSectionLetters()
    {
        Character[] letters = new Character[mSectionIndices.length];
        for (int i = 0; i < mSectionIndices.length; i++)
        {
            letters[i] = list.get(mSectionIndices[i]).getInitials().charAt(0);
        }
        return letters;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        convertView = getItemView(convertView, R.layout.listitem_select_contact, parent);
        TextView tv_text = getView(convertView, R.id.tv_text);
        String text = getItem(position).getContact();
        tv_text.setText(text);
        return convertView;
    }

    public View getHeaderView(int position, View convertView, ViewGroup parent)
    {
        convertView = getItemView(convertView, R.layout.listitem_select_contact_header, parent);
        TextView tv_text = getView(convertView, R.id.tv_text);
        CharSequence headerChar = getItem(position).getInitials().subSequence(0, 1);
        tv_text.setText(headerChar);
        return convertView;
    }

    public long getHeaderId(int position)
    {
        return list.get(position).getInitials().subSequence(0, 1).charAt(0);
    }

    public int getPositionForSection(int section)
    {
        if (mSectionIndices.length == 0)
        {
            return 0;
        }

        if (section >= mSectionIndices.length)
        {
            section = mSectionIndices.length - 1;
        }
        else if (section < 0)
        {
            section = 0;
        }
        return mSectionIndices[section];
    }

    public int getSectionForPosition(int position)
    {
        for (int i = 0; i < mSectionIndices.length; i++)
        {
            if (position < mSectionIndices[i])
            {
                return i - 1;
            }
        }
        return mSectionIndices.length - 1;
    }

    public Object[] getSections()
    {
        return mSectionLetters;
    }
}
