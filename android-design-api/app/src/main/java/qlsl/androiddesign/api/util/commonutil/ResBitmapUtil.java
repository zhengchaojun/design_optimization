package qlsl.androiddesign.api.util.commonutil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.lang.ref.SoftReference;
import java.util.HashMap;

import qlsl.androiddesign.api.activity.R;

/**
 * Created by litian on 2017/1/7.
 * description:用于获取drawable目录下面的资源对应的 图片，并用Bitmap的名称作为键值存储
 */
public class ResBitmapUtil
{
    public static HashMap<String, SoftReference<Bitmap>> getResourseBitmap(Context context)
    {
        HashMap<String, SoftReference<Bitmap>> resBtimaps = new HashMap<>();

        //指定的图片大小
        int decodeSize = 32;

        //将图片压缩为指定大小，避免图片过大
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        Bitmap gasacpfaBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.p1301, options);

        float width = options.outWidth;

        options.inJustDecodeBounds = false;
        //设置压缩比例
        int smapleSize = (int) (width / decodeSize);

        options.inSampleSize = smapleSize;

        gasacpfaBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.p1301, options);
        resBtimaps.put("1301", new SoftReference<Bitmap>(gasacpfaBitmap));

        Bitmap gasacpfbBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.p1302, options);
        resBtimaps.put("1302", new SoftReference<Bitmap>(gasacpfbBitmap));

        Bitmap gasacpfcBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.p1303, options);
        resBtimaps.put("1303", new SoftReference<Bitmap>(gasacpfcBitmap));

        Bitmap gasacpfdBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.p1304, options);
        resBtimaps.put("p1304", new SoftReference<Bitmap>(gasacpfdBitmap));

        Bitmap gasacpfgBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.p1305, options);
        resBtimaps.put("1305", new SoftReference<Bitmap>(gasacpfgBitmap));

        Bitmap gasacpfhBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.p1306, options);
        resBtimaps.put("1306", new SoftReference<Bitmap>(gasacpfhBitmap));

        Bitmap gasacpzaBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.p1307, options);
        resBtimaps.put("1307", new SoftReference<Bitmap>(gasacpzaBitmap));

        Bitmap gasacpzbBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.p1308, options);
        resBtimaps.put("1308", new SoftReference<Bitmap>(gasacpzbBitmap));

        Bitmap gasacpzcBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.p1309, options);
        resBtimaps.put("1309", new SoftReference<Bitmap>(gasacpzcBitmap));

        Bitmap gasacpzdBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.p1310, options);
        resBtimaps.put("1310", new SoftReference<Bitmap>(gasacpzdBitmap));

        Bitmap gasacpzgBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.p1311, options);
        resBtimaps.put("1311", new SoftReference<Bitmap>(gasacpzgBitmap));

        Bitmap gasacpzhBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.p1312, options);
        resBtimaps.put("1312", new SoftReference<Bitmap>(gasacpzhBitmap));

        Bitmap gasauxiliaryaBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.p1313, options);
        resBtimaps.put("1313", new SoftReference<Bitmap>(gasauxiliaryaBitmap));

        Bitmap gasauxiliarybBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.p1314, options);
        resBtimaps.put("1314", new SoftReference<Bitmap>(gasauxiliarybBitmap));

        Bitmap gasauxiliarycBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.p1315, options);
        resBtimaps.put("1315", new SoftReference<Bitmap>(gasauxiliarycBitmap));

        Bitmap gasauxiliarydBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.p1316, options);
        resBtimaps.put("1316", new SoftReference<Bitmap>(gasauxiliarydBitmap));

        Bitmap gasauxiliarygBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.p1317, options);
        resBtimaps.put("1317", new SoftReference<Bitmap>(gasauxiliarygBitmap));

        Bitmap gasauxiliaryhBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.p1318, options);
        resBtimaps.put("1318", new SoftReference<Bitmap>(gasauxiliaryhBitmap));

        Bitmap gasbleederaBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.p1319, options);
        resBtimaps.put("1319", new SoftReference<Bitmap>(gasbleederaBitmap));

        Bitmap gasbleederbBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.p1400, options);
        resBtimaps.put("1400", new SoftReference<Bitmap>(gasbleederbBitmap));

        return resBtimaps;
    }
}
