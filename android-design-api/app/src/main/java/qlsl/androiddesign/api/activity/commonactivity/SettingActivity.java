package qlsl.androiddesign.api.activity.commonactivity;

import android.view.View;
import android.widget.BaseAdapter;

import java.util.HashMap;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.activity.baseactivity.ListViewBaseActivity;
import qlsl.androiddesign.api.adapter.commonadapter.RoundSearchAdapter;
import qlsl.androiddesign.api.adapter.commonadapter.SettingAdapter;
import qlsl.androiddesign.api.http.service.commonservice.ACopyService;
import qlsl.androiddesign.api.util.commonutil.ScreenUtils;
import qlsl.androiddesign.api.util.other.Utils;
import qlsl.androiddesign.api.view.commonview.ListViewNeedAdapterDialog;

/**
 * 类描述：设置页
 * 需要传入的键：****
 * 传入的值类型：****
 * 传入的值含义：****
 * 是否必传：****
 * 作者：郑朝军 on 2017/6/14 11:29
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/14 11:29
 * 修改备注：
 */
public class SettingActivity extends ListViewBaseActivity<HashMap<String, Object>, ACopyService>
{
    private ListViewNeedAdapterDialog needAdapterDialog;

    private Utils util = Utils.getInstance();

    @Override
    protected void initView()
    {
        super.initView();
        setTitle("设置");
    }


    @Override
    protected void initData()
    {
        setListViewData();
        super.initData();
    }

    @Override
    protected void initListener()
    {

    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.list_item:
                doClickListItem(view);
                break;
        }
    }

    public void showNetWorkSucceedData(String method, Object values)
    {

    }

    @Override
    public BaseAdapter getAdapter()
    {
        return new SettingAdapter(activity, list);
    }

    private void setListViewData()
    {
        String username = "150";
        String mobile = "0.0MB";

        String[] names = new String[]{"周边半径搜索设置", "清除缓存"};
        String[] values = new String[]{username, mobile};

        for (int index = 0; index < names.length; index++)
        {
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("text", names[index]);
            map.put("value", values[index]);
            list.add(map);
        }
    }

    private void doClickListItem(View view)
    {
        int position = listView.getPositionForView(view);
        showToast("position==="+position);
        if (position == 0)
        {
            showQuickOption(new RoundSearchAdapter(activity, util.createRoundSearchData()));
        }
        else if (position == 1)
        {
        }
    }

    private void showQuickOption(BaseAdapter adapter)
    {
        needAdapterDialog = util.showQuickOption(activity, adapter, 120,
                ScreenUtils.getScreenWidth(activity), null);
    }

}
