package qlsl.androiddesign.api.aspect;


import com.cx.annotation.aspect.ZCJLog;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

import qlsl.androiddesign.api.util.commonutil.Log;

/**
 * 类描述：根据注解TimeLog自动添加打印方法耗代码
 * 需要传入的键：****
 * 传入的值类型：****
 * 传入的值含义：****
 * 是否必传：****
 * 作者：郑朝军 on 2017/6/14 11:29
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/14 11:29
 * 修改备注：
 */
@Aspect
public class LogAspect
{

    @Pointcut("execution(@com.cx.annotation.aspect.ZCJLog * *(..))")//方法切入点
    public void methodPointcut()
    {
    }


    @Around("methodPointcut()")//在连接点进行方法替换
    public Object aroundJoinPoint(ProceedingJoinPoint joinPoint) throws Throwable
    {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        ZCJLog log = methodSignature.getMethod().getAnnotation(ZCJLog.class);
        String name = log.value();

        switch (name)
        {
            case "onCreateViewLazy":
                Log.i("onCreateViewLazy：fragment：<br/>", methodSignature.getMethod().toString());
                break;
            case "onStartLazy":

                break;
            case "onStopLazy":

                break;
            case "onResumeLazy":
                Log.i("onResumeLazy：fragment：<br/>", methodSignature.getMethod());
                break;
            case "onPauseLazy":
                break;
            case "onDestoryLazy":
                Log.i("onDestoryLazy：fragment：<br/>", methodSignature.getMethod());
                break;
            default:
                break;
        }
        Object result = joinPoint.proceed();//执行原方法
        return result;
    }
}