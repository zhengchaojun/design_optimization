package qlsl.androiddesign.api.fragment.basefragment;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.text.TextUtils;
import android.view.View;

import org.greenrobot.eventbus.EventBus;

import qlsl.androiddesign.api.activity.baseactivity.CommonContract;
import qlsl.androiddesign.api.activity.baseactivity.FunctionActivity;
import qlsl.androiddesign.api.util.commonutil.ToastUtils;

/**
 * 公共业务逻辑
 * 作者：郑朝军 on 2017/3/18 10:12
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
public abstract class ZLazyFragment<P extends CommonContract.IService>
        extends LazyFragment implements CommonContract.CommonView<P>
{
    private P p;

    @Override
    protected void onCreateViewLazy(Bundle savedInstanceState)
    {
        super.onCreateViewLazy(savedInstanceState);
        if (getLayoutId() > 0)
        {
            setContentView(getLayoutId());
            bindUI(getRealRootView());
        }
        if (useEventBus())
        {
            EventBus.getDefault().register(this);
        }
        bindEvent();
        init();
    }

    @Override
    public void bindUI(View rootView)
    {
    }

    @Override
    public boolean useEventBus()
    {
        return false;
    }


    @Override
    public void bindEvent()
    {

    }

    /**
     * 查找和添加公有控件，初始化数据及监听<br/>
     */
    private void init()
    {
        initView();
        initData();
        initListener();
    }

    /**
     * 用于控制标题栏及初始化控件<br/>
     */
    protected abstract void initView();

    /**
     * 用于初始化数据<br/>
     */
    protected abstract void initData();

    /**
     * 用于设置事件监听<br/>
     */
    protected abstract void initListener();

    public void onClick(View view)
    {

    }

    @Override
    protected void onDestoryLazy()
    {
        super.onDestoryLazy();
        if (useEventBus())
        {
            EventBus.getDefault().unregister(this);
        }
        if (getP() != null)
        {
            getP().detachV();
        }

        p = null;
    }

    protected P getP()
    {
        if (p == null)
        {
            p = newP();
            if (p != null)
            {
                p.attachV(this);
            }
        }
        return p;
    }

    //----------------------------方法提供给子类调用--------------------------------------------------------------

    /**
     * 隐藏进度条<br/>
     */
    public void hideProgressBar()
    {
        ((FunctionActivity) activity).hideProgressBar();
    }

    public void showProgressBar()
    {
        ((FunctionActivity) activity).showProgressBar();
    }

    public <V extends View> V findView(@IdRes int id)
    {
        View rootView = getRootView();
        if (rootView != null)
        {
            return (V) rootView.findViewById(id);
        }
        return null;
    }

    /**
     * 显示长Toast
     */
    public void showToast(String text)
    {
        if (!TextUtils.isEmpty(text))
        {
            ToastUtils.showLongToast(text);
        }
    }

    /**
     * 显示短Toast
     */
    public void showShortToast(String text)
    {
        if (!TextUtils.isEmpty(text))
        {
            ToastUtils.showShortToast(text);
        }
    }
    //----------------------------接口方法,网络层,数据库层等其他回调方法-----------------------------------------------------

    /**
     * 显示数据:<br/>
     * 待接收对象：showData函数中的t容器对象<br/>
     */
    @SuppressWarnings("hiding")
    public void showNetWorkSucceedData(String method, Object values)
    {

    }

    /**
     * 提示没有数据:<br/>
     * 待接收对象：showNoData函数中的t容器对象<br/>
     */
    @SuppressWarnings("hiding")
    public void showNetWorkFaildData(String method, Object values)
    {
        ToastUtils.showNoDataToast(values.toString());
    }

    /**
     * 显示数据库数据:<br/>
     * 待接收对象：showData函数中的t容器对象<br/>
     */
    @SuppressWarnings("hiding")
    public void showDbSucceedData(String method, Object values)
    {

    }

    /**
     * 提示数据库没有数据:<br/>
     * 待接收对象：showNoData函数中的t容器对象<br/>
     */
    @SuppressWarnings("hiding")
    public void showFaildDbData(String method, Object values)
    {
        ToastUtils.showNoDataToast(values.toString());
    }

    /**
     * 显示其他数据:<br/>
     * 待接收对象：showData函数中的t容器对象<br/>
     */
    @SuppressWarnings("hiding")
    public void showOtherSucceedData(String method, Object values)
    {

    }

    /**
     * 提示其他没有数据:<br/>
     * 待接收对象：showData函数中的t容器对象<br/>
     */
    @SuppressWarnings("hiding")
    public void showFaildOtherData(String method, Object values)
    {
        ToastUtils.showNoDataToast(values.toString());
    }

    public <T extends Object> void showData(T... t)
    {

    }


    public <T extends Object> void showNoData(T... t)
    {

    }

}
