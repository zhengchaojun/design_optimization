package qlsl.androiddesign.api.util.commonutil;



import android.support.annotation.NonNull;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * 类描述：定时器工具类Rxjava2.0
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传：
 * 作者：郑朝军 on 2017/7/26 16:27
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/7/26 16:27
 * 修改备注：
 */
public class LoopTimeUtil
{
    private static Disposable mDisposable;

    /**
     * 执行一次
     *
     * @param milliseconds
     * @param next
     */
    public static void timer(long milliseconds, final IRxNext next)
    {
        Observable.timer(milliseconds, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Long>()
                {
                    @Override
                    public void onSubscribe(@NonNull Disposable disposable)
                    {
                        mDisposable = disposable;
                    }

                    @Override
                    public void onNext(@NonNull Long number)
                    {
                        if (next != null)
                        {
                            next.doNext(number);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e)
                    {
                        //取消订阅
                        cancel();
                    }

                    @Override
                    public void onComplete()
                    {
                        //取消订阅
                        cancel();
                    }
                });
    }


    /**
     * 每隔milliseconds毫秒后执行next操作
     *
     * @param milliseconds
     * @param next
     */
    public static void interval(long milliseconds, final IRxNext next)
    {
        Observable.interval(milliseconds, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Long>()
                {
                    @Override
                    public void onSubscribe(@NonNull Disposable disposable)
                    {
                        mDisposable = disposable;
                    }

                    @Override
                    public void onNext(@NonNull Long number)
                    {
                        if (next != null)
                        {
                            next.doNext(number);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e)
                    {

                    }

                    @Override
                    public void onComplete()
                    {

                    }
                });
    }


    /**
     * 取消订阅
     */
    public static void cancel()
    {
        if (mDisposable != null && !mDisposable.isDisposed())
        {
            mDisposable.dispose();
            Log.e("zcj","====定时器取消======");
        }
    }

    public interface IRxNext
    {
        void doNext(long number);
    }
}
