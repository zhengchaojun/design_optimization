package qlsl.androiddesign.api.aspect;

import android.view.View;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

import qlsl.androiddesign.api.application.SoftwareApplication;
import qlsl.androiddesign.api.manager.ActivityManager;
import qlsl.androiddesign.api.util.commonutil.SPUtils;
import qlsl.androiddesign.api.util.commonutil.ToastUtils;

/**
 * 类描述：通过CheckLogin注解检查用户是否登陆注解
 * 需要传入的键：****
 * 传入的值类型：****
 * 传入的值含义：****
 * 是否必传：****
 * 作者：郑朝军 on 2017/6/14 11:29
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/14 11:29
 * 修改备注：
 */
@Aspect
public class CheckLoginAspect
{
    @Pointcut("execution(@com.cx.annotation.aspect.CheckLogin * *(..))")//方法切入点
    public void methodPointcut()
    {
    }

    @Around("methodPointcut()")//在连接点进行方法替换
    public Object aroundJoinPoint(ProceedingJoinPoint joinPoint) throws Throwable
    {
        String token = (String) SPUtils.get(SoftwareApplication.getInstance(), SPUtils
                .MEMBER_TOKEN, "-1");
        if (token.equals("-1"))
        {
            ToastUtils.showSnackbar(ActivityManager.getInstance().currentActivity(), "请先登录!",
                    true, "登录", new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View view)
                        {
//                    Router.newIntent(ActivityManager.getInstance().currentActivity()).to
// (TestActivity.class).anim(R.anim.push_bottom_in, R.anim.push_bottom_out).launch();
                        }
                    });
            return null;
        }
        Object result = joinPoint.proceed();//执行原方法
        return result;
    }
}

