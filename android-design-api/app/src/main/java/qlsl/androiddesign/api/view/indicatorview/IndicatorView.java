package qlsl.androiddesign.api.view.indicatorview;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import java.util.List;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.util.commonutil.ImageUtils;


/**
 * Network ImageIndicatorView, by urls
 *
 * @author steven-pan
 */
public class IndicatorView extends ImageIndicatorView
{

    public IndicatorView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public IndicatorView(Context context)
    {
        super(context);
    }

    /**
     * 无图片时占位
     */
    public void setupLayout(final List<String> urlList)
    {
        clear();
        int size = urlList.size();
        if (size > 0)
        {
            for (int index = 0; index < size; index++)
            {
                final ImageView pageItem = new ImageView(getContext());
                pageItem.setScaleType(ScaleType.FIT_XY);
                ImageUtils.rect(getContext(), urlList.get(index), pageItem, R.drawable.iv_default);
                addViewItem(pageItem);
            }
        } else
        {
            final ImageView pageItem = new ImageView(getContext());
            pageItem.setScaleType(ScaleType.FIT_XY);
            pageItem.setImageResource(R.drawable.iv_error);
            addViewItem(pageItem);
        }
    }

    /**
     * 无图片时占位
     */
    public void setupLayoutOri(final List<String> urlList)
    {
        clear();
        int size = urlList.size();
        if (size > 0)
        {
            for (int index = 0; index < size; index++)
            {
                final ImageView pageItem = new ImageView(getContext());
                pageItem.setScaleType(ScaleType.FIT_CENTER);
                ImageUtils.rectOri(getContext(), urlList.get(index), pageItem, R.drawable.iv_default);

                addViewItem(pageItem);
            }
        } else
        {
            final ImageView pageItem = new ImageView(getContext());
            pageItem.setScaleType(ScaleType.FIT_XY);
            pageItem.setImageResource(R.drawable.iv_error);
            addViewItem(pageItem);
        }
    }

    /**
     * 无图片时占位
     */
    public void setupLayout(final String url)
    {
        clear();
        if (!TextUtils.isEmpty(url))
        {
            final ImageView pageItem = new ImageView(getContext());
            pageItem.setScaleType(ScaleType.FIT_XY);
            ImageUtils.rect(getContext(), url, pageItem, R.drawable.iv_default);
            addViewItem(pageItem);
        } else
        {
            final ImageView pageItem = new ImageView(getContext());
            pageItem.setScaleType(ScaleType.FIT_XY);
            pageItem.setImageResource(R.drawable.iv_error);
            addViewItem(pageItem);
        }
    }

    /**
     * 无图片时不占位
     */
    public void setupLayoutUnPlaceHolder(final List<String> urlList)
    {
        clear();
        int size = urlList.size();
        if (size > 0)
        {
            for (int index = 0; index < size; index++)
            {
                final ImageView pageItem = new ImageView(getContext());
                pageItem.setAdjustViewBounds(true);
                ImageUtils.rect(getContext(), urlList.get(index), pageItem);
                addViewItem(pageItem);
            }
        }
    }

    /**
     * 建立size个默认图片<br/>
     */
    public void setupLayoutByDefaultResource(final int size)
    {
        clear();
        for (int index = 0; index < size; index++)
        {
            final ImageView pageItem = new ImageView(getContext());
            pageItem.setScaleType(ScaleType.FIT_XY);
            ImageUtils.rect(getContext(), "", pageItem);
            addViewItem(pageItem);
        }

    }

}
