package qlsl.androiddesign.api.activity.baseactivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.View;


import org.greenrobot.eventbus.EventBus;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.activity.commonactivity.MainActivity;
import qlsl.androiddesign.api.manager.ActivityManager;
import qlsl.androiddesign.api.util.commonutil.ExtraUtils;
import qlsl.androiddesign.api.util.commonutil.Log;
import qlsl.androiddesign.api.view.commonview.SwipeBackLayout;


/**
 * 公共业务逻辑
 * 作者：郑朝军 on 2017/3/18 10:12
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
public abstract class CommonActivityBase<P extends CommonContract.IService> extends
        FunctionActivity<P>
{
    private P p;

    public Activity activity;

    private SwipeBackLayout mSwipeBackLayout;

    /**
     * 活动初始化函数，可以传入初始化参数
     *
     * @param context
     * @param destClass
     * @param extraUtils 需要传递的参数，封装在Map里面
     */
    public static void actionStart(Context context, Class<? extends Activity> destClass,
                                   ExtraUtils extraUtils)
    {
        Intent intent = new Intent(context, destClass);
        intent.putExtra("data", extraUtils);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        outputCreateActivityInfo();
        super.onCreate(savedInstanceState);
        pushActivityToStack();
        setOriention();
        activity = this;
        int layoutResID = getLayoutId();
        if (layoutResID > 0)
        {
            bindUI(null);
            setCommonContentView(layoutResID);
            bindEvent();
        }
    }

    private void outputCreateActivityInfo()
    {
        Log.i("onCreate：activity：<br/>" + getLocalClassName());
    }

    private void pushActivityToStack()
    {
        ActivityManager.getInstance().pushActivity(this);
    }

    private void setOriention()
    {
        boolean screenOrientation = setScreenOrientation();
        setRequestedOrientation(screenOrientation ? ActivityInfo.SCREEN_ORIENTATION_PORTRAIT : ActivityInfo
                .SCREEN_ORIENTATION_LANDSCAPE);
    }

    /**
     * true为竖屏
     * false设置横屏
     *
     * @return
     */
    public boolean setScreenOrientation()
    {
        return true;
    }

    @Override
    public void bindUI(View rootView)
    {
    }

    public void setCommonContentView(int layoutResID)
    {
        boolean isNotSwipeBack = layoutResID == R.layout.activity_main || layoutResID == R.layout.activity_launcher;
        if (isNotSwipeBack)
        {
            setContentView(layoutResID);
            init();
        }
        else
        {
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mSwipeBackLayout = new SwipeBackLayout(this);
            mSwipeBackLayout.attachToActivity(this, SwipeBackLayout.EDGE_LEFT);
            // 触摸边缘变为屏幕宽度的1/20
            mSwipeBackLayout.setEdgeSize((int) (getResources().getDisplayMetrics().widthPixels * 0.05));
            setFunctionContentView(layoutResID);
        }
    }

    @Override
    public void bindEvent()
    {

    }

    public void onRestart()
    {
        super.onRestart();

    }

    public void onStart()
    {
        super.onStart();
        if (useEventBus())
        {
            EventBus.getDefault().register(this);
        }
    }

    protected void onResume()
    {
        super.onResume();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
    }

    public void onStop()
    {
        super.onStop();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        outputDestroyActivityInfo();
        if (useEventBus())
        {
            EventBus.getDefault().unregister(this);
        }
        if (getP() != null)
        {
            getP().detachV();
        }
        p = null;
    }

    protected P getP()
    {
        if (p == null)
        {
            p = newP();
            if (p != null)
            {
                p.attachV(this);
            }
        }
        return p;
    }

    private void outputDestroyActivityInfo()
    {
        Log.i("onDestroy：activity：<br/>" + getLocalClassName());
    }

    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

    }

    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        switch (keyCode)
        {
            case KeyEvent.KEYCODE_BACK:
                if (!getLocalClassName().equals(MainActivity.class.getName()))
                {
                    //主页Activity
                    ActivityManager.getInstance().popActivity();
                    return false;
                }
                break;
        }
        return super.onKeyDown(keyCode, event);
    }


    public SwipeBackLayout getmSwipeBackLayout()
    {
        return mSwipeBackLayout;
    }
}
