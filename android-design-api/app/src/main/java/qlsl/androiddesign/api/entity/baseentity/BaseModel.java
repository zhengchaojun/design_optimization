package qlsl.androiddesign.api.entity.baseentity;


import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.HashMap;

import qlsl.androiddesign.api.entity.commonentity.Pager;
import qlsl.androiddesign.api.util.commonutil.ReflectUtil;
import qlsl.androiddesign.api.util.commonutil.ValueUtil;

/**
 * 类描述：所有模型基类
 * 需要传入的键：****
 * 传入的值类型：****
 * 传入的值含义：****
 * 是否必传：****
 * 作者：郑朝军 on 2017/6/14 11:29
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/14 11:29
 * 修改备注：
 */
public class BaseModel implements Serializable
{
    protected boolean error;

    protected String msg;
    protected String ret;
    protected String errorCode;

    protected String code;// 验证码
    protected Integer total;// 总条数
    protected Pager pager;
    protected HashMap<String, Object> m_HashMap;

    public String getRet()
    {
        return ret;
    }

    public void setRet(String ret)
    {
        this.ret = ret;
    }

    public HashMap<String, Object> getHashMap()
    {
        return m_HashMap;
    }

    public void setHashMap(HashMap<String, Object> hashMap)
    {
        m_HashMap = hashMap;
    }

    public Pager getPager()
    {
        return pager;
    }

    public void setPager(Pager pager)
    {
        this.pager = pager;
    }


    public String getMsg()
    {
        return msg;
    }

    public void setMsg(String msg)
    {
        this.msg = msg;
    }

    public String getErrorCode()
    {
        return errorCode;
    }

    public void setErrorCode(String errorCode)
    {
        this.errorCode = errorCode;
    }

    public Integer getTotal()
    {
        return total;
    }

    public void setTotal(Integer total)
    {
        this.total = total;
    }

    public boolean isError()
    {
        return error;
    }

    public void setError(boolean error)
    {
        this.error = error;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    @Override
    public String toString()
    {
        return "error===" + error + "msg===" + msg + "errorCode===" + errorCode + "code" + code +
                "total===" + total;
    }


    /**
     * 验证每一个数据是否为空
     *
     * @return
     */
    public boolean isEmpty()
    {
        boolean flag = false;
        try
        {
            for (Field f : getClass().getDeclaredFields())
            {
                f.setAccessible(true);
                String fieldName = f.getName();
                if (fieldName.equals("ret") || fieldName.equals("msg") || fieldName.equals("errcode") || fieldName
                        .equals("error") || fieldName.equals("errorCode") || fieldName.equals("code") || fieldName
                        .equals("total") || fieldName.equals("pager") || fieldName.equals("m_HashMap"))
                {
                    continue;
                }
                if (f.get(this) == null)
                {
                    flag = true;
                    return flag;
                }
            }
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();
        }
        return flag;
    }

    /**
     * 验证数据是否为空
     *
     * @param key 你要验证的所有参数
     * @return
     */
    public boolean isEmpty(final String[] key)
    {
        boolean flag = false;
        try
        {
            for (int i = 0, count = key.length; i < count; i++)
            {
                if (ValueUtil.isEmpty(ReflectUtil.getValueByFieldName(this, key[i])))
                {
                    return true;
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return flag;
    }
}
