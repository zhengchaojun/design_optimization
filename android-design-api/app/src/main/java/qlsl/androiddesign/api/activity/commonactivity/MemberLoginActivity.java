package qlsl.androiddesign.api.activity.commonactivity;

import android.Manifest;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.cx.annotation.aspect.Permission;
import com.cx.annotation.aspect.SingleClick;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.activity.baseactivity.CommonActivityBase;
import qlsl.androiddesign.api.http.service.commonservice.MemberService;
import qlsl.androiddesign.api.util.commonutil.Log;
import qlsl.androiddesign.api.view.commonview.ClearEditText;

/**
 * 功能：登录
 * 作者：郑朝军 on 2017/4/7 23:23
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */

public class MemberLoginActivity extends CommonActivityBase<MemberService>
{
    private ClearEditText m_etAccount;

    private ClearEditText m_etPassword;
    
    private RecyclerView m_recyclerView;

    @Override
    public int getLayoutId()
    {
        return R.layout.activity_member_login_new;
    }

    @Override
    public boolean useEventBus()
    {
        return false;
    }

    @Override
    public MemberService newP()
    {
        return new MemberService();
    }

    @Override
    protected void initView()
    {
        setTitle("登录");
        setRightButtonText("注册");

        m_etAccount = findView(R.id.et_account);
        m_etPassword = findView(R.id.et_password);
        m_recyclerView = findView(R.id.gridView);

        showRightButton();
        hideBackButton();
        if (Log.isDebug)
        {
            m_etAccount.setText("admin");
            m_etPassword.setText("admin");
        }
    }


    @Override
    protected void initData()
    {
    }

    @Override
    protected void initListener()
    {
    }

    @Override
    public void showNetWorkSucceedData(String method, Object values)
    {
        if (method.equals("loginByEmail"))
        {
            getP().getSysCache();
        }
        else if (method.equals("getSysCache"))
        {
            getP().getUserInfo();
        }
        else if (method.equals("getUserInfo"))
        {
            startToHomePage();
        }
    }

    @SingleClick
    @Permission(value = {
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
    })
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.btn_login:
                doClickLoginButton();
                break;
            case R.id.tv_forget_pwd:
                doClickForgetPwdView();
                break;
        }
    }

    private void startToHomePage()
    {
        startActivity(MainActivity.class);
    }

    private void doClickLoginButton()
    {
        String account = m_etAccount.getText().toString();
        String password = m_etPassword.getText().toString();
        if (TextUtils.isEmpty(account))
        {
            showToast("请输入用户名");
            return;
        }
        if (TextUtils.isEmpty(password))
        {
            showToast("请输入密码");
            return;
        }
        if (password.length() < 4)
        {
            showToast("密码不得小于6位");
            return;
        }

        getP().loginByEmail(account, password);
    }

    private void doClickForgetPwdView()
    {
        getP().initTB();
//        startActivity(MemberForgetPasswordActivity.class);
    }

    protected void doClickRightButton(View view)
    {
        startActivity(MemberRegistActivity.class);
    }
}
