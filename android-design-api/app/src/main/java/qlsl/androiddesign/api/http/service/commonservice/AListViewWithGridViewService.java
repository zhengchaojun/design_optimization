package qlsl.androiddesign.api.http.service.commonservice;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cx.annotation.aspect.CheckNetWork;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import qlsl.androiddesign.api.activity.commonactivity.CopyActivity;
import qlsl.androiddesign.api.http.service.baseservice.ServiceBase;
import qlsl.androiddesign.api.http.xutils.HttpProtocol;
import qlsl.androiddesign.api.util.commonutil.Log;

/**
 * 类描述：****页
 * 需要传入的键：****
 * 传入的值类型：****
 * 传入的值含义：****
 * 是否必传：****
 * 作者：郑朝军 on 2017/6/14 11:29
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/14 11:29
 * 修改备注：
 */
public class AListViewWithGridViewService extends ServiceBase<CopyActivity>
{
    private String className = getClassName(AListViewWithGridViewService.class);

    public void test()
    {
        Log.e("zcj", "1111111111111111111test1111111");
    }

    /**
     * 用户登录
     */
    @CheckNetWork
    public void loginByEmail()
    {
        final String method = "loginByEmail";
        getV().showProgressBar();
        Flowable.create(new FlowableOnSubscribe<Object>()
        {
            @Override
            public void subscribe(FlowableEmitter<Object> e) throws Exception
            {
                HttpProtocol protocol = new HttpProtocol();
                try
                {
                    JSONObject jo = null;

                    if (isDataInvalid(jo, e))
                    {
                        return;
                    }

                    JSONObject jsonObject = (JSONObject) JSON.parseArray(jo.getString("row")).get(0);

                    e.onNext(jsonObject);
                    e.onComplete();
                    outputMessageInfo(protocol, jo, className, method);
                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                    outputException(e, exception, className);
                }
            }
        }, BackpressureStrategy.BUFFER)
                .compose(getScheduler())
                .compose(getV().bindToLifecycle())
                .subscribe(createSubscriber(method));
    }
}
