package qlsl.androiddesign.api.activity.commonactivity;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import java.io.File;
import java.util.List;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.activity.baseactivity.CommonActivityBase;
import qlsl.androiddesign.api.adapter.baseadapter.BasePagerAdapter;
import qlsl.androiddesign.api.adapter.commonadapter.PhotoBrowseAdapter;
import qlsl.androiddesign.api.http.service.commonservice.ACopyService;

/**
 * 类描述：相片浏览页
 * 需要传入的键：name，position,list
 * 传入的值类型：String,int，List < String >或List< File >
 * 传入的值含义：名称，位置，网络图片表或本地图片表
 * 是否必传：否，否，是
 * 作者：郑朝军 on 2017/6/14 11:29
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/14 11:29
 * 修改备注：
 */
public class PhotoBrowseActivity extends CommonActivityBase<ACopyService>
{
    private ViewPager viewPager;

    private TextView tv_num;

    private List<?> list;

    @Override
    public int getLayoutId()
    {
        return R.layout.activity_photo_browse;
    }

    @Override
    public boolean useEventBus()
    {
        return false;
    }

    @Override
    public ACopyService newP()
    {
        return new ACopyService();
    }

    @Override
    protected void initView()
    {
        Intent intent = getIntent();
        String title = intent.getStringExtra("name");
        int position = intent.getIntExtra("position", 0);
        list = (List<Object>) intent.getSerializableExtra("list");

        hideTitleBar();
        TextView tv_title = (TextView) findViewById(R.id.photo_title_view);
        tv_title.setText(title == null ? "相片浏览" : title);

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tv_num = (TextView) findViewById(R.id.tv_num);
        setViewPagerData(position);
    }


    @Override
    protected void initData()
    {
    }

    @Override
    protected void initListener()
    {

    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.photo_back_view:
                activity.finish();
                break;
            case R.id.photo_right_view:
                doClickRightButton(view);
                break;
        }
    }

    public void showNetWorkSucceedData(String method, Object values)
    {

    }

    public <T> void showData(T... t)
    {

    }

    public <T> void showNoData(T... t)
    {

    }

    private void setViewPagerData(int position)
    {
        BasePagerAdapter<?> adapter = null;
        if (list.size() != 0)
        {
            adapter = new PhotoBrowseAdapter(this, (List<String>) list);
        }
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(position);
        int size = list.size();
        if (size != 0)
        {
            tv_num.setText((position + 1) + "/" + list.size());
        }
        else
        {
            tv_num.setText("0/0");
        }
    }


}
