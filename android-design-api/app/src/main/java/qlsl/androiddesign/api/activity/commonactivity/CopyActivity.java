package qlsl.androiddesign.api.activity.commonactivity;

import android.view.View;

import com.cx.annotation.aspect.SingleClick;

import qlsl.androiddesign.api.activity.R;
import qlsl.androiddesign.api.activity.baseactivity.CommonActivityBase;
import qlsl.androiddesign.api.http.service.commonservice.ACopyService;

/**
 * 类描述：****页
 * 需要传入的键：****
 * 传入的值类型：****
 * 传入的值含义：****
 * 是否必传：****
 * 作者：郑朝军 on 2017/6/14 11:29
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/14 11:29
 * 修改备注：
 */
public class CopyActivity extends CommonActivityBase<ACopyService>
{
    @Override
    public int getLayoutId()
    {
        return R.layout.activity_copy;
    }

    @Override
    public boolean useEventBus()
    {
        return false;
    }

    @Override
    public ACopyService newP()
    {
        return new ACopyService();
    }

    @Override
    protected void initView()
    {
    }

    @Override
    protected void initData()
    {
    }

    @Override
    protected void initListener()
    {

    }

    @SingleClick
    public void onClick(View view)
    {

    }

    public void showNetWorkSucceedData(String method, Object values)
    {

    }

    public <T> void showData(T... t)
    {

    }

    public <T> void showNoData(T... t)
    {

    }

}
