package qlsl.androiddesign.api.util.commonutil;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 类描述：数组处理工具类
 * 作者：郑朝军 on 2017/7/10 9:58
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/7/10 9:58
 * 修改备注：
 */
public class ArrayUtils
{
    /**
     * List数组转String数组
     *
     * @param stringList
     * @return
     */
    public static String[] ListArrayTOStringArray(List<String> stringList)
    {
        return stringList.toArray(new String[stringList.size()]);
    }

    /**
     * List数组转String数组
     *
     * @param stringList
     * @return
     */
    public static Double[] ListArrayTODoubleArray(List<Double> stringList)
    {
        return stringList.toArray(new Double[stringList.size()]);
    }


    /**
     * 一维数组转List数组
     *
     * @param strings
     * @return
     */
    public static <T> List<T> ArrayTOListArray(T[] strings)
    {
        return Arrays.asList(strings);
    }

    /**
     * 移除集合里面所有null元素
     *
     * @param list
     * @param <T>
     * @return
     */
    public static <T> void removeNullElements(List<T> list)
    {
        list.removeAll(Collections.singleton(null));
        list.removeAll(Collections.singleton(""));
    }

    /**
     * 传递一个列表给我，返回一个最大的数字
     *
     * @param list 一个列表
     * @param <T>  最终要得到的数字
     * @return
     */
    public static <T extends Object & Comparable<? super T>> T queryMAXNumber(List<? extends T> list)
    {
        return Collections.max(list);
    }

    /**
     * 传递一个列表给我，返回一个最小的数字
     *
     * @param list 一个列表
     * @param <T>  最终要得到的数字
     * @return
     */
    public static <T extends Object & Comparable<? super T>> T queryMINNumber(List<? extends T> list)
    {
        return Collections.min(list);
    }
////////////////////////////////////////以下方法待优化//////////////////////////////////////////////////////////////////////

    /**
     * StringArrayList 转换为LongArrayList
     *
     * @param stringArrayList
     * @return
     */
    public static ArrayList<Long> stringArrayTOLongArray(ArrayList<String> stringArrayList)
    {
        ArrayList<Long> arrayList = new ArrayList<>();
        for (int i = 0; i < stringArrayList.size(); i++)
        {
            arrayList.add(Long.parseLong(stringArrayList.get(i)));
        }
        return arrayList;
    }

    /**
     * String数组转Long数组
     *
     * @param strings
     * @return
     */
    public static Long[] StringArrayTOLongArray(String[] strings)
    {
        Long[] str2 = new Long[strings.length];
        for (int i = 0; i < strings.length; i++)
        {
            str2[i] = Long.valueOf(strings[i]);
        }
        return str2;
    }

    /**
     * Long数组转String数组
     *
     * @param longs
     * @return
     */
    public static String[] LongArrayTOStringArray(Long[] longs)
    {
        String[] str2 = new String[longs.length];
        for (int i = 0; i < longs.length; i++)
        {
            str2[i] = String.valueOf(longs[i]);
        }
        return new String[longs.length];
    }


    /**
     * 得到一个平均值
     *
     * @param list 一组数据
     * @return 一个平均值
     */
    public static Double getAverage(@NonNull List<Double> list)
    {
        Double sum = 0d;
        int size = list.size();
        for (int i = 0; i < size; i++)
        {
            Double item = list.get(i);
            sum += item;
        }
        return sum / size;
    }


    /**
     * List数组转double数组
     *
     * @param values
     * @return
     */
    public static double[] listTODoubleArray(List<Double> values)
    {
        int length = values.size();
        double[] result = new double[length];
        for (int i = 0; i < length; i++)
        {
            result[i] = values.get(i);
        }
        return result;
    }

    /**
     * double数组转List数组
     *
     * @param values
     * @return
     */
    public static List<Double> doubleToListArray(double[] values)
    {
        List<Double> list = new ArrayList<>();
        int length = values.length;
        for (int i = 0; i < length; i++)
        {
            list.add(values[i]);
        }
        return list;
    }
}
