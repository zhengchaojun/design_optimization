package qlsl.androiddesign.api.activity.baseactivity;

import android.support.annotation.IdRes;
import android.support.annotation.StringRes;
import android.view.View;

/**
 * 公共的Service  Model   View
 * 作者：郑朝军 on 2017/3/20 16:28
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
public interface CommonContract
{
    interface BaseModel
    {
    }

    // 提供给子层调用
    interface IService<V>
    {
        void attachV(V view);

        void detachV();
    }

    interface IView<P>
    {
        void bindUI(View rootView);

        void bindEvent();

        int getLayoutId();

        boolean useEventBus();

        P newP();
    }

    // 提供给P层调用,FunctionActivity,BaseService,ZLazyFragment
    interface CommonView<P> extends CommonContract.IView<P>
    {
        void showDbSucceedData(String method, Object values);

        void showFaildDbData(String method, Object values);

        void showNetWorkSucceedData(String method, Object values);

        void showNetWorkFaildData(String method, Object values);

        void showOtherSucceedData(String method, Object values);

        void showFaildOtherData(String method, Object values);

        void hideProgressBar();

        void showProgressBar();

        <V extends View> V findView(@IdRes int id);

        String getString(@StringRes int resId);
    }
}
