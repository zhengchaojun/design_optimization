package qlsl.androiddesign.api.manager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;

import qlsl.androiddesign.api.activity.baseactivity.FunctionActivity;


@SuppressLint("NewApi")
public class ExitManager
{

    public static long currentBackPressedTime = 0;

    private static final int BACK_PRESSED_INTERVAL = 2000;

    public static void onBackPressed(Context context)
    {
        FunctionActivity activity = (FunctionActivity) context;
        if (System.currentTimeMillis() - currentBackPressedTime > BACK_PRESSED_INTERVAL)
        {
            currentBackPressedTime = System.currentTimeMillis();
            activity.showToast("再按一次退出软件");
        }
        else
        {
            Exit();
        }
    }

    private static void showExitToast(FunctionActivity activity, int minValue, int maxValue,
                                      String[] texts)
    {
        Resources resources = activity.getResources();
        int index = BaseMethod.getRandomValue(minValue, maxValue);
        String resName = "iv_exit_icon_" + index;
        int resId = resources.getIdentifier(activity.getPackageName() + ":drawable/" + resName,
                null, null);
        int textIndex = index % texts.length;

//        activity.showExitToast(texts[textIndex], resId);
    }

    /**
     * 退出<br/>
     * 主线程中调用<br/>
     */
    public static void Exit()
    {
        clearStaticData();
        ActivityManager.getInstance().popAllActivity();
    }

    /**
     * 单例设计类必须清除静态数据<br/>
     */
    public static void clearStaticData()
    {
//        UserMethod.setUser(null);
//        Constant.TOKEN = null;
//        Constant.APPID = null;
//        Constant.APPCODE = null;

//        SoftwareApplication.getInstance().setiSSCount(0);
//
//        SoftwareApplication.getInstance().setbEntityFinished(false);
//        SoftwareApplication.getInstance().setbFldFinished(false);
//        SoftwareApplication.getInstance().setbFldValFinished(false);
//        SoftwareApplication.getInstance().setbLayerFinished(false);
//        SoftwareApplication.getInstance().setbMaparaFinished(false);
//        SoftwareApplication.getInstance().setbSysCacheFinished(false);
//        SoftwareApplication.getInstance().setbSyscfgFinished(false);
//        SoftwareApplication.getInstance().setbUserFinished(false);
//        SoftwareApplication.getInstance().setbViewPortFinished(false);
//
//        SoftwareApplication.getInstance().setBundle(null);
//
//        SoftwareApplication.getInstance().setEntityCfg(null);
//        SoftwareApplication.getInstance().setFldCfg(null);
//        SoftwareApplication.getInstance().setFldValCfg(null);
//        SoftwareApplication.getInstance().setLayerCfg(null);
//        SoftwareApplication.getInstance().setMaparaCfg(null);
//        SoftwareApplication.getInstance().setViewPortCfg(null);
//        SoftwareApplication.getInstance().setUserCfg(null);

    }

    /**
     * 延迟退出<br/>
     * 主线程中调用<br/>
     */
    public static void ExitDelayed()
    {
        new Handler().postDelayed(new Runnable()
        {
            public void run()
            {
                Exit();
            }
        }, 3000);
    }

}
