package com.qlsl.androiddesign.api.wxapi;

import qlsl.androiddesign.api.activity.baseactivity.CommonActivityBase;

public class WXPayEntryActivity extends CommonActivityBase
{
    @Override
    public int getLayoutId()
    {
        return 0;
    }

    @Override
    public boolean useEventBus()
    {
        return false;
    }

    @Override
    public Object newP()
    {
        return null;
    }

    @Override
    protected void initView()
    {

    }

    @Override
    protected void initData()
    {

    }

    @Override
    protected void initListener()
    {

    }
}