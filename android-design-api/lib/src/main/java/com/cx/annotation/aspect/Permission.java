package com.cx.annotation.aspect;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 类描述：
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传：
 * 作者：郑朝军 on 2017/7/15 17:23
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/7/15 17:23
 * 修改备注：
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Permission
{
    String[] value();
}
