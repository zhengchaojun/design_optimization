package com.cx.annotation.test;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 类描述：
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传：
 * 作者：郑朝军 on 2017/7/26 17:27
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/7/26 17:27
 * 修改备注：
 */
public class Test
{
    public static String ArrayTOString(double[] longs)
    {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0, count = longs.length; i < count; i++)
        {
            stringBuffer.append(i == count - 1 ? longs[i] : longs[i] + ",");
        }
        return stringBuffer.toString();
    }

    public static String getNowYMD()
    {
        SimpleDateFormat mDateFormat = new SimpleDateFormat(
                "yyyy-MM-dd");
        String date = mDateFormat.format(new Date());
        return date;
    }

    public static void main1(String[] args)
    {
//        double[] wgslocation = new double[2];
//        wgslocation[0] = 1.2;
//        wgslocation[1] = 1.5;
////        System.out.println(ArrayTOString(wgslocation));
//        String data = getNowYMD();
//        System.out.println(data);
//        System.out.println(data.replace("-",""));


        System.out.println(getSubString(0, 1, "郑朝军的会计法律的解放路的减肥快乐的快乐的咖啡机的"));
    }

    /***
     * 截取字符串
     *
     * @param start 从那里开始，0算起
     * @param num   截取多少个
     * @param str   截取的字符串
     * @return
     */
    public static String getSubString(int start, int num, String str)
    {
        if (str == null)
        {
            return "";
        }
        int leng = str.length();
        if (start < 0)
        {
            start = 0;
        }
        if (start > leng)
        {
            start = leng;
        }
        if (num < 0)
        {
            num = 1;
        }
        int end = start + num;
        if (end > leng)
        {
            end = leng;
        }
        return str.substring(start, end);
    }

    public static String LongArrayTOStringArray(double[] longs)
    {
        StringBuffer str2 = new StringBuffer();
        for (int i = 0, count = longs.length; i < count; i++)
        {
            str2.append((i == count - 1) ? longs[i] : longs[i] + ",");
        }
        return str2.toString();
    }

    public static void main2(String[] args)
    {
        double[] aa = new double[4];
        aa[0] = 1.1;
        aa[1] = 1.3;
        aa[2] = 1.4;
        aa[3] = 1.5;
        System.out.println(LongArrayTOStringArray(aa));
    }

    public static void mainOld(String[] args)
    {

//        HashSet<Test02> set = new HashSet<>();
//        for (int i = 0; i < 2; i++)
//        {
//            Test02 aa = new Test02();
//            aa.setAge("17");
//            aa.setName("郑朝军");
//            set.add(aa);
//            set.add(aa);
//            set.add(aa);
//            set.add(aa);
//            set.add(aa);
//            set.add(aa);
//        }
//        Iterator iterator = set.iterator();
//        while (iterator.hasNext())
//        {
//            Test02 test02 = (Test02) iterator.next();
//            System.out.println(test02.getAge());
//            System.out.println(test02.getName());
//        }

    }

    public static void mainOld2(String[] args)
    {

        HashSet<String> set = new HashSet<>();
        for (int i = 0; i < 200; i++)
        {
            set.add("1121");
            set.add("1121");
            set.add("1121");
            set.add("1121");
            set.add("1121");
            set.add("4545");
        }
        Iterator iterator = set.iterator();
        while (iterator.hasNext())
        {
            String test02 = (String) iterator.next();
            System.out.println(test02);
        }

    }

    public static void mainold2(String[] args)
    {
        String str = "管理员@:办理@:派单3434343343434343434";
        String pattern = "@:";

        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(str);
        System.out.println(m.matches());
    }

    public static void mainOld3(String[] args)
    {
        String str = "xqsw10200030";
        System.out.println(str.replaceAll("10200030", ""));

//        String str = "x{}'qsw':;102+=|{}',/00':;030";
//        // 只允许字母和数字
//        // String   regEx  =  "[^a-zA-Z0-9]";
//        // 清除掉所有特殊字符
//        String regEx="[`~!@#$%^&*()+=|{}':;',//[//].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
//        Pattern   p   =   Pattern.compile(regEx);
//        Matcher   m   =   p.matcher(str);
//        System.out.println(m.replaceAll("").trim());
    }

    public static void mainOld4(String[] args)
    {
        // get two double numbers
        double x = 60984.1;
        double y = 1000000;

        // print the larger number between x and y
//        System.out.println("Math.max(" + x + "," + y + ")=" + Math.max(x, y));

        Integer[] aa = {};


//        System.out.println(aa.length);
        int pos = -1;
        if (pos >= 100)
        {
            System.out.println("等于-1");
        }
        else
        {
            System.out.println("不等于-1");
        }
    }

    public static void mainOlD3(String[] args)
    {

        Date currentTime = new Date();// 当前时间
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String nowtime = formatter.format(currentTime);

        Calendar now = Calendar.getInstance();
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        String t1 = "2016-03-29 08:30:00";
        String t2 = "2017-09-7 09:30:00";
        try
        {
            now.setTime(formatter.parse(nowtime));
            c1.setTime(formatter.parse(t1));
            c2.setTime(formatter.parse(t2));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        int result1 = now.compareTo(c1);// 比开始时间小，未开始
        int result2 = now.compareTo(c2);// 比结束时间大，已结束
        System.out.println("result1=======" + result1);
        System.out.println("result2=======" + result2);
    }

    /**
     * 判断时间1大于时间2（true 时间1大于时间2）
     *
     * @param date1 字符串日期1
     * @param date2 字符串日期2
     * @return
     * @descript:判断时间1大于时间2（true 时间1大于时间2）
     */
    public static boolean isTime1GreaterTime2(String date1, String date2)
    {
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
        try
        {
            long d1 = formater.parse(date1).getTime();
            long d2 = formater.parse(date2).getTime();
            if (d1 - d2 > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return false;
    }

    public static void main(String[] args)
    {
        System.out.println("======" +     Integer.getInteger("32323",1234));
        System.out.println("" + isTime1GreaterTime2("2018-3-31", "2018-02-05"));
//        String Str = new String("www.google.com");
//
//        System.out.print("匹配成功返回值 :" );
//        System.out.println(Str.replaceAll("google", "runoob" ));
//        System.out.print("匹配失败返回值 :" );
//        System.out.println(Str.replaceAll("(.*)taobao(.*)", "runoob" ));
    }

    public static double totalMoney(double money)
    {
        java.math.BigDecimal bigDec = new java.math.BigDecimal(money);
        double total = bigDec.setScale(6, java.math.BigDecimal.ROUND_HALF_UP).doubleValue();
        DecimalFormat df = new DecimalFormat("0.00");
        return total;
    }

    public static boolean isDouble(String str)
    {
        try
        {
            Double.parseDouble(str);
            return true;
        }
        catch (NumberFormatException ex)
        {
            return false;
        }
    }
}
