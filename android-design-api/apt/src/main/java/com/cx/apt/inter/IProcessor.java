package com.cx.apt.inter;

import com.cx.apt.AnnotationProcessor;

import javax.annotation.processing.RoundEnvironment;

/**
 * 类描述：注解处理器接口
 * 需要传入的键：****
 * 传入的值类型：****
 * 传入的值含义：****
 * 是否必传：****
 * 作者：郑朝军 on 2017/6/14 11:29
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/6/14 11:29
 * 修改备注：
 */
public interface IProcessor
{
    void process(RoundEnvironment roundEnv, AnnotationProcessor mAbstractProcessor);
}
