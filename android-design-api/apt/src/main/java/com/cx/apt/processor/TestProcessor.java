package com.cx.apt.processor;

import com.cx.annotation.aspect.MemoryCache;
import com.cx.apt.AnnotationProcessor;
import com.cx.apt.inter.IProcessor;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;

import javax.annotation.processing.RoundEnvironment;

import static com.squareup.javapoet.TypeSpec.classBuilder;
import static javax.lang.model.element.Modifier.FINAL;
import static javax.lang.model.element.Modifier.PUBLIC;
import static javax.lang.model.element.Modifier.STATIC;

/**
 * 类描述：
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传：
 * 作者：郑朝军 on 2017/7/14 20:16
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2017/7/14 20:16
 * 修改备注：
 */
public class TestProcessor implements IProcessor
{

    @Override
    public void process(RoundEnvironment roundEnv, AnnotationProcessor mAbstractProcessor)
    {
        String CLASS_NAME = "InstanceFactory";
        TypeSpec.Builder tb = classBuilder(CLASS_NAME).addModifiers(PUBLIC, FINAL).addJavadoc("@ 实例化工厂 此类由apt自动生成");
        MethodSpec.Builder methodBuilder1 = MethodSpec.methodBuilder("create").addAnnotation(MemoryCache.class)
                .addJavadoc("@此方法由apt自动生成")
                .returns(Object.class).addModifiers(PUBLIC, STATIC).addException(IllegalAccessException.class).addException(InstantiationException.class)
                .addParameter(Class.class, "mClass");
    }
}
