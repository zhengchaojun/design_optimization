#android-design-api

1.包含的基础功能

  activity基类；adapter基类[自行发挥]；网络回调；窗口栈管理；工具

2.窗口基类标题栏的实现方式

  基于ToolBar实现，向官方靠拢

3.说明

  此api结构中无强行使用第三方库，本api仅提供一个尽量不受约束的空壳结构，更详尽的功能请自行发挥

  此为原AndroidDesign的一个api抽象或简化版.精简其结构并去掉了所有非必要的模块

  简化(1)：BaseActivity函数量精简，使用频度不高的或非必要的已被清除。

          窗口层中去掉了FunctionView结构，窗口层进行了进一步耦合

  简化(2)：BaseService函数形参及及函数量精简，去除HttpHandler结构，去除class及functionView形参，去掉网络日志打印，
         
          去掉查看友好化源Json数据及错误自动定位输出功能

  去掉：去掉的功能诸如FunctionView结构，语音，艺术字，全部调试模块(日志，内存，进程及控件调试)，邮件，数据库等以及

       xutils,actionbarsherlock,slidingmenu等所有第三方依赖

  一致：可变可不变的地方尽量向原AndroidDesign靠拢，诸如BaseActivity各函数名称，ids文件中各个公用id的定义等尽量与原有定义保持一致

  差异：窗口层中的view变量，旧义为common_root_fit.xml中的根控件，新义为内容栏根控件且更名为contentView

       窗口层中的查找指定序号的同名id控件对应的函数，旧名为findViewById，新名为findView，且查找范围从根控件缩小为contentView

4.目的

  向官方靠拢并兼容，去除所有约束




