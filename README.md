通用App基础框架
描述：使用基础框架常用方法调用,基础框架封装了通用app的代码。大多数app需要的代码都可以在里面找到,只需要会用他就行，经过多个上线app的使用基础框架是比较稳定的。
模块功能：

1：使用三种方式实现底部Tab，禁止滑动，允许滑动
2：修改优化pullToRefresh源码，新增两种adapter获取方式，用于更新adapterView与获取item，解决源码分页bug，修改头尾分页具体提示，并新增PullToRefreshSwipeListView与PullToRefreshWrapListView等控件
3：二次拦截监听技术解析：(符合面向对象的开闭原则)
一次拦截监听技术原理：（简单来说一个类实现了一个监听器对该监听器起到拦截的作用）

二次拦截功能需求： (简单来说一次拦截功能不满足需求,需要再次处理数据)
二次拦截技术核心：接收上层，回调下层(或接收基层，回调运行层)，即阻断了基层与运行层的连接，事实上，一二次拦截及运行层监听均会执行
拦截技术比较：一次拦截耗费速度，不易维护，适用基层共性监听；二次拦截不影响速度，易维护，适用扩展独特性监听；运行层拦截不影响速度，难维护，适用非公用一次性监听

4：在CrashHandler uncaughtException错误日志邮件发送

5: ActivityManager activity自定义栈
stack本身会存在重复添加同一个activity的现象
其里功能需要在任何activity的创建，返回finish掉同步到自定义栈的前提下才能正常运行
同步添加，移除后，则不会出现重复activity的现象
能够在任何位置直接获取当前activity对象
跨级返回传递数据
包含获取,移除activity，获取,移除指定activity，跳转activity，判断栈中是否包含该元素等

6：基类的封装：
1：通用标题栏api封装，api举例：设置标题栏名称，设置标题栏颜色，设置标题栏右上角资源图片，设置显示进度条，设置隐藏进度条。通过封装标题栏和进度条子类的布局文件中不需要加入标题栏，和进度条，只需要调用父类接口的方法
2：基类主要为子类提供api方法比如
7：通用框架代码优化
1：每一行代码都需要考虑，整洁，观赏性好，
2：当一个类中代码较多需要封装成一个类总结一句话：继承，封装，多态，反射，注解（aop），范型，分层架构
3：每一个类中都需要注释，每一个方法也都要注释
4：将第三方库优化成自己的代码
8：GIS通用模块封装：
Arcgis基类封装
1：添加矢量图形到Overlay如点线面
2：将天地图进行封装，主要是将地图配置做成json，子类加载天地图只需要获取MapViewManager调用接口方法，总结：将地图相关的api进行封装
9：aop面向切面
1：动态权限申请，举例：在方法中加入注解写入对应的权限
2：log日志
10：网络层
1.HandlerThread+Handler这样的自定义异步加载方式，一般为静态方式 [为适合的方式]（简单来说该方法是静态的通过HandlerThread+Handler配合回调到窗口层）
2.关于网络层父类与网络层子类：
网络层父类：为网络内核层，例如可以是封装了第三方网络后的最近的一层，主要提供get(),post(),updateFile(),updateFiles(),addParams(),getUrl()等函数（简单来说就是一个抽象类定义抽象方法，提供子类实现）网络层子类：完整的子层网络应该包括三项：封装命令与参数；发出请求；同步返回数据到HandlerThread层后经过异步HandlerThread解析

8：常用工具类：
TDevice 手机设备相关
AbJsonUtil Gson转换封装
CodecUtil 文件MD5，BASE64，SHA，MAC，DES加密解密相关
CacheManager 离线缓存
Logger日志统一打印输出
TimeFormatUtils 时间日期格式化
ViewOutUtils 递归输出参数ViewGroup 子孩子
ParametersUtil 后台参数统一验证参数（分别为1：当前时间2：签名参数3：随机数）
AdapterViewUtils 重置ListView高度
等等
9:数据库脚本初始化
10：tif文件的瓦片裁剪
11：shp，kml文件读写
12：数据库建模自动生成SQL脚本工具
13：mbtils离线文件包的生成
14: 本地仓库构建
15：模板代码的拷贝
16:第三方：集成了CityPick，DataPick，Pulltorefresh,友盟，mail，log4j退出主Activity主动清理静态内存。
17:板块架构及常用功能点
1.调试模式：可以提供查看日志，内存，进程
2.发布模式:不执行调试模式下的代码
3.常用界面，AboutActivity,ActionSearchActivity ,EditActivity ,InstanceActivity ，JsonFormatActivity，LauncherActivity,LogActivity,MainActivity,MemberCenterActivity,MemberForgetPasswordActivity,MemberInfoActivity,MemberLoginActivity,MemberRegistActivity,MemberResetPasswordActivity ,MemoryActivity,PhotoBrowseActivity,ProcessActivity,ProcessDetailActivity,ProvisionActivity,SearchActivity,WebActivity(基类)，ListWapBaseView（基类） 
4.基类宏观调控，
5.动态findViewByid使用DataBinding
6.相一布局相同id序号查找id
7.ListView动态设置Param
8.采用Aop方式1：检测是否登录 2：oncreate方法打印Fragment生命周期 3：防止重复点击
9.ListView重置高度通过获取Adapter获取每一个item的高度重置ListView高度
10.基类自动处理ProgressBar显示和未显示
11.LazyFragment懒加载
开发过程中注意事项：父类执行构造函数调用到子类方法时，子类的成员变量都为空，getParent()向上查找只查找一级和findviewbyid不相同，map中put一个list为引用传递,设置LayoutParams最好getLayoutParams。

11.框架相关优化：
1.优化：布局的优化
common_progressbar 
common_record_text_view 
common_refresh_grid_view_smaller 
common_refresh_list_view_no_devider 
公共布局使用<inclued>标签
去除不必要的嵌套和view节点